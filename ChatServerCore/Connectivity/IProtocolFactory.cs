﻿namespace ChatServerCore.Connectivity
{
    /// <summary>
    /// The type which implemented this interface can spawn a object to cater client connection.
    /// </summary>
    public interface IProtocolFactory
    {
        /// <summary>
        /// Startup the factory, allows the factory to initialize itself.
        /// </summary>
        void Start();

        /// <summary>
        /// Spawn a new service object.
        /// </summary>
        /// <returns></returns>
        IProtocol CreateService();

        /// <summary>
        /// Tear down the factory.
        /// </summary>
        void Stop();

        /// <summary>
        /// Return name of factory.
        /// </summary>
        string GetName();
    }
}
