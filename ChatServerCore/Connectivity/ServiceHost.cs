﻿using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using ChatServerCore.Utilities;

namespace ChatServerCore.Connectivity
{
    /// <summary>
    /// Host application service with TCP protocol, listen and accept new connection from clients.
    /// </summary>
    public class ServiceHost
    {
        private IProtocolFactory serviceFactory;
        private int port;
        private Socket listener;
        private Thread listenThread;

        /// <summary>
        /// Current IP address and port. Available only when the instance is running.
        /// </summary>
        public IPEndPoint ListeningEndPoint { get; private set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="port">The port that this instance listen on.</param>
        /// <param name="serviceFactory">A factory that spawn service object when new connection is accepted.</param>
        public ServiceHost(int port, IProtocolFactory serviceFactory)
        {
            this.port = port;
            this.serviceFactory = serviceFactory;
        }

        /// <summary>
        /// Start to accept new connection.
        /// </summary>
        public void Run()
        {
            if (listener == null)
            {
                ListeningEndPoint = ResolveEndPoint();

                listener = new Socket(ListeningEndPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                listener.Bind(ListeningEndPoint);
                listener.Listen(1024);

                serviceFactory.Start();

                listenThread = new Thread(ListenRoutine);
                listenThread.Start();
                return;
            }
            throw new InvalidOperationException("ServiceHost.Start occurs error: ServiceHost is already running.");
        }

        /// <summary>
        /// Stop running.
        /// </summary>
        public void Stop()
        {
            if (listener != null)
            {
                listener.Close();
                serviceFactory.Stop();
                return;
            }
            throw new InvalidOperationException("ServiceHost.Stop occurs error: ServiceHost is not started.");
        }

        private void ListenRoutine()
        {
            try
            {
                while (true)
                {
                    var asyncResult = listener.BeginAccept(new AsyncCallback(AcceptCallback), listener);
                    asyncResult.AsyncWaitHandle.WaitOne();
                }
            }
            catch (Exception ex)
            {
                Logger.Info(String.Format("ServiceHost.ListenRoutine occurs error: {0}\nStack Trace:\n{1}", ex.Message, ex.StackTrace));
            }
        }

        private void AcceptCallback(IAsyncResult ar)
        {
            try
            {
                if (listener == null) return;

                Socket pSocket = listener.EndAccept(ar);
                //Logger.Info(String.Format("Connection from {0} accepted.", pSocket.RemoteEndPoint));
                IProtocol service = serviceFactory.CreateService();
                TcpTransport transport = new TcpTransport(pSocket, service);
                service.ConnectionMade(transport);

            }
            catch (Exception ex)
            {
                Logger.Info(String.Format("ServiceHost.AcceptCallback occurs error: {0}\nStack Trace:\n{1}", ex.Message, ex.StackTrace));
            }
        }

        private IPEndPoint ResolveEndPoint()
        {
            IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
            int ipv4Index = 0;
            for (int index = 0; index < ipHostInfo.AddressList.Length; index++)
            {
                IPAddress addressFromLocal = ipHostInfo.AddressList[index];
                if (addressFromLocal.AddressFamily == AddressFamily.InterNetwork)
                {
                    if (!serviceFactory.GetName().Equals("Primary Service"))
                    {
                        ipv4Index = index;
                        break;
                    }
                    else
                    {
                        if (!addressFromLocal.ToString().StartsWith("192"))
                        {
                            ipv4Index = index;
                            break;
                        }
                    }
                }
            }

            if (ipv4Index > ipHostInfo.AddressList.Count())
            {
                ipv4Index = 0;
            }
            IPAddress ipAddress = ipHostInfo.AddressList[ipv4Index];
            return new IPEndPoint(ipAddress, port);
        }
    }
}
