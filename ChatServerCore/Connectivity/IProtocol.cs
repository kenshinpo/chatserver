﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatServerCore.Connectivity
{
    /// <summary>
    /// The type that implemented this interface is a type that cater user connection.
    /// </summary>
    public interface IProtocol
    {
        /// <summary>
        /// Notify the object that a new connection is made.
        /// </summary>
        /// <param name="transport">The new connection.</param>
        void ConnectionMade(ITransport transport);

        /// <summary>
        /// Notify the object that the connection is lost.
        /// </summary>
        /// <param name="transport">The lost connection.</param>
        void ConnectionLost(ITransport transport);

        /// <summary>
        /// Notify the object that the connection is timeout.
        /// </summary>
        /// <param name="transport"></param>
        void ConnectionTimeOut(ITransport transport);
        /// <summary>
        /// Notify the object that data is sent.
        /// </summary>
        /// <param name="data">Sent data.</param>
        void DataSent(byte[] data, int sentByteCount);

        /// <summary>
        /// Notify the object that data is received.
        /// </summary>
        /// <param name="data">Received data.</param>
        void DataReceived(byte[] data, int receivedByteCount);
    }
}
