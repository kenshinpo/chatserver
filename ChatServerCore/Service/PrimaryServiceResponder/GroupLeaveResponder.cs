﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChatServerCore.Data;
using ChatServerCore.Data.Database;
using ChatServerCore.Data.Entity;
using ChatServerCore.Message;
using ChatServerCore.Utilities;

namespace ChatServerCore.Service.PrimaryServiceResponder
{
    public class GroupLeaveResponder : MessageResponder
    {
        public GroupLeaveResponder()
            : base()
        {
            MessageOfInterested = MessageFormat.GROUPLEAVE.First();
        }

        public override void Respond(ChatSessionCollection onlineUsers, ChatSession currentUser, string message)
        {
            try
            {
                GroupLeaveMessage msg = RealizeMessage<GroupLeaveMessage>(message);
                IEnumerable<Participant> allParts = null;
                Chat currentChat = null;
                bool isChangeOfAdmin = false;

                Repositories<ParticipantRepository> participants = new Repositories<ParticipantRepository>();
                Repositories<ChatRepository> chats = new Repositories<ChatRepository>();
                Repositories<ConversationRepository> conversations = new Repositories<ConversationRepository>();

                allParts = participants[currentUser.ChatUserID].FindBySenderChatId(msg.ChatID, true);

                var chatRepo = chats[currentUser.ChatUserID];
                currentChat = chatRepo.GetById(msg.ChatID, true);

                string senderMsgID = conversations[currentUser.ChatUserID].CreateSendConv(msg.ChatID, null, null, null, "GROUPMSG", "You have left.", 6);
                string[] recvNotificationIDs = new string[allParts.Count()];
                string[] recvMessages = new string[allParts.Count()];

                if (currentChat.AdminID == currentUser.ChatUserID)
                {
                    isChangeOfAdmin = true;
                }

                int count = 0;
                string newAdminID = string.Empty;

                foreach (var p in allParts)
                {
                    if (p.ReceiverChatStatus != null && p.ReceiverChatStatus == true)
                    {
                        // Update admin
                        if (count == 0 && isChangeOfAdmin)
                        {
                            newAdminID = p.ReceiverID;
                        }

                        if (isChangeOfAdmin && !string.IsNullOrEmpty(newAdminID))
                        {
                            // Update chat for new admin
                            chats[p.ReceiverID].UpdateAdmin(p.ReceiverChatID, newAdminID);
                        }

                        string leaveMessage = string.Format("{0} has left.", MessageConverter.HtmlDecode(ChatAccountManager.GetUserName(currentUser.ChatUserID, p.ReceiverID)));

                        // Create "Leave" conversation for other members
                        recvMessages[count] = leaveMessage;
                        recvNotificationIDs[count++] = conversations[p.ReceiverID].CreateRecvConv(currentUser.ChatUserID, msg.ChatID, senderMsgID, p.ReceiverChatID, "MSGPEND", leaveMessage, 6);

                        // Remove chat from respective members
                        participants[p.ReceiverID].Remove(p.ReceiverChatID, currentUser.ChatUserID, msg.ChatID);
                        participants[currentUser.ChatUserID].Remove(msg.ChatID, p.ReceiverID, p.ReceiverChatID);

                        if (!isChangeOfAdmin)
                        {
                            chats[p.ReceiverID].MarkAsUpdated(p.ReceiverChatID);
                        }
                    }
                }

                conversations[currentUser.ChatUserID].UpdateStatusBySenderChatId(msg.ChatID, 21);
                chatRepo.Delete(msg.ChatID);

                string[] allMembers = (from p in allParts where p.ReceiverChatStatus == true select p.ReceiverID).ToArray();
                string[] membersProfileName = new string[allMembers.Length];

                membersProfileName = ChatAccountManager.GetUserProfileName(allMembers);

                //Logger.Info("Members left count: " + allMembers.Count());

                string allMemberIDString = string.Empty;
                string allMemberProfileNameString = string.Empty;

                for (int index = 0; index < allMembers.Count(); index++)
                {
                    if (index != allMembers.Count() - 1)
                    {
                        allMemberIDString += allMembers[index] + ',';
                        allMemberProfileNameString += MessageConverter.HtmlEncode(membersProfileName[index]) + ',';
                    }
                    else
                    {
                        allMemberIDString += allMembers[index];
                        allMemberProfileNameString += MessageConverter.HtmlEncode(membersProfileName[index]);
                    }
                }

                // Sending back to current user
                GroupLeaveAckMessage gpAck = new GroupLeaveAckMessage()
                {
                    ChatID = msg.ChatID,
                    Timestamp = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff")
                };

                ReplyMessage(currentUser, gpAck);

                count = 0;
                // Sending to rest of the members
                foreach (var p in allParts)
                {
                    if (p.ReceiverChatStatus != null && p.ReceiverChatStatus == true && p.ReceiverID != currentUser.ChatUserID)
                    {
                        var user = onlineUsers[p.ReceiverID];

                        GroupLeaveSyncMessage gpSync = new GroupLeaveSyncMessage()
                        {
                            ChatID = p.ReceiverChatID,
                            AdminID = isChangeOfAdmin ? newAdminID : currentChat.AdminID,
                            MembersID = allMemberIDString,
                            MembersProfileName = allMemberProfileNameString,
                            NotificationID = recvNotificationIDs[count],
                            NotificationMessage = MessageConverter.HtmlEncode(recvMessages[count]),
                            Timestamp = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff")
                        };

                        if (user != null)
                        {
                            ReplyMessage(user, gpSync);
                        }
                        else
                        {
                            RelayServerCommunication.SendSearchUserMessage(p.ReceiverID, gpSync.ToString());
                        }

                        count++;

                        //Logger.Info(gpSync.ToString());
                    }
                   
                }
            }
            catch (Exception ex)
            {
                Logger.Info(ex.Message.ToString() + " " + ex.StackTrace.ToString());
            }
        }
    }
}
