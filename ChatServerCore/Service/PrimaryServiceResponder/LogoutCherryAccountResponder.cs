﻿using System;
using System.Linq;
using ChatServerCore.Data;
using ChatServerCore.Message;
using ChatServerCore.Utilities;

namespace ChatServerCore.Service.PrimaryServiceResponder
{
    public class LogoutCherryAccountResponder : MessageResponder
    {
        public LogoutCherryAccountResponder()
            : base()
        {
            MessageOfInterested = MessageFormat.LOGOUTCHERRYACCOUNT.First();
        }

        public override void Respond(ChatSessionCollection onlineUsers, ChatSession currentUser, string message)
        {
            try
            {
                // current user in this case refers to web service
                LogoutCherryAccountMessage msg = RealizeMessage<LogoutCherryAccountMessage>(message);

                var primaryOnlineUsers = MessageServiceFactory.Instances["Primary Service"].OnlineUsers;
                // Reply back to orginal user
                if (primaryOnlineUsers[msg.ExistingChatUserID] != null)
                {
                    ReplyMessage(primaryOnlineUsers[msg.ExistingChatUserID], msg);
                }
                else
                {
                    var users = new Repositories<UserRepository>();
                    var userRepo = users[msg.ExistingChatUserID];
                    int operatingType = userRepo.GetOperatingType();
                    string deviceToken = userRepo.GetDeviceToken();
                    int badge = userRepo.GetBadge();
                    PushNotification(deviceToken, "Your Cherry Account has been used to log in to other device.", "0", badge, operatingType);
                }
            }
            catch (Exception ex)
            {
                Logger.Info(ex.StackTrace.ToString());
            }
        }
    }
}
