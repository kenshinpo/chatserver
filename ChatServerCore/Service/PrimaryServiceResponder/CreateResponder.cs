﻿using System;
using System.Linq;
using ChatServerCore.Data;
using ChatServerCore.Message;
using ChatServerCore.Utilities;

namespace ChatServerCore.Service.PrimaryServiceResponder
{
    public class CreateResponder : MessageResponder
    {
        public CreateResponder()
            : base()
        {
            MessageOfInterested = MessageFormat.CREATE.First();
        }

        public override void Respond(ChatSessionCollection onlineUsers, ChatSession currentUser, string message)
        {
            try
            {
                CreateMessage msg = RealizeMessage<CreateMessage>(message);
                string senderChatId = string.Empty;

                Repositories<ParticipantRepository> participants = new Repositories<ParticipantRepository>();

                var existing = participants[currentUser.ChatUserID].FindByReceiverId(msg.RecvID, false);

                if (existing.Count() != 0)
                {
                    senderChatId = existing.First().SenderChatID;
                }
                else
                {
                    Repositories<ChatRepository> chats = new Repositories<ChatRepository>();

                    senderChatId = chats[currentUser.ChatUserID].Create(false, null, null, null);

                    string receiverChatId = chats[msg.RecvID].Create(false, null, null, null);

                    participants[currentUser.ChatUserID].Add(senderChatId, receiverChatId, msg.RecvID, false);
                    participants[msg.RecvID].Add(receiverChatId, senderChatId, currentUser.ChatUserID, false);
                }

                CreateAckMessage ack = new CreateAckMessage() { ChatId = senderChatId, RecvId = msg.RecvID, Temp = msg.Temp };
                ReplyMessage(currentUser, ack);
            }
            catch (Exception ex)
            {
                Logger.Info(ex.Message.ToString() + " " + ex.StackTrace.ToString());
                //Respond(onlineUsers, currentUser, message);
            }
        }
    }
}
