﻿using System;
using System.Linq;
using ChatServerCore.Data;
using ChatServerCore.Data.Entity;
using ChatServerCore.Message;
using ChatServerCore.Utilities;

namespace ChatServerCore.Service.PrimaryServiceResponder
{
    public class RecvAckResponder : MessageResponder
    {
        public RecvAckResponder()
            : base()
        {
            MessageOfInterested = MessageFormat.RECVACK.First();
        }

        public override void Respond(ChatSessionCollection onlineUsers, ChatSession currentUser, string message)
        {
            try
            {
                RecvAckMessage msg = RealizeMessage<RecvAckMessage>(message);
                Conversation conv = null;
                Chat chat = null;

                //[@6/17/2014 11:43:32 AM]: <@RECVACK>R01mg0wu6hbxI6C<@CHATID>M01mr0wu6hbHu24<@MSGID><@EOF> was received from User CC0100wu6dgHw2y.


                var conversations = new Repositories<ConversationRepository>();
                var chats = new Repositories<ChatRepository>();
                var users = new Repositories<UserRepository>();

                var recvConvRepo = conversations[currentUser.ChatUserID];
                conv = recvConvRepo.FindById(msg.MsgID).First();

                chat = chats[currentUser.ChatUserID].GetById(msg.ChatID);

                recvConvRepo.UpdateStatusByMsgId(msg.MsgID, 5);
                users[currentUser.ChatUserID].DecrementBadge();

                if (!chat.IsGroupChat)
                {
                    conversations[conv.SenderID].UpdateStatusByMsgId(conv.SenderConversationID, 2);

                    var user = onlineUsers[conv.SenderID];
                    SentMessage reply = new SentMessage() { ChatID = conv.SenderChatID, MsgID = conv.SenderConversationID };
                    if (user != null)
                    {
                        ReplyMessage(user, reply);
                    }
                    else
                    {
                        RelayServerCommunication.SendSearchUserMessage(conv.SenderID, reply.ToString());
                    }
                } 
            }
            catch (Exception ex)
            {
                Logger.Info(ex.StackTrace.ToString());
            }
        }
    }
}
