﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ChatServerCore.Data;
using ChatServerCore.Data.Database;
using ChatServerCore.Data.Entity;
using ChatServerCore.Message;
using ChatServerCore.Utilities;

namespace ChatServerCore.Service.PrimaryServiceResponder
{
    public class LogoutChatAccountResponder : MessageResponder
    {
        public LogoutChatAccountResponder()
            : base()
        {
            MessageOfInterested = MessageFormat.LOGOUTCHATACCOUNT.First();
        }

        public override void Respond(ChatSessionCollection onlineUsers, ChatSession currentUser, string message)
        {
            try
            {
                // current user in this case refers to web service
                LogoutChatAccountMessage msg = RealizeMessage<LogoutChatAccountMessage>(message);

                var primaryOnlineUsers = MessageServiceFactory.Instances["Primary Service"].OnlineUsers;
                
                // Force leave all group chats
                IEnumerable<Chat> groupChats = null;

                Repositories<ParticipantRepository> participants = new Repositories<ParticipantRepository>();
                Repositories<ChatRepository> chats = new Repositories<ChatRepository>();
                Repositories<ConversationRepository> conversations = new Repositories<ConversationRepository>();

                groupChats = chats[msg.ExistingChatUserID].FindGroupWithChatUserID(true);

                foreach (var currentGroupChat in groupChats)
                {
                    bool isChangeOfAdmin = false;
                    IEnumerable<Participant> allParts = participants[msg.ExistingChatUserID].FindBySenderChatId(currentGroupChat.ChatID, true);

                    string senderMsgID = conversations[msg.ExistingChatUserID].CreateSendConv(currentGroupChat.ChatID, null, null, null, "GROUPMSG", "You have left.", 6);
                    string[] recvNotificationIDs = new string[allParts.Count()];
                    string[] recvMessages = new string[allParts.Count()];

                    if (currentGroupChat.AdminID == msg.ExistingChatUserID)
                    {
                        isChangeOfAdmin = true;
                    }

                    int count = 0;
                    string newAdminID = string.Empty;

                    foreach (var p in allParts)
                    {
                        if (p.ReceiverChatStatus != null && p.ReceiverChatStatus == true)
                        {
                            // Update admin
                            if (count == 0 && isChangeOfAdmin)
                            {
                                newAdminID = p.ReceiverID;
                            }

                            if (isChangeOfAdmin && !string.IsNullOrEmpty(newAdminID))
                            {
                                // Update chat for new admin
                                chats[p.ReceiverID].UpdateAdmin(p.ReceiverChatID, newAdminID);
                            }

                            string leaveMessage = string.Format("{0} has left.", MessageConverter.HtmlDecode(ChatAccountManager.GetUserName(msg.ExistingChatUserID, p.ReceiverID)));

                            // Create "Leave" conversation for other members
                            recvMessages[count] = leaveMessage;
                            recvNotificationIDs[count++] = conversations[p.ReceiverID].CreateRecvConv(msg.ExistingChatUserID, currentGroupChat.ChatID, senderMsgID, p.ReceiverChatID, "MSGPEND", leaveMessage, 6);

                            // Remove chat from respective members
                            participants[p.ReceiverID].Remove(p.ReceiverChatID, msg.ExistingChatUserID, currentGroupChat.ChatID);
                            participants[msg.ExistingChatUserID].Remove(currentGroupChat.ChatID, p.ReceiverID, p.ReceiverChatID);

                            if (!isChangeOfAdmin)
                            {
                                chats[p.ReceiverID].MarkAsUpdated(p.ReceiverChatID);
                            }
                        }
                    }

                    conversations[msg.ExistingChatUserID].UpdateStatusBySenderChatId(currentGroupChat.ChatID, 21);
                    chats[msg.ExistingChatUserID].Delete(currentGroupChat.ChatID);

                    string[] allMembers = (from p in allParts where p.ReceiverChatStatus == true select p.ReceiverID).ToArray();
                    string[] membersProfileName = new string[allMembers.Length];

                    membersProfileName = ChatAccountManager.GetUserProfileName(allMembers);

                    //Logger.Info("Members left count: " + allMembers.Count());

                    string allMemberIDString = string.Empty;
                    string allMemberProfileNameString = string.Empty;

                    for (int index = 0; index < allMembers.Count(); index++)
                    {
                        if (index != allMembers.Count() - 1)
                        {
                            allMemberIDString += allMembers[index] + ',';
                            allMemberProfileNameString += MessageConverter.HtmlEncode(membersProfileName[index]) + ',';
                        }
                        else
                        {
                            allMemberIDString += allMembers[index];
                            allMemberProfileNameString += MessageConverter.HtmlEncode(membersProfileName[index]);
                        }
                    }

                    count = 0;
                    // Sending to rest of the members
                    foreach (var p in allParts)
                    {
                        if (p.ReceiverChatStatus != null && p.ReceiverChatStatus == true && p.ReceiverID != msg.ExistingChatUserID)
                        {
                            var user = onlineUsers[p.ReceiverID];

                            GroupLeaveSyncMessage gpSync = new GroupLeaveSyncMessage()
                            {
                                ChatID = p.ReceiverChatID,
                                AdminID = isChangeOfAdmin ? newAdminID : currentGroupChat.AdminID,
                                MembersID = allMemberIDString,
                                MembersProfileName = allMemberProfileNameString,
                                NotificationID = recvNotificationIDs[count],
                                NotificationMessage = MessageConverter.HtmlEncode(recvMessages[count]),
                                Timestamp = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff")
                            };

                            if (user != null)
                            {
                                ReplyMessage(user, gpSync);
                            }
                            else
                            {
                                RelayServerCommunication.SendSearchUserMessage(p.ReceiverID, gpSync.ToString());
                            }

                            count++;

                            //Logger.Info(gpSync.ToString());
                        }

                    }
                }
                
            }
            catch (Exception ex)
            {
                Logger.Info(ex.StackTrace.ToString());
            }
        }
    }
}
