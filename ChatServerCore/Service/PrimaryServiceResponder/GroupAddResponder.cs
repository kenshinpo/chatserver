﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChatServerCore.Data;
using ChatServerCore.Data.Database;
using ChatServerCore.Data.Entity;
using ChatServerCore.Message;
using ChatServerCore.Utilities;

namespace ChatServerCore.Service.PrimaryServiceResponder
{
    public class GroupAddResponder : MessageResponder
    {
        public GroupAddResponder()
        {
            MessageOfInterested = MessageFormat.GROUPADD.First();
        }

        public override void Respond(ChatSessionCollection onlineUsers, ChatSession currentUser, string message)
        {
            try
            {
                GroupAddMessage msg = RealizeMessage<GroupAddMessage>(message);
                IEnumerable<Participant> existingPart = null;
                Chat existingChat = null;

                Repositories<ChatRepository> chats = new Repositories<ChatRepository>();
                Repositories<ParticipantRepository> participants = new Repositories<ParticipantRepository>();
                Repositories<ConversationRepository> conversations = new Repositories<ConversationRepository>();

                existingChat = chats[currentUser.ChatUserID].GetById(msg.ChatID);
                existingPart = participants[currentUser.ChatUserID].FindBySenderChatId(msg.ChatID, true);

                Dictionary<string, Notification> notificationDict = new Dictionary<string, Notification>();

                string[] newMembersID = msg.Members.Split(',');
                List<string> nonExistingNewMemberIDs = new List<string>();

                foreach (string newMemberID in newMembersID)
                {
                    var chatRepo = chats[newMemberID];

                    string senderMessage = string.Format("You have added {0}.", MessageConverter.HtmlDecode(ChatAccountManager.GetUserName(newMemberID, currentUser.ChatUserID)));
                    string senderMsgID = conversations[currentUser.ChatUserID].CreateSendConv(msg.ChatID, null, null, null, "GROUPMSG", senderMessage, 6);

                    // Check if such member got removed before
                    bool isRemovedBefore = false;
                    string removedChatID = string.Empty;

                    foreach (var p in existingPart)
                    {
                        if (p.ReceiverID == newMemberID && p.ReceiverChatStatus == false)
                        {
                            isRemovedBefore = true;
                            removedChatID = p.ReceiverChatID;
                            break;
                        }
                    }

                    // Member not removed previously
                    if (!isRemovedBefore)
                    {
                        //Logger.Info("Member not removed previously");
                        string recvMessage = string.Format("You have been added by {0}.", MessageConverter.HtmlDecode(ChatAccountManager.GetUserName(currentUser.ChatUserID, newMemberID)));

                        string newChatID = chatRepo.Create(true, MessageConverter.HtmlDecode(existingChat.Title), existingChat.GroupPicURL == " " || string.IsNullOrEmpty(existingChat.GroupPicURL) ? null : existingChat.GroupPicURL, existingChat.AdminID);

                        nonExistingNewMemberIDs.Add(newMemberID);

                        //chatRepo.UpdatePicture(newChatID, existingChat.GroupPicURL, existingChat.ModifiedBy);

                        var newMemberPartRepo = participants[newMemberID];

                        string recvMessageID = conversations[newMemberID].CreateRecvConv(currentUser.ChatUserID, msg.ChatID, senderMsgID, newChatID, "MSGPEND", recvMessage, 6);

                        // Notification object for new member does not exist
                        if (!notificationDict.ContainsKey(newMemberID))
                        {
                            Notification notification = new Notification();
                            notification.NotificationID = new List<string>();
                            notification.NotificationID.Add(recvMessageID);
                            notification.NotificationMessage = new List<string>();
                            notification.NotificationMessage.Add(recvMessage);

                            notificationDict.Add(newMemberID, notification);

                        }
                        // Notification object exists for new member
                        else
                        {
                            notificationDict[newMemberID].NotificationID.Add(recvMessageID);
                            notificationDict[newMemberID].NotificationMessage.Add(recvMessage);
                        }

                        // Update to all existing old members
                        foreach (var p in existingPart)
                        {
                            if (p.ReceiverChatStatus != null && p.ReceiverChatStatus == true)
                            {
                                recvMessage = string.Format("{0} has been added by {1}.", MessageConverter.HtmlDecode(ChatAccountManager.GetUserName(newMemberID, p.ReceiverID)), MessageConverter.HtmlDecode(ChatAccountManager.GetUserName(currentUser.ChatUserID, p.ReceiverID)));
                                recvMessageID = conversations[p.ReceiverID].CreateRecvConv(currentUser.ChatUserID, msg.ChatID, senderMsgID, p.ReceiverChatID, "MSGPEND", recvMessage, 6);

                                // Notification object for existing member does not exist
                                if (!notificationDict.ContainsKey(p.ReceiverID))
                                {
                                    Notification notification = new Notification();
                                    notification.NotificationID = new List<string>();
                                    notification.NotificationID.Add(recvMessageID);
                                    notification.NotificationMessage = new List<string>();
                                    notification.NotificationMessage.Add(recvMessage);

                                    notificationDict.Add(p.ReceiverID, notification);

                                }
                                // Notification object exists for member
                                else
                                {
                                    notificationDict[p.ReceiverID].NotificationID.Add(recvMessageID);
                                    notificationDict[p.ReceiverID].NotificationMessage.Add(recvMessage);
                                }

                                chats[p.ReceiverID].MarkAsUpdated(p.ReceiverChatID);

                                //Logger.Info(string.Format("Adding participant: {0} for member: {1}", newMemberID, p.ReceiverID));

                                participants[p.ReceiverID].Add(p.ReceiverChatID, newChatID, newMemberID, true);

                                //Logger.Info(string.Format("Adding participant: {0} for member: {1}", p.ReceiverID, newMemberID));

                                newMemberPartRepo.Add(newChatID, p.ReceiverChatID, p.ReceiverID, true);

                            }

                        }

                        //Logger.Info(string.Format("Adding participant: {0} for current user: {1}", newMemberID, currentUser.ChatUserID));

                        participants[currentUser.ChatUserID].Add(msg.ChatID, newChatID, newMemberID, true);

                        //Logger.Info(string.Format("Adding participant: {0} for member: {1}", currentUser.ChatUserID, newMemberID));

                        newMemberPartRepo.Add(newChatID, msg.ChatID, currentUser.ChatUserID, true);
                    }

                    // Member removed previously
                    else
                    {
                        //Logger.Info("Update chat status from removed to exist");
                        //Logger.Info("Member removed previously");
                        //Logger.Info("ChatID: " + removedChatID);
                        //Logger.Info("ChatUserID: " + newMemberID);

                        chatRepo.UpdateStatus(removedChatID, true);
                        chatRepo.UpdateTitle(removedChatID, existingChat.Title);
                        chatRepo.UpdatePicture(removedChatID, existingChat.GroupPicURL);

                        IEnumerable<Participant> existingForCurrentRemovedUser = participants[newMemberID].FindBySenderChatId(removedChatID, true);

                        string[] membersOfOldRemovedChat = (from p in existingForCurrentRemovedUser where p.ReceiverID != currentUser.ChatUserID select p.ReceiverID).ToArray();
                        string[] membersOfCurrentChat = (from p in existingPart where p.ReceiverID != newMemberID && p.ReceiverChatStatus == true select p.ReceiverID).ToArray();

                        string[] newMembersForOldRemovedChat = membersOfCurrentChat.Except(membersOfOldRemovedChat).ToArray();
                        string[] removedMembersForOldRemovedChat = membersOfOldRemovedChat.Except(membersOfCurrentChat).ToArray();
                        string recvMessageID = string.Empty;
                        string recvMessage = string.Empty;

                        #region Remove
                        // Remove participants from removed-added-back chat
                        // Contains currentUser
                        foreach (var p in existingForCurrentRemovedUser)
                        {
                            if (p.ReceiverID != currentUser.ChatUserID)
                            {
                                // Remove participants
                                if (removedMembersForOldRemovedChat.Contains(p.ReceiverID))
                                {
                                    //Logger.Info("This person has been removed from removed-add-back chat");

                                    recvMessage = string.Format("{0} has left.", MessageConverter.HtmlDecode(ChatAccountManager.GetUserName(p.ReceiverID, newMemberID)));

                                    if (p.ReceiverChatStatus != null && p.ReceiverChatStatus != false)
                                    {
                                        participants[p.ReceiverID].Remove(p.ReceiverChatID, newMemberID, removedChatID);
                                        participants[newMemberID].Remove(removedChatID, p.ReceiverID, p.ReceiverChatID);
                                    }

                                    // Creating for removed-added-back member
                                    recvMessageID = conversations[newMemberID].CreateRecvConv(currentUser.ChatUserID, msg.ChatID, senderMsgID, removedChatID, "MSGPEND", recvMessage, 6);

                                    // Notification object for removed-added-back member does not exist
                                    if (!notificationDict.ContainsKey(newMemberID))
                                    {
                                        Notification notification = new Notification();
                                        notification.NotificationID = new List<string>();
                                        notification.NotificationID.Add(recvMessageID);
                                        notification.NotificationMessage = new List<string>();
                                        notification.NotificationMessage.Add(recvMessage);

                                        notificationDict.Add(newMemberID, notification);

                                    }
                                    // Notification object exists for removed-added-back member
                                    else
                                    {
                                        notificationDict[newMemberID].NotificationID.Add(recvMessageID);
                                        notificationDict[newMemberID].NotificationMessage.Add(recvMessage);
                                    }
                                }
                                else
                                {
                                    //Logger.Info(string.Format("{0} does not contains in removed", p.ReceiverID));
                                }
                            }
                        }
                        #endregion

                        // Add participants for the removed-added-back chat
                        // Does not contain currentUser
                        foreach (var p in existingPart)
                        {
                            if (p.ReceiverChatStatus != null && p.ReceiverChatStatus == true && p.ReceiverID != newMemberID)
                            {
                                // Add participants
                                if (newMembersForOldRemovedChat.Contains(p.ReceiverID))
                                {
                                    // New members
                                    if (msg.Members.Split(',').Contains(p.ReceiverID))
                                    {
                                        recvMessage = string.Format("{0} has been added by {1}.", MessageConverter.HtmlDecode(ChatAccountManager.GetUserName(newMemberID, p.ReceiverID)), MessageConverter.HtmlDecode(ChatAccountManager.GetUserName(currentUser.ChatUserID, p.ReceiverID)));
                                    }
                                    // Already existed
                                    else
                                    {
                                        recvMessage = string.Format("{0} has joined.", MessageConverter.HtmlDecode(ChatAccountManager.GetUserName(newMemberID, p.ReceiverID)));
                                    }

                                    participants[p.ReceiverID].Add(p.ReceiverChatID, removedChatID, newMemberID, true);
                                    participants[newMemberID].Add(removedChatID, p.ReceiverChatID, p.ReceiverID, true);
                                }
                                else
                                {
                                    recvMessage = string.Format("{0} has been added back by {1}.", MessageConverter.HtmlDecode(ChatAccountManager.GetUserName(newMemberID, p.ReceiverID)), MessageConverter.HtmlDecode(ChatAccountManager.GetUserName(currentUser.ChatUserID, p.ReceiverID)));
                                }

                                if (!string.IsNullOrEmpty(recvMessage))
                                {
                                    // Creating for existing/new member to be added to removed-added-back member
                                    recvMessageID = conversations[p.ReceiverID].CreateRecvConv(currentUser.ChatUserID, msg.ChatID, senderMsgID, p.ReceiverChatID, "MSGPEND", recvMessage, 6);

                                    // Notification object for existing member does not exist
                                    if (!notificationDict.ContainsKey(p.ReceiverID))
                                    {
                                        Notification notification = new Notification();
                                        notification.NotificationID = new List<string>();
                                        notification.NotificationID.Add(recvMessageID);
                                        notification.NotificationMessage = new List<string>();
                                        notification.NotificationMessage.Add(recvMessage);

                                        notificationDict.Add(p.ReceiverID, notification);

                                    }
                                    // Notification object exists for member
                                    else
                                    {
                                        notificationDict[p.ReceiverID].NotificationID.Add(recvMessageID);
                                        notificationDict[p.ReceiverID].NotificationMessage.Add(recvMessage);
                                    }
                                }

                            }

                        }

                        string recvAddedMessage = string.Format("You have been added back into this group by {0}.", MessageConverter.HtmlDecode(ChatAccountManager.GetUserName(currentUser.ChatUserID, newMemberID)));
                        string recvAddedMessageID = conversations[newMemberID].CreateRecvConv(currentUser.ChatUserID, msg.ChatID, senderMsgID, removedChatID, "MSGPEND", recvAddedMessage, 6);

                        // Notification object for removed-added-back member does not exist
                        if (!notificationDict.ContainsKey(newMemberID))
                        {
                            Notification notification = new Notification();
                            notification.NotificationID = new List<string>();
                            notification.NotificationID.Add(recvAddedMessageID);
                            notification.NotificationMessage = new List<string>();
                            notification.NotificationMessage.Add(recvAddedMessage);

                            notificationDict.Add(newMemberID, notification);

                        }
                        // Notification object exists for removed-added-back member
                        else
                        {
                            notificationDict[newMemberID].NotificationID.Add(recvAddedMessageID);
                            notificationDict[newMemberID].NotificationMessage.Add(recvAddedMessage);
                        }
                    }

                    existingPart = participants[currentUser.ChatUserID].FindBySenderChatId(msg.ChatID, true);
                }

                // Update current user who add the member
                chats[currentUser.ChatUserID].MarkAsUpdated(msg.ChatID);

                // Get all members
                string[] allMembers = (from ep in existingPart where ep.ReceiverChatStatus == true select ep.ReceiverID).ToArray();

                if (!allMembers.Contains(currentUser.ChatUserID))
                {
                    allMembers = allMembers.Concat(new string[] { currentUser.ChatUserID }).ToArray();
                }

                string[] membersProfileName = new string[allMembers.Length];
                membersProfileName = ChatAccountManager.GetUserProfileName(allMembers);

                string allMemberIDString = string.Empty;
                string allMemberProfileNameString = string.Empty;

                for (int i = 0; i < allMembers.Count(); i++)
                {
                    if (i != allMembers.Count() - 1)
                    {
                        allMemberIDString += allMembers[i] + ',';
                        allMemberProfileNameString += MessageConverter.HtmlEncode(membersProfileName[i]) + ',';
                    }
                    else
                    {
                        allMemberIDString += allMembers[i];
                        allMemberProfileNameString += MessageConverter.HtmlEncode(membersProfileName[i]);
                    }
                }

                // Reply to everyone, including current user
                foreach (var ep in existingPart)
                {
                    if (ep.ReceiverChatStatus != null && ep.ReceiverChatStatus == true && ep.ReceiverID != currentUser.ChatUserID)
                    {
                        //Logger.Info(string.Format("Reply sync to member {0}", ep.ReceiverID));

                        var user = onlineUsers[ep.ReceiverID];

                        List<string> notificationIDs = notificationDict[ep.ReceiverID].NotificationID;
                        List<string> notificationMessages = notificationDict[ep.ReceiverID].NotificationMessage;

                        string allNotificationIDString = string.Empty;
                        string allNotificationMessageString = string.Empty;

                        for (int i = 0; i < notificationIDs.Count(); i++)
                        {
                            if (i != notificationIDs.Count() - 1)
                            {
                                allNotificationIDString += notificationIDs[i] + ',';
                                allNotificationMessageString += MessageConverter.HtmlEncode(notificationMessages[i]) + ',';
                            }
                            else
                            {
                                allNotificationIDString += notificationIDs[i];
                                allNotificationMessageString += MessageConverter.HtmlEncode(notificationMessages[i]);
                            }
                        }

                        string syncMessage = string.Empty;

                        if (nonExistingNewMemberIDs.Contains(ep.ReceiverID))
                        {
                            GroupSyncMessage groupSync = new GroupSyncMessage()
                            {
                                ChatID = ep.ReceiverChatID,
                                Title = MessageConverter.HtmlEncode(existingChat.Title),
                                GroupPicURL = string.IsNullOrEmpty(existingChat.GroupPicURL) ? " " : existingChat.GroupPicURL,
                                AdminID = existingChat.AdminID,
                                MembersID = allMemberIDString,
                                MembersProfileName = allMemberProfileNameString,
                                Timestamp = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff")
                            };

                            syncMessage = groupSync.ToString();
                        }
                        else
                        {
                            GroupAddSyncMessage addSync = new GroupAddSyncMessage()
                            {
                                ChatID = ep.ReceiverChatID,
                                MembersID = allMemberIDString,
                                MembersProfileName = allMemberProfileNameString,
                                NotificationID = allNotificationIDString,
                                NotificationMessage = allNotificationMessageString,
                                Timestamp = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff")
                            };

                            syncMessage = addSync.ToString();
                        }

                        if (user != null)
                        {
                            //Logger.Info("Same Server");
                            ReplyMessage(user, syncMessage);
                        }
                        else
                        {
                            //Logger.Info("Sent to RS");
                            RelayServerCommunication.SendSearchUserMessage(ep.ReceiverID, syncMessage);
                        }
                    }

                }

                // Reply back to the one who add these new members
                GroupAddAckMessage gpAddAck = new GroupAddAckMessage()
                {
                    ChatID = msg.ChatID,
                    MembersID = allMemberIDString,
                    MembersProfileName = allMemberProfileNameString,
                    Timestamp = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff")
                };

                //Logger.Info(string.Format("Reply add ack to {0}", currentUser.ChatUserID));
                ReplyMessage(currentUser, gpAddAck);

            }
            catch (Exception ex)
            {
                Logger.Info(ex.Message.ToString() + " " + ex.StackTrace.ToString());
            }
        }
    }
}
