﻿using System;
using System.Linq;
using ChatServerCore.Data;
using ChatServerCore.Message;
using ChatServerCore.Utilities;

namespace ChatServerCore.Service.PrimaryServiceResponder
{
    public class SentAckResponder : MessageResponder
    {
        public SentAckResponder()
        {
            MessageOfInterested = MessageFormat.SENTACK.First();
        }

        public override void Respond(ChatSessionCollection onlineUsers, ChatSession currentUser, string message)
        {
            try
            {
                SentAckMessage msg = RealizeMessage<SentAckMessage>(message);

                Repositories<ConversationRepository> conversations = new Repositories<ConversationRepository>();

                conversations[currentUser.ChatUserID].UpdateStatusByMsgId(msg.MsgID, 3);
            }
            catch (Exception ex)
            {
                Logger.Info(ex.StackTrace.ToString());
            }
        }
    }
}
