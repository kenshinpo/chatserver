﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChatServerCore.Data;
using ChatServerCore.Data.Database;
using ChatServerCore.Data.Entity;
using ChatServerCore.Message;
using ChatServerCore.Utilities;

namespace ChatServerCore.Service.PrimaryServiceResponder
{
    public class GroupRemoveResponder : MessageResponder
    {
        public GroupRemoveResponder()
            : base()
        {
            MessageOfInterested = MessageFormat.GROUPREMOVE.First();
        }

        public override void Respond(ChatSessionCollection onlineUsers, ChatSession currentUser, string message)
        {
            try
            {
                GroupRemoveMessage msg = RealizeMessage<GroupRemoveMessage>(message);

                IEnumerable<Participant> allParts = null;
                Chat currentChatOfRemovedMember = null;
                bool isChangeOfAdmin = false;
                List<string> removedMembersID = new List<string>();

                Repositories<ParticipantRepository> participants = new Repositories<ParticipantRepository>();
                Repositories<ChatRepository> chats = new Repositories<ChatRepository>();
                Repositories<ConversationRepository> conversations = new Repositories<ConversationRepository>();

                allParts = participants[currentUser.ChatUserID].FindBySenderChatId(msg.ChatID, true);

                string chatIDOfMemberToBeRemoved = string.Empty;

                foreach (var p in allParts)
                {
                    // This is the person to be removed from DB
                    if (p.ReceiverID == msg.RemovedMemberID)
                    {
                        chatIDOfMemberToBeRemoved = p.ReceiverChatID;
                        break;
                    }
                }

                allParts = participants[msg.RemovedMemberID].FindBySenderChatId(chatIDOfMemberToBeRemoved, true);

                // Person who removed
                string senderMessage = string.Format("You have removed {0}.", MessageConverter.HtmlDecode(ChatAccountManager.GetUserName(msg.RemovedMemberID, currentUser.ChatUserID)));
                string senderMsgID = conversations[currentUser.ChatUserID].CreateSendConv(msg.ChatID, null, null, null, "GROUPMSG", senderMessage, 6);

                var chatRepo = chats[msg.RemovedMemberID];
                currentChatOfRemovedMember = chatRepo.GetById(chatIDOfMemberToBeRemoved, true);

                if (currentChatOfRemovedMember.AdminID == msg.RemovedMemberID)
                {
                    isChangeOfAdmin = true;
                }

                string[] recvNotificationIDs = new string[allParts.Count()];
                string[] recvMessages = new string[allParts.Count()];

                int count = 0;
                string newAdminID = string.Empty;

                foreach (var p in allParts)
                {
                    if (p.ReceiverChatStatus != null && p.ReceiverChatStatus == true && p.ReceiverID != currentUser.ChatUserID)
                    {
                        // Update admin
                        if (count == 0 && isChangeOfAdmin)
                        {
                            newAdminID = p.ReceiverID;
                        }

                        if (isChangeOfAdmin && !string.IsNullOrEmpty(newAdminID))
                        {
                            // Update chat for new admin
                            chats[p.ReceiverID].UpdateAdmin(p.ReceiverChatID, newAdminID);
                        }

                        string removeMessage = string.Format("{0} has been removed by {1}.", MessageConverter.HtmlDecode(ChatAccountManager.GetUserName(msg.RemovedMemberID, p.ReceiverID)), MessageConverter.HtmlDecode(ChatAccountManager.GetUserName(currentUser.ChatUserID, p.ReceiverID)));

                        // Create "Remove" conversation
                        recvMessages[count] = removeMessage;
                        recvNotificationIDs[count++] = conversations[p.ReceiverID].CreateRecvConv(currentUser.ChatUserID, msg.ChatID, senderMsgID, p.ReceiverChatID, "MSGPEND", removeMessage, 6);

                        //participants[p.ReceiverID].Remove(p.ReceiverChatID, msg.RemovedMemberID, chatIDOfMemberToBeRemoved);
                        //participants[msg.RemovedMemberID].Remove(chatIDOfMemberToBeRemoved, p.ReceiverID, p.ReceiverChatID);

                        if (!isChangeOfAdmin)
                        {
                            chats[p.ReceiverID].MarkAsUpdated(p.ReceiverChatID);
                        }
                    }

                }

                // Update status of group to false
                chatRepo.UpdateStatus(chatIDOfMemberToBeRemoved, false);

                // Person who got removed
                string removedMessage = string.Format("You have been removed by {0}.", MessageConverter.HtmlDecode(ChatAccountManager.GetUserName(currentUser.ChatUserID, msg.RemovedMemberID)));
                string removedMsgID = conversations[msg.RemovedMemberID].CreateRecvConv(currentUser.ChatUserID, msg.ChatID, senderMsgID, chatIDOfMemberToBeRemoved, "MSGPEND", removedMessage, 6);

                // Update all messages of removed member
                conversations[msg.RemovedMemberID].UpdateStatusBySenderChatId(chatIDOfMemberToBeRemoved, 22);

                string[] allMembers = (from p in allParts where p.ReceiverChatStatus == true select p.ReceiverID).ToArray();
                string[] membersProfileName = new string[allMembers.Length];

                membersProfileName = ChatAccountManager.GetUserProfileName(allMembers);

                //Logger.Info("Members left count: " + allMembers.Count());

                string allMemberIDString = string.Empty;
                string allMemberProfileNameString = string.Empty;

                for (int index = 0; index < allMembers.Count(); index++)
                {
                    string memberID = allMembers[index];

                    if (index != allMembers.Count() - 1)
                    {
                        allMemberIDString += memberID + ',';
                        allMemberProfileNameString += MessageConverter.HtmlEncode(membersProfileName[index]) + ',';
                    }
                    else
                    {
                        allMemberIDString += memberID;
                        allMemberProfileNameString += MessageConverter.HtmlEncode(membersProfileName[index]);
                    }
                }

                // Sending back to current user who remove the member
                GroupRemoveAckMessage gpAck = new GroupRemoveAckMessage()
                {
                    ChatID = msg.ChatID,
                    AdminID = isChangeOfAdmin ? newAdminID : currentChatOfRemovedMember.AdminID,
                    MembersID = allMemberIDString,
                    Timestamp = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff")
                };

                ReplyMessage(currentUser, gpAck);

                // Sending sync to removed member
                GroupRemoveSyncMessage gpSyncForRemoved = new GroupRemoveSyncMessage()
                {
                    ChatID = chatIDOfMemberToBeRemoved,
                    AdminID = isChangeOfAdmin ? newAdminID : currentChatOfRemovedMember.AdminID,
                    MembersID = allMemberIDString,
                    MembersProfileName = allMemberProfileNameString,
                    NotificationID = removedMsgID,
                    NotificationMessage = MessageConverter.HtmlEncode(removedMessage),
                    Timestamp = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff")
                };

                if (onlineUsers[msg.RemovedMemberID] != null)
                {
                    ReplyMessage(onlineUsers[msg.RemovedMemberID], gpSyncForRemoved);
                }
                else
                {
                    RelayServerCommunication.SendSearchUserMessage(msg.RemovedMemberID, gpSyncForRemoved.ToString());
                }
               

                count = 0;

                // Sending to rest of the members
                foreach (var p in allParts)
                {
                    if (p.ReceiverChatStatus != null && p.ReceiverChatStatus == true)
                    {
                        var user = onlineUsers[p.ReceiverID];

                        GroupRemoveSyncMessage gpSync = new GroupRemoveSyncMessage()
                        {
                            ChatID = p.ReceiverChatID,
                            AdminID = isChangeOfAdmin ? newAdminID : currentChatOfRemovedMember.AdminID,
                            MembersID = allMemberIDString,
                            MembersProfileName = allMemberProfileNameString,
                            NotificationID = recvNotificationIDs[count],
                            NotificationMessage = MessageConverter.HtmlEncode(recvMessages[count]),
                            Timestamp = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff")
                        };

                        if (p.ReceiverID != msg.RemovedMemberID && p.ReceiverID != currentUser.ChatUserID)
                        {
                            if (user != null)
                            {
                                ReplyMessage(user, gpSync);
                            }
                            else
                            {
                                RelayServerCommunication.SendSearchUserMessage(p.ReceiverID, gpSync.ToString());
                            }

                            //Logger.Info(gpSync.ToString());


                            count++;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Info(ex.Message.ToString() + " " + ex.StackTrace.ToString());
            }
        }
    }
}
