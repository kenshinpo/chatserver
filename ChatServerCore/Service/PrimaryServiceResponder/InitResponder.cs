﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChatServerCore.Data;
using ChatServerCore.Message;
using ChatServerCore.Utilities;

namespace ChatServerCore.Service.PrimaryServiceResponder
{
    public class InitResponder : MessageResponder
    {
        public InitResponder()
            : base()
        {
            MessageOfInterested = MessageFormat.INIT.First();
        }

        public override void Respond(ChatSessionCollection onlineUsers, ChatSession currentUser, string message)
        {
            try
            {
                InitMessage msg = RealizeMessage<InitMessage>(message);

                if (!string.IsNullOrEmpty(msg.ChatUserID))
                {
                    currentUser.ChatUserID = msg.ChatUserID;

                    var existingUser = onlineUsers[msg.ChatUserID];

                    if (existingUser != null)
                    {
                        onlineUsers.Remove(msg.ChatUserID);
                        existingUser.Transport.Close();
                    }

                    onlineUsers[msg.ChatUserID] = currentUser;

                    Dictionary<string, int> dict = new Dictionary<string, int>();
                    Repositories<UserRepository> users = new Repositories<UserRepository>();
                    dict = users[msg.ChatUserID].InitAccount(msg.CID.Length > 1 ? msg.CID : string.Empty, msg.DeviceToken);

                    if (dict["isValid"] == 1)
                    {
                        RelayServerCommunication.SendClientConnectMessage(msg.ChatUserID);
                    }

                    InitAckMessage ack = new InitAckMessage() { ChatUserID = msg.ChatUserID, IsValid = dict["isValid"], IsCherryAccountLogin = dict["isCherryAccountLogin"] };
                    ReplyMessage(currentUser, ack);
                }
            
            }
            catch (Exception ex)
            {
                Logger.Info(ex.StackTrace.ToString());
            }
           
        }
    }
}
