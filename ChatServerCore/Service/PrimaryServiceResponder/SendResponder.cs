﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using ChatServerCore.Data;
using ChatServerCore.Data.Database;
using ChatServerCore.Data.Entity;
using ChatServerCore.Message;
using ChatServerCore.Utilities;

namespace ChatServerCore.Service.PrimaryServiceResponder
{
    public class SendResponder : MessageResponder
    {
        public SendResponder()
            : base()
        {
            MessageOfInterested = MessageFormat.SEND.First();
        }

        //TODO: Check msg status and CreateSendConv
        public override void Respond(ChatSessionCollection onlineUsers, ChatSession currentUser, string message)
        {
            try
            {
                SendMessage msg = RealizeMessage<SendMessage>(message);

                Queue<string> msgIdQueue = new Queue<string>();
                IEnumerable<Participant> parts;
                string senderMsgID;

                Repositories<ChatRepository> chats = new Repositories<ChatRepository>();
                Repositories<ParticipantRepository> participants = new Repositories<ParticipantRepository>();
                Repositories<ConversationRepository> conversations = new Repositories<ConversationRepository>();
                Repositories<UserRepository> users = new Repositories<UserRepository>();

                var senderChat = chats[currentUser.ChatUserID].GetById(msg.ChatID);
                parts = participants[currentUser.ChatUserID].FindBySenderChatId(msg.ChatID, senderChat.IsGroupChat);

                string decodedMessage = MessageConverter.HtmlDecode(msg.Message);
                bool isReceiverAdmin = false;

                foreach (var p in parts)
                {
                    if (p.ReceiverID.Contains("CCAdmin"))
                    {
                        isReceiverAdmin = true;
                        break;
                    }
                }

                //Logger.Info("Number of participants: " + parts.Count());

                if (isReceiverAdmin && !senderChat.IsGroupChat)
                {
                    senderMsgID = conversations[currentUser.ChatUserID].CreateSendConv(msg.ChatID, null, null, null, "SENTPEND", decodedMessage, msg.Type);
                    Conversation senderMessage = conversations[currentUser.ChatUserID].FindById(senderMsgID).First();
                    SendAckMessage ack = new SendAckMessage()
                    {
                        ChatID = msg.ChatID,
                        MsgID = senderMsgID,
                        Temp = msg.Temp,
                        Timestamp = senderMessage.CreatedOn.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss.fff")
                    };
                    ReplyMessage(currentUser, ack);

                    foreach (var p in parts)
                    {
                        SentMessage reply = new SentMessage() { ChatID = msg.ChatID, MsgID = senderMsgID };
                        ReplyMessage(currentUser, reply);

                        string adminChatUserID = p.ReceiverID;
                        string adminChatID = p.ReceiverChatID;

                        string broadcastedDefaultMessage = ChatAccountManager.GetDefaultMessageFromEvent(adminChatUserID);

                        // Save message sent by client to admin
                        conversations[p.ReceiverID].CreateRecvConv(currentUser.ChatUserID, msg.ChatID, senderMsgID, p.ReceiverChatID, "RECEIVED", decodedMessage, msg.Type);

                        if (!string.IsNullOrEmpty(broadcastedDefaultMessage))
                        {
                            // Replied back to client with default text
                            string adminMsgID = conversations[adminChatUserID].CreateSendConv(p.ReceiverChatID, null, currentUser.ChatUserID, msg.ChatID, "SNDPEND", broadcastedDefaultMessage, 1);

                            Dictionary<string, bool> dict = ChatAccountManager.IsUserBlocked(adminChatUserID, currentUser.ChatUserID);
                            string recvMessageID = string.Empty;

                            if (dict["isBlocked"])
                            {
                                recvMessageID = conversations[currentUser.ChatUserID].CreateRecvConv(adminChatUserID, adminChatID, adminMsgID, msg.ChatID, "BLOCKED", broadcastedDefaultMessage, 1);
                            }
                            else
                            {
                                recvMessageID = conversations[currentUser.ChatUserID].CreateRecvConv(adminChatUserID, adminChatID, adminMsgID, msg.ChatID, "MSGPEND", broadcastedDefaultMessage, 1);

                                Conversation newRecvMessage = conversations[currentUser.ChatUserID].FindById(recvMessageID).First();

                                RecvMessage recv = new RecvMessage()
                                {
                                    ChatID = msg.ChatID,
                                    MsgID = recvMessageID,
                                    SenderChatUserID = adminChatUserID,
                                    Message = MessageConverter.HtmlEncode(broadcastedDefaultMessage),
                                    Type = 1,
                                    Timestamp = newRecvMessage.CreatedOn.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss.fff")
                                };
                                ReplyMessage(onlineUsers[currentUser.ChatUserID], recv);
                            }
                        }
                    }
                }
                else
                {
                    if (senderChat.IsGroupChat)
                    {
                        senderMsgID = conversations[currentUser.ChatUserID].CreateSendConv(msg.ChatID, null, null, null, "GROUPMSG", decodedMessage, msg.Type);
                    }
                    else
                    {
                        senderMsgID = conversations[currentUser.ChatUserID].CreateSendConv(msg.ChatID, null, parts.First().ReceiverID, parts.First().ReceiverChatID, "SNDPEND", decodedMessage, msg.Type);
                    }

                    foreach (var p in parts)
                    {
                        if (p.ReceiverChatStatus != null && p.ReceiverChatStatus == true)
                        {
                            //Logger.Info(string.Format("{0} is true", p.ReceiverID));

                            string notificationName = string.Empty;
                            Dictionary<string, bool> dict = ChatAccountManager.IsUserBlocked(currentUser.ChatUserID, p.ReceiverID);
                            string recvMessageID = string.Empty;

                            // Check if group chat
                            if (senderChat.IsGroupChat)
                            {
                                Chat recvChat = chats[p.ReceiverID].GetById(p.ReceiverChatID);
                                notificationName = string.Format("{0} @ {1}", ChatAccountManager.GetUserName(currentUser.ChatUserID, p.ReceiverID), senderChat.Title);

                                //Logger.Info(notificationName);
                                recvMessageID = conversations[p.ReceiverID].CreateRecvConv(currentUser.ChatUserID, msg.ChatID, senderMsgID, p.ReceiverChatID, "MSGPEND", decodedMessage, msg.Type);


                                users[p.ReceiverID].IncrementBadge();

                                Conversation newRecvMessage = conversations[p.ReceiverID].FindById(recvMessageID).First();

                                //Logger.Info(msg.Message);

                                if (onlineUsers[p.ReceiverID] == null)
                                {
                                    //Logger.Info("User not found -> sent to RS");

                                    var userRepo = users[p.ReceiverID];
                                    int operatingType = userRepo.GetOperatingType();
                                    string deviceToken = userRepo.GetDeviceToken();
                                    int badge = userRepo.GetBadge();
                                    string chatID = p.ReceiverChatID;

                                    string notification = string.Empty;

                                    if (dict["isNotificationAllow"])
                                    {
                                        if (msg.Type == 1)
                                        {
                                            notification = MessageConverter.HtmlEncode(String.Format("{0}: {1}", notificationName, decodedMessage));
                                        }
                                        else if (msg.Type == 2)
                                        {
                                            notification = MessageConverter.HtmlEncode(String.Format("{0} {1}", notificationName, String.Format("sent you an image!")));
                                        }
                                        else if (msg.Type == 3)
                                        {
                                            notification = MessageConverter.HtmlEncode(String.Format("{0} {1}", notificationName, String.Format("sent you a sticker!")));
                                        }
                                        else if (msg.Type == 4)
                                        {
                                            notification = MessageConverter.HtmlEncode(String.Format("{0} {1}", notificationName, String.Format("sent you a gift!")));
                                        }
                                        else if (msg.Type == 5)
                                        {
                                            notification = MessageConverter.HtmlEncode(String.Format("{0} {1}", notificationName, String.Format("sent you an invitation!")));
                                        }
                                    }

                                    RSRecvMessage rsRecv = new RSRecvMessage()
                                    {
                                        ChatID = p.ReceiverChatID,
                                        MsgID = recvMessageID,
                                        SenderChatUserID = currentUser.ChatUserID,
                                        Message = MessageConverter.HtmlEncode(decodedMessage),
                                        Type = msg.Type,
                                        Timestamp = newRecvMessage.CreatedOn.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss.fff"),
                                        NotificationMessage = string.IsNullOrEmpty(notification) ? " " : notification,
                                        Badge = badge,
                                        OperatingType = operatingType,
                                        DeviceToken = string.IsNullOrWhiteSpace(deviceToken) ? " " : deviceToken
                                    };

                                    RelayServerCommunication.SendSearchUserMessage(p.ReceiverID, rsRecv.ToString());
                                }
                                else
                                {
                                    //Logger.Info("User found -> no need sent to RS");
                                    //Logger.Info("Converted text: " + MessageConverter.HtmlEncode(msg.Message));
                                    RecvMessage recv = new RecvMessage()
                                    {
                                        ChatID = p.ReceiverChatID,
                                        MsgID = recvMessageID,
                                        SenderChatUserID = currentUser.ChatUserID,
                                        Message = MessageConverter.HtmlEncode(decodedMessage),
                                        Type = msg.Type,
                                        Timestamp = newRecvMessage.CreatedOn.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss.fff")
                                    };
                                    ReplyMessage(onlineUsers[p.ReceiverID], recv);
                                }
                            }
                            else
                            {
                                notificationName = string.Format("{0}", ChatAccountManager.GetUserName(currentUser.ChatUserID, p.ReceiverID));

                                //Logger.Info(notificationName);

                                if (dict["isBlocked"])
                                {
                                    recvMessageID = conversations[p.ReceiverID].CreateRecvConv(currentUser.ChatUserID, msg.ChatID, senderMsgID, p.ReceiverChatID, "BLOCKED", decodedMessage, msg.Type);
                                }
                                else
                                {
                                    recvMessageID = conversations[p.ReceiverID].CreateRecvConv(currentUser.ChatUserID, msg.ChatID, senderMsgID, p.ReceiverChatID, "MSGPEND", decodedMessage, msg.Type);

                                    users[p.ReceiverID].IncrementBadge();

                                    Conversation newRecvMessage = conversations[p.ReceiverID].FindById(recvMessageID).First();

                                    //Logger.Info(msg.Message);

                                    if (onlineUsers[p.ReceiverID] == null)
                                    {
                                        //Logger.Info("User not found -> sent to RS");

                                        var userRepo = users[p.ReceiverID];
                                        int operatingType = userRepo.GetOperatingType();
                                        string deviceToken = userRepo.GetDeviceToken();
                                        int badge = userRepo.GetBadge();
                                        string chatID = p.ReceiverChatID;

                                        string notification = string.Empty;

                                        if (dict["isNotificationAllow"])
                                        {
                                            if (msg.Type == 1)
                                            {
                                                notification = MessageConverter.HtmlEncode(String.Format("{0}: {1}", notificationName, decodedMessage));
                                            }
                                            else if (msg.Type == 2)
                                            {
                                                notification = MessageConverter.HtmlEncode(String.Format("{0} {1}", notificationName, String.Format("sent you an image!")));
                                            }
                                            else if (msg.Type == 3)
                                            {
                                                notification = MessageConverter.HtmlEncode(String.Format("{0} {1}", notificationName, String.Format("sent you a sticker!")));
                                            }
                                            else if (msg.Type == 4)
                                            {
                                                notification = MessageConverter.HtmlEncode(String.Format("{0} {1}", notificationName, String.Format("sent you a gift!")));
                                            }
                                            else if (msg.Type == 5)
                                            {
                                                notification = MessageConverter.HtmlEncode(String.Format("{0} {1}", notificationName, String.Format("sent you an invitation!")));
                                            }
                                        }

                                        RSRecvMessage rsRecv = new RSRecvMessage()
                                        {
                                            ChatID = p.ReceiverChatID,
                                            MsgID = recvMessageID,
                                            SenderChatUserID = currentUser.ChatUserID,
                                            Message = MessageConverter.HtmlEncode(decodedMessage),
                                            Type = msg.Type,
                                            Timestamp = newRecvMessage.CreatedOn.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss.fff"),
                                            NotificationMessage = string.IsNullOrEmpty(notification) ? " " : notification,
                                            Badge = badge,
                                            OperatingType = operatingType,
                                            DeviceToken = string.IsNullOrWhiteSpace(deviceToken) ? " " : deviceToken
                                        };

                                        RelayServerCommunication.SendSearchUserMessage(p.ReceiverID, rsRecv.ToString());
                                    }
                                    else
                                    {
                                        //Logger.Info("User found -> no need sent to RS");
                                        //Logger.Info("Converted text: " + MessageConverter.HtmlEncode(msg.Message));
                                        RecvMessage recv = new RecvMessage()
                                        {
                                            ChatID = p.ReceiverChatID,
                                            MsgID = recvMessageID,
                                            SenderChatUserID = currentUser.ChatUserID,
                                            Message = MessageConverter.HtmlEncode(decodedMessage),
                                            Type = msg.Type,
                                            Timestamp = newRecvMessage.CreatedOn.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss.fff")
                                        };
                                        ReplyMessage(onlineUsers[p.ReceiverID], recv);
                                    }
                                }
                            }


                        }
                        else
                        {
                            Logger.Info(string.Format("{0} is false", p.ReceiverID));
                        }

                    }

                    Conversation senderMessage = conversations[currentUser.ChatUserID].FindById(senderMsgID).First();

                    SendAckMessage ack = new SendAckMessage()
                    {
                        ChatID = msg.ChatID,
                        MsgID = senderMsgID,
                        Temp = msg.Temp,
                        Timestamp = senderMessage.CreatedOn.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss.fff")
                    };

                    ReplyMessage(currentUser, ack);
                }

            }
            catch (SqlException ex)
            {
                Logger.Info(ex.Message.ToString() + " " + ex.StackTrace.ToString());
                //Respond(onlineUsers, currentUser, message);
            }
            catch (Exception ex)
            {
                Logger.Info(ex.Message.ToString() + " " + ex.StackTrace.ToString());
            }

        }
    }
}
