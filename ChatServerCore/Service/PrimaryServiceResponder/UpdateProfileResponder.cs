﻿using System;
using System.Collections;
using System.Linq;
using ChatServerCore.Data.Database;
using ChatServerCore.Message;
using ChatServerCore.Utilities;

namespace ChatServerCore.Service.PrimaryServiceResponder
{
    public class UpdateProfileResponder : MessageResponder
    {
        public UpdateProfileResponder()
            : base()
        {
            MessageOfInterested = MessageFormat.UPDATEPROFILE.First();
        }

        public override void Respond(ChatSessionCollection onlineUsers, ChatSession currentUser, string message)
        {
            try
            {
                // current user in this case refers to web service
                UpdateProfileMessage msg = RealizeMessage<UpdateProfileMessage>(message);

                var primaryOnlineUsers = MessageServiceFactory.Instances["Primary Service"].OnlineUsers;

                // Get the friends of current user whose profile is being updated
                ArrayList friendList = ChatAccountManager.GetFriendsForUser(msg.ChatUserID);

                foreach (string chatUserID in friendList)
                {
                    // Reply back to friends
                    if (primaryOnlineUsers[chatUserID] != null)
                    {
                        ReplyMessage(primaryOnlineUsers[chatUserID], msg);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Info(ex.StackTrace.ToString());
            }
        }
    }
}
