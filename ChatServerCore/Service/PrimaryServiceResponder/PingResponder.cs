﻿using System.Linq;
using ChatServerCore.Message;

namespace ChatServerCore.Service.PrimaryServiceResponder
{
    public class PingResponder : MessageResponder
    {
        public PingResponder()
            : base()
        {
            MessageOfInterested = MessageFormat.PING.First();
        }

        public override void Respond(ChatSessionCollection onlineUsers, ChatSession currentUser, string message)
        {
            ReplyMessage(currentUser, new OkMessage());
        }
    }
}
