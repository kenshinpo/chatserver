﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ChatServerCore.Data;
using ChatServerCore.Data.Entity;
using ChatServerCore.Message;
using ChatServerCore.Utilities;

namespace ChatServerCore.Service.PrimaryServiceResponder
{
    public class FetchConversationPerChatResponder : MessageResponder
    {
        public FetchConversationPerChatResponder()
            : base()
        {
            MessageOfInterested = MessageFormat.FETCHCONVERSATION.First();
        }

        public override void Respond(ChatSessionCollection onlineUsers, ChatSession currentUser, string message)
        {
            try
            {
                FetchConversationMessage msg = RealizeMessage<FetchConversationMessage>(message);
                IEnumerable<Conversation> outdatedConv = null;

                Repositories<ChatRepository> chats = new Repositories<ChatRepository>();
                Repositories<ConversationRepository> conversations = new Repositories<ConversationRepository>();

                var convRepo = conversations[currentUser.ChatUserID];
                outdatedConv = convRepo.FindByAllNewConversationByChatID(msg.ChatID, msg.Timestamp);

                string syncAppendMessage = "<@STARTCHATSYNC>";

                foreach (var oc in outdatedConv)
                {
                    // Image
                    if (oc.Type == 2)
                    {
                        ChatRecvMediaMessage newMessage = new ChatRecvMediaMessage
                        {
                            ChatID = oc.ReceiverChatID,
                            MsgID = oc.ConversationID,
                            SenderChatUserID = oc.SenderID,
                            Message = oc.Message,
                            Type = oc.Type,
                            Timestamp = oc.CreatedOn.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss.fff")
                        };

                        syncAppendMessage += newMessage;
                    }
                    // Gift
                    else if (oc.Type == 4)
                    {
                        //if (oc.Status.StartsWith("GIFT_MSGPEND"))
                        if (oc.StatusCode == 17)
                        {
                            ChatRecvGiftMessage newMessage = new ChatRecvGiftMessage
                            {
                                ChatID = oc.ReceiverChatID,
                                MsgID = oc.ConversationID,
                                SenderChatUserID = oc.SenderID,
                                Message = oc.Message,
                                Type = oc.Type,
                                Timestamp = oc.CreatedOn.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss.fff")
                            };

                            syncAppendMessage += newMessage;
                        }
                        //else if (oc.Status.StartsWith("GIFT_SND"))
                        else if (oc.StatusCode == 14)
                        {
                            ChatSendGiftMessage newMessage = new ChatSendGiftMessage
                            {
                                ChatID = oc.SenderChatID,
                                MsgID = oc.SenderConversationID,
                                RecvChatUserID = oc.ReceiverID,
                                Message = oc.Message,
                                Type = oc.Type,
                                Timestamp = oc.CreatedOn.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss.fff")
                            };
                            syncAppendMessage += newMessage;
                        }
                        //else if (oc.Status.StartsWith("GIFT_!SNTPEND") || (oc.Status.StartsWith("GIFT_SNTPEND")))
                        else if (oc.StatusCode == 19 || oc.StatusCode == 18)
                        {
                            ChatSentGiftMessage newMessage = new ChatSentGiftMessage
                            {
                                ChatID = oc.SenderChatID,
                                MsgID = oc.SenderConversationID,
                                RecvChatUserID = oc.ReceiverID,
                                Message = oc.Message,
                                Type = oc.Type,
                                Timestamp = oc.CreatedOn.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss.fff")
                            };
                            syncAppendMessage += newMessage;
                        }
                    }
                    // Invitation
                    else if (oc.Type == 5)
                    {
                        //if (oc.Status.StartsWith("INVITE_MSGPEND"))
                        if (oc.StatusCode == 10)
                        {
                            ChatRecvInviteMessage newMessage = new ChatRecvInviteMessage
                            {
                                ChatID = oc.ReceiverChatID,
                                MsgID = oc.ConversationID,
                                SenderChatUserID = oc.SenderID,
                                Message = oc.Message,
                                Type = oc.Type,
                                Timestamp = oc.CreatedOn.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss.fff")
                            };
                            syncAppendMessage += newMessage;
                        }
                        //else if (oc.Status.StartsWith("INVITE_SND"))
                        else if (oc.StatusCode == 7)
                        {
                            ChatSendInviteMessage newMessage = new ChatSendInviteMessage
                            {
                                ChatID = oc.SenderChatID,
                                MsgID = oc.ConversationID,
                                RecvChatUserID = oc.ReceiverID,
                                Message = oc.Message,
                                Type = oc.Type,
                                Timestamp = oc.CreatedOn.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss.fff")
                            };
                            syncAppendMessage += newMessage;
                        }
                        //else if (oc.Status.StartsWith("INVITE_!SNTPEND") || (oc.Status.StartsWith("INVITE_SNTPEND")))
                        else if (oc.StatusCode == 12 || oc.StatusCode == 11)
                        {
                            ChatSentInviteMessage newMessage = new ChatSentInviteMessage
                            {
                                ChatID = oc.SenderChatID,
                                MsgID = oc.ConversationID,
                                RecvChatUserID = oc.ReceiverID,
                                Message = oc.Message,
                                Type = oc.Type,
                                Timestamp = oc.CreatedOn.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss.fff")
                            };
                            syncAppendMessage += newMessage;
                        }
                    }
                    // Plain text or sticker
                    else
                    {
                        //if (oc.Status.StartsWith("MSGPEND"))
                        if (oc.StatusCode == 4)
                        {
                            ChatRecvMessage newMessage = new ChatRecvMessage
                            {
                                ChatID = oc.ReceiverChatID,
                                MsgID = oc.ConversationID,
                                SenderChatUserID = oc.SenderID,
                                Message = MessageConverter.HtmlEncode(oc.Message),
                                Type = oc.Type,
                                Timestamp = oc.CreatedOn.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss.fff")
                            };
                            syncAppendMessage += newMessage;
                        }
                        //else if (oc.Status.StartsWith("SENTPEND"))
                        else if (oc.StatusCode == 2)
                        {
                            ChatSentMessage newMessage = new ChatSentMessage
                            {
                                ChatID = oc.SenderChatID,
                                MsgID = oc.ConversationID
                            };
                            syncAppendMessage += newMessage;
                        }

                    }
                }

                syncAppendMessage += "<@ENDCHATSYNC><@EOF>";

                ReplyMessage(currentUser, syncAppendMessage);
            }
            catch (Exception ex)
            {
                Logger.Info(ex.StackTrace.ToString());
            }
        }
    }
}
