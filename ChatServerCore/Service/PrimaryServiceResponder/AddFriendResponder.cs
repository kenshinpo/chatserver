﻿using System;
using System.Linq;
using ChatServerCore.Message;
using ChatServerCore.Utilities;

namespace ChatServerCore.Service.PrimaryServiceResponder
{
    public class AddFriendResponder : MessageResponder
    {
        public AddFriendResponder()
            : base()
        {
            MessageOfInterested = MessageFormat.ADDFRIEND.First();
        }

        public override void Respond(ChatSessionCollection onlineUsers, ChatSession currentUser, string message)
        {
            try
            {
                // current user in this case refers to web service
                AddFriendMessage msg = RealizeMessage<AddFriendMessage>(message);

                var primaryOnlineUsers = MessageServiceFactory.Instances["Primary Service"].OnlineUsers;

                // Reply back to friends
                if (primaryOnlineUsers[msg.ChatUserID] != null)
                {
                    ReplyMessage(primaryOnlineUsers[msg.ChatUserID], msg);
                }
            }
            catch (Exception ex)
            {
                Logger.Info(ex.StackTrace.ToString());
            }

        }
    }
}
