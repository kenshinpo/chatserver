﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChatServerCore.Data;
using ChatServerCore.Data.Database;
using ChatServerCore.Data.Entity;
using ChatServerCore.Message;
using ChatServerCore.Utilities;

namespace ChatServerCore.Service.PrimaryServiceResponder
{
    public class SyncResponder : MessageResponder
    {
        public SyncResponder()
            : base()
        {
            MessageOfInterested = MessageFormat.SYNC.First();
        }

        public override void Respond(ChatSessionCollection onlineUsers, ChatSession currentUser, string message)
        {
            try
            {
                SyncMessage msg = RealizeMessage<SyncMessage>(message);

                Logger.Info(msg.ToString());

                IEnumerable<Chat> outdatedChat = null;
                Queue<IEnumerable<Participant>> outdatedParts = new Queue<IEnumerable<Participant>>();
                IEnumerable<LastConversation> outdatedConv = null;
                Queue<DateTime> timestamp = new Queue<DateTime>();

                Repositories<ChatRepository> chats = new Repositories<ChatRepository>();
                Repositories<ParticipantRepository> participants = new Repositories<ParticipantRepository>();
                Repositories<ConversationRepository> conversations = new Repositories<ConversationRepository>();
                Repositories<UserRepository> users = new Repositories<UserRepository>();

                outdatedChat = chats[currentUser.ChatUserID].FindGroupSince(msg.Timestamp);

                var partRepo = participants[currentUser.ChatUserID];

                // Reset badge to 0
                users[currentUser.ChatUserID].ClearBadge();

                foreach (var oc in outdatedChat)
                {
                    outdatedParts.Enqueue(partRepo.FindBySenderChatId(oc.ChatID, true));
                    timestamp.Enqueue(oc.LastModified);
                }

                var convRepo = conversations[currentUser.ChatUserID];
                outdatedConv = convRepo.FindByLastPendingRecv(msg.Timestamp);

                string syncAppendMessage = "<@STARTSYNC>";

                foreach (var oc in outdatedChat)
                {
                    string[] members = null;

                    if (!oc.Status)
                    {
                        members = (from op in outdatedParts.Dequeue() select op.ReceiverID).ToArray();
                    }
                    else
                    {
                        members = (from op in outdatedParts.Dequeue() where op.ReceiverChatStatus == true select op.ReceiverID)
                                  .Concat(new string[] { currentUser.ChatUserID }).ToArray();
                    }
                   

                    string[] membersProfileName = new string[members.Length];

                    membersProfileName = ChatAccountManager.GetUserProfileName(members);

                    string allMemberIDString = string.Empty;
                    string allMemberProfileNameString = string.Empty;

                    for (int index = 0; index < members.Count(); index++)
                    {
                        if (index != members.Count() - 1)
                        {
                            allMemberIDString += members[index] + ',';
                            allMemberProfileNameString += MessageConverter.HtmlEncode(membersProfileName[index]) + ',';
                        }
                        else
                        {
                            allMemberIDString += members[index];
                            allMemberProfileNameString += MessageConverter.HtmlEncode(membersProfileName[index]);
                        }
                    }

                    GroupLastSyncMessage gpSync = new GroupLastSyncMessage()
                    {
                        ChatID = oc.ChatID,
                        Title = MessageConverter.HtmlEncode(oc.Title),
                        MembersID = allMemberIDString,
                        MembersProfileName = allMemberProfileNameString,
                        GroupPicURL = (!string.IsNullOrEmpty(oc.GroupPicURL) ? oc.GroupPicURL : " "),
                        AdminID = oc.AdminID,
                        Timestamp = timestamp.Dequeue().ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss.fff")
                    };

                    syncAppendMessage += gpSync;
                }

                foreach (var oc in outdatedConv)
                {
                    if (oc.Type == 4 || oc.Type == 5)
                    {
                        // For game gift/invite which this user sent
                        ChatGameSyncMessage gpSync = new ChatGameSyncMessage()
                        {
                            ChatID = currentUser.ChatUserID == oc.SenderID ? oc.SenderChatID : oc.ReceiverChatID,
                            ReceiverChatUserID = oc.ReceiverID,
                            SenderChatUserID = oc.SenderID,
                            Message = MessageConverter.HtmlEncode(oc.Message),
                            Type = oc.Type,
                            UnreadCount = oc.UnreadCount,
                            Timestamp = oc.CreatedOn.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss.fff")
                        };
                        syncAppendMessage += gpSync;
                    }
                    else
                    {
                        // For all other messages which this user received
                        ChatSyncMessage gpSync = new ChatSyncMessage()
                        {
                            ChatID = oc.ReceiverChatID,
                            SenderChatUserID = oc.SenderID,
                            Message = MessageConverter.HtmlEncode(oc.Message),
                            Type = oc.Type,
                            UnreadCount = oc.UnreadCount,
                            Timestamp = oc.CreatedOn.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss.fff")
                        };
                        syncAppendMessage += gpSync;
                    }
                }

                syncAppendMessage += "<@ENDSYNC><@EOF>";

                ReplyMessage(currentUser, syncAppendMessage);

                //Logger.Info("END OF SYNC");
            }
            catch (Exception ex)
            {
                Logger.Info(ex.Message.ToString() + " " + ex.StackTrace.ToString());
            }
           
        }
    }
}
