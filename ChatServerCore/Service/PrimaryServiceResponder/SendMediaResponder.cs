﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChatServerCore.Data;
using ChatServerCore.Data.Database;
using ChatServerCore.Data.Entity;
using ChatServerCore.Message;
using ChatServerCore.Utilities;

namespace ChatServerCore.Service.PrimaryServiceResponder
{
    public class SendMediaResponder : MessageResponder
    {
        public SendMediaResponder()
            : base()
        {
            MessageOfInterested = MessageFormat.SENDMEDIA.First();
        }

        //TODO: Check msg status and CreateSendConv
        public override void Respond(ChatSessionCollection onlineUsers, ChatSession currentUser, string message)
        {
            try
            {
                SendMediaMessage msg = RealizeMessage<SendMediaMessage>(message);

                Queue<string> msgIdQueue = new Queue<string>();
                IEnumerable<Participant> parts;
                string senderMsgID;

                var chats = new Repositories<ChatRepository>();
                var participants = new Repositories<ParticipantRepository>();
                var conversations = new Repositories<ConversationRepository>();
                var users = new Repositories<UserRepository>();


                var senderChat = chats[currentUser.ChatUserID].GetById(msg.ChatID);
                parts = participants[currentUser.ChatUserID].FindBySenderChatId(msg.ChatID, senderChat.IsGroupChat);

                if (senderChat.IsGroupChat)
                {
                    senderMsgID = conversations[currentUser.ChatUserID].CreateSendConv(msg.ChatID, null, null, null, "GROUPMSG", msg.Message, msg.Type);
                }
                else
                {
                    senderMsgID = conversations[currentUser.ChatUserID].CreateSendConv(msg.ChatID, null, parts.First().ReceiverID, parts.First().ReceiverChatID, "SNDPEND", msg.Message, msg.Type);
                }


                foreach (var p in parts)
                {
                    Dictionary<string, bool> dict = ChatAccountManager.IsUserBlocked(currentUser.ChatUserID, p.ReceiverID);
                    string recvMessageID = string.Empty;

                    if (dict["isBlocked"])
                    {
                        recvMessageID = conversations[p.ReceiverID].CreateRecvConv(currentUser.ChatUserID, msg.ChatID, senderMsgID, p.ReceiverChatID, "BLOCKED", msg.Message, msg.Type);
                    }
                    else
                    {
                        recvMessageID = conversations[p.ReceiverID].CreateRecvConv(currentUser.ChatUserID, msg.ChatID, senderMsgID, p.ReceiverChatID, "MSGPEND", msg.Message, msg.Type);

                        users[p.ReceiverID].IncrementBadge();

                        Conversation newRecvMessage = conversations[p.ReceiverID].FindById(recvMessageID).First();

                        if (onlineUsers[p.ReceiverID] == null)
                        {
                            var userRepo = users[p.ReceiverID];
                            int operatingType = userRepo.GetOperatingType();
                            string deviceToken = userRepo.GetDeviceToken();
                            int badge = userRepo.GetBadge();
                            string chatID = p.ReceiverChatID;

                            string notification = string.Empty;

                            if (dict["isNotificationAllow"])
                            {
                                notification = MessageConverter.HtmlEncode(String.Format("{0} {1}", ChatAccountManager.GetUserName(currentUser.ChatUserID, p.ReceiverID), String.Format("sent you an image!")));
                            }

                            RSRecvMediaMessage rsRecvMedia = new RSRecvMediaMessage()
                            {
                                ChatID = p.ReceiverChatID,
                                MsgID = recvMessageID,
                                SenderChatUserID = currentUser.ChatUserID,
                                Message = msg.Message,
                                Type = msg.Type,
                                Timestamp = newRecvMessage.CreatedOn.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss.fff"),
                                NotificationMessage = string.IsNullOrEmpty(notification) ? " " : notification,
                                Badge = badge,
                                OperatingType = operatingType,
                                DeviceToken = string.IsNullOrWhiteSpace(deviceToken) ? " " : deviceToken
                            };

                            RelayServerCommunication.SendSearchUserMessage(p.ReceiverID, rsRecvMedia.ToString());

                        }
                        else
                        {
                            RecvMediaMessage recv = new RecvMediaMessage()
                            {
                                ChatID = p.ReceiverChatID,
                                MsgID = recvMessageID,
                                SenderChatUserID = currentUser.ChatUserID,
                                Message = msg.Message,
                                Type = msg.Type,
                                Timestamp = newRecvMessage.CreatedOn.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss.fff")
                            };
                            ReplyMessage(onlineUsers[p.ReceiverID], recv);
                        }
                    }
                }

                Conversation senderMessage = conversations[currentUser.ChatUserID].FindById(senderMsgID).First();
                SendAckMessage ack = new SendAckMessage()
                {
                    ChatID = msg.ChatID,
                    MsgID = senderMsgID,
                    Temp = msg.Temp,
                    Timestamp = senderMessage.CreatedOn.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss.fff")
                };
                ReplyMessage(currentUser, ack);
            }
            catch (Exception ex)
            {
                Logger.Info(ex.StackTrace.ToString());
            }

        }
    }
}
