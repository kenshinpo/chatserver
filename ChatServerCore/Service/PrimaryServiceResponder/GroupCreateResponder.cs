﻿using System;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using ChatServerCore.Data;
using ChatServerCore.Data.Database;
using ChatServerCore.Message;
using ChatServerCore.Utilities;

namespace ChatServerCore.Service.PrimaryServiceResponder
{
    public class GroupCreateResponder : MessageResponder
    {
        public GroupCreateResponder()
            : base()
        {
            MessageOfInterested = MessageFormat.GROUPCREATE.First();
        }

        public override void Respond(ChatSessionCollection onlineUsers, ChatSession currentUser, string message)
        {
            try
            {
                GroupCreateMessage msg = RealizeMessage<GroupCreateMessage>(message);
                string[] members = msg.Members.Split(',').Concat(new string[] { currentUser.ChatUserID }).ToArray();
                string[] memberChatId = new string[members.Length];
                string[] membersProfileName = new string[members.Length];
                string allMemberIDString = string.Empty;
                string allMemberProfileNameString = string.Empty;
                string senderChatID = string.Empty;

                membersProfileName = ChatAccountManager.GetUserProfileName(members);

                Repositories<ChatRepository> chats = new Repositories<ChatRepository>();
                Repositories<ConversationRepository> conversations = new Repositories<ConversationRepository>();
                Repositories<ParticipantRepository> participants = new Repositories<ParticipantRepository>();

                // Create chat for corresponding members
                for (int index = 0; index < members.Count(); index++)
                {
                    // Creating chat in DB with null for group pic
                    string chatID = chats[members[index]].Create(true, MessageConverter.HtmlDecode(msg.Title), null, currentUser.ChatUserID);
                    memberChatId[index] = chatID;

                    if (members[index] == currentUser.ChatUserID)
                    {
                        senderChatID = chatID;
                    }

                    // Consolidate names and IDs to string
                    if (index != members.Count() - 1)
                    {
                        allMemberIDString += members[index] + ',';
                        allMemberProfileNameString += MessageConverter.HtmlEncode(membersProfileName[index]) + ',';
                    }
                    else
                    {
                        allMemberIDString += members[index];
                        allMemberProfileNameString += MessageConverter.HtmlEncode(membersProfileName[index]);
                    }
                }

                // Create participant for corresponding pair of members
                for (int i = 0; i < members.Length; i++)
                {
                    for (int j = 0; j < members.Length; j++)
                    {
                        if (members[i] == members[j]) continue;
                        participants[members[i]].Add(memberChatId[i], memberChatId[j], members[j], true);
                    }
                }

                // Create "Create" and "Join" conversations for sender
                string[] correspondingChatUserIDs = new string[members.Count()];
                string[] senderMessageIDs = new string[members.Count()];

                senderMessageIDs[0] = conversations[currentUser.ChatUserID].CreateSendConv(senderChatID, null, null, null, "GROUPMSG", "You have created this group.", 6);

                for (int index = 1; index - 1 < members.Count(); index++)
                {
                    if (!members[index - 1].Equals(currentUser.ChatUserID))
                    {
                        senderMessageIDs[index] = conversations[currentUser.ChatUserID].CreateSendConv(senderChatID, null, null, null, "GROUPMSG", string.Format("{0} has joined.",  MessageConverter.HtmlDecode(ChatAccountManager.GetUserName(members[index - 1], currentUser.ChatUserID))), 6);
                        correspondingChatUserIDs[index] = members[index - 1];
                    }
                   
                }

                // Create "Create" and "Join" conversations for receivers

                // Loop through members
                for (int i = 0; i < members.Count(); i++)
                {
                    if (!members[i].Equals(currentUser.ChatUserID))
                    {
                        // Loop through conversations
                        for(int j = 0; j < senderMessageIDs.Count(); j++)
                        {
                            string creationMessage = string.Empty;

                            if (j == 0)
                            {
                                creationMessage = string.Format("{0} has created this group.", MessageConverter.HtmlDecode(ChatAccountManager.GetUserName(currentUser.ChatUserID, members[i])));
                                Thread.Sleep(1);
                            }
                            else
                            {
                                if (correspondingChatUserIDs[j].Equals(members[i]))
                                {
                                    creationMessage = "You have joined.";
                                }
                                else
                                {
                                    creationMessage = string.Format("{0} has joined.", MessageConverter.HtmlDecode(ChatAccountManager.GetUserName(correspondingChatUserIDs[j], members[i])));
                                }
                            }

                            conversations[members[i]].CreateRecvConv(currentUser.ChatUserID, senderChatID, senderMessageIDs[j], memberChatId[i], "MSGPEND", creationMessage, 6);
                        }
                       
                    }
                }

                for (int index = 0; index < members.Length; index++)
                {
                    var user = onlineUsers[members[index]];

                    if (user != null && user.ChatUserID != currentUser.ChatUserID)
                    {
                        //Logger.Info("Group Sync Member for Create: " + user.ChatUserID);

                        GroupSyncMessage sync = new GroupSyncMessage()
                        {
                            ChatID = memberChatId[index],
                            Title = MessageConverter.HtmlEncode(msg.Title),
                            GroupPicURL = " ",
                            AdminID = currentUser.ChatUserID,
                            MembersID = allMemberIDString,
                            MembersProfileName = allMemberProfileNameString,
                            Timestamp = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff")
                        };
                        ReplyMessage(user, sync);
                    }

                    else if (user != null && user.ChatUserID == currentUser.ChatUserID)
                    {
                        //Logger.Info("Creator: " + currentUser.ChatUserID);

                        GroupCreateAckMessage ack = new GroupCreateAckMessage()
                        {
                            ChatID = memberChatId[index],
                            //Title = msg.Title,
                            //Admin = currentUser.Id,
                            //Members = members,
                            Temp = msg.Temp,
                            MembersID = allMemberIDString,
                            MembersProfileName = allMemberProfileNameString,
                            Timestamp = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff")
                        };
                        ReplyMessage(currentUser, ack);
                    }

                    else if (user == null && members[index] != currentUser.ChatUserID)
                    {
                        GroupSyncMessage sync = new GroupSyncMessage()
                        {
                            ChatID = memberChatId[index],
                            Title = MessageConverter.HtmlEncode(msg.Title),
                            GroupPicURL = " ",
                            AdminID = currentUser.ChatUserID,
                            MembersID = allMemberIDString,
                            MembersProfileName = allMemberProfileNameString,
                            Timestamp = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff")
                        };

                        RelayServerCommunication.SendSearchUserMessage(members[index], sync.ToString());
                    }
                }

            }
            catch (SqlException ex)
            {
                Logger.Info(ex.Message.ToString() + " " + ex.StackTrace.ToString());
                //Respond(onlineUsers, currentUser, message);
            }
            catch (Exception ex)
            {
                Logger.Info(ex.Message.ToString() + " " + ex.StackTrace.ToString());
                //Respond(onlineUsers, currentUser, message);
            }
        }

    }
}
