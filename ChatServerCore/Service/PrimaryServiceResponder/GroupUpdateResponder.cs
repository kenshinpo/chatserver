﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChatServerCore.Data;
using ChatServerCore.Data.Database;
using ChatServerCore.Data.Entity;
using ChatServerCore.Message;
using ChatServerCore.Utilities;

namespace ChatServerCore.Service.PrimaryServiceResponder
{
    public class GroupUpdateResponder : MessageResponder
    {
        public GroupUpdateResponder()
            : base()
        {
            MessageOfInterested = MessageFormat.GROUPUPDATE.First();
        }

        public override void Respond(ChatSessionCollection onlineUsers, ChatSession currentUser, string message)
        {
            try
            {
                GroupUpdateMessage msg = RealizeMessage<GroupUpdateMessage>(message);
                Chat currentChat = null;

                IEnumerable<Participant> receivers = null;

                Repositories<ChatRepository> chats = new Repositories<ChatRepository>();
                Repositories<ConversationRepository> conversations = new Repositories<ConversationRepository>();
                Repositories<ParticipantRepository> participants = new Repositories<ParticipantRepository>();

                string senderUpdatePicMessage = string.Empty;
                string senderUpdateTitleMessage = string.Empty;

                List<string> removedRecvForUpdatePic = new List<string>();
                List<string> removedRecvForUpdateTitle = new List<string>();

                receivers = participants[currentUser.ChatUserID].FindBySenderChatId(msg.ChatID, true);
                string[] recvUpdatePicMessageIDs = new string[receivers.Count()];
                string[] recvUpdateTitleMessageIDs = new string[receivers.Count()];
                string[] recvUpdatePicMessages = new string[receivers.Count()];
                string[] recvUpdateTitleMessages = new string[receivers.Count()];

                currentChat = chats[currentUser.ChatUserID].GetById(msg.ChatID);

                int count = 0;

                // Update image if its not null
                if (!msg.GroupPicURL.Equals(" ") || msg.GroupPicURL.Equals("null"))
                {
                    Logger.Info("GroupPicURL: " + msg.GroupPicURL);

                    // Update for the one who modified
                    chats[currentUser.ChatUserID].UpdatePicture(msg.ChatID, msg.GroupPicURL);

                    if (msg.GroupPicURL.Equals("null"))
                    {
                        senderUpdatePicMessage = "You have removed the group picture.";
                    }
                    else
                    {
                        senderUpdatePicMessage = "You have updated the group picture.";
                    }

                    string senderMsgID = senderMsgID = conversations[currentUser.ChatUserID].CreateSendConv(msg.ChatID, null, null, null, "GROUPMSG", senderUpdatePicMessage, 6);

                    // Update all other members
                    count = 0;
                    foreach (var r in receivers)
                    {
                        if (r.ReceiverChatStatus != null && r.ReceiverChatStatus == true)
                        {
                            Chat recvChat = chats[r.ReceiverID].GetById(r.ReceiverChatID);
                            string recvUpdatePicMessage = string.Empty;

                            if (msg.GroupPicURL.Equals("null"))
                            {
                                recvUpdatePicMessage = string.Format("{0} has removed the group picture.", MessageConverter.HtmlDecode(ChatAccountManager.GetUserName(currentUser.ChatUserID, r.ReceiverID)));
                            }
                            else
                            {
                                recvUpdatePicMessage = string.Format("{0} has updated the group picture.", MessageConverter.HtmlDecode(ChatAccountManager.GetUserName(currentUser.ChatUserID, r.ReceiverID)));
                            }

                            chats[r.ReceiverID].UpdatePicture(r.ReceiverChatID, msg.GroupPicURL);
                            recvUpdatePicMessages[count] = recvUpdatePicMessage;
                            recvUpdatePicMessageIDs[count++] = conversations[r.ReceiverID].CreateRecvConv(currentUser.ChatUserID, msg.ChatID, senderMsgID, r.ReceiverChatID, "MSGPEND", recvUpdatePicMessage, 6);
                        }
                    }
                }

                // Update title if its not null
                if (!msg.Title.Equals(" "))
                {
                    // Update for the one who modified
                    chats[currentUser.ChatUserID].UpdateTitle(msg.ChatID, MessageConverter.HtmlDecode(msg.Title));

                    senderUpdateTitleMessage = "You have changed the group title.";

                    string senderMsgID = senderMsgID = conversations[currentUser.ChatUserID].CreateSendConv(msg.ChatID, null, null, null, "GROUPMSG", senderUpdateTitleMessage, 6);

                    // Update all other members
                    count = 0;
                    foreach (var r in receivers)
                    {
                        if (r.ReceiverChatStatus != null && r.ReceiverChatStatus == true)
                        {
                            Chat recvChat = chats[r.ReceiverID].GetById(r.ReceiverChatID);
                            string recvUpdateTitleMessage = string.Format("{0} has changed the group title.", MessageConverter.HtmlDecode(ChatAccountManager.GetUserName(currentUser.ChatUserID, r.ReceiverID)));

                            chats[r.ReceiverID].UpdateTitle(r.ReceiverChatID, MessageConverter.HtmlDecode(msg.Title));

                            recvUpdateTitleMessages[count] = recvUpdateTitleMessage;
                            recvUpdateTitleMessageIDs[count++] = conversations[r.ReceiverID].CreateRecvConv(currentUser.ChatUserID, msg.ChatID, senderMsgID, r.ReceiverChatID, "MSGPEND", recvUpdateTitleMessage, 6);               
                        }                       
                    }
                }

                // Send Sync to all other members
                string[] allMembersId = (from p in receivers where p.ReceiverChatStatus == true select p.ReceiverID).ToArray();
                allMembersId = allMembersId.Concat(new string[] { currentUser.ChatUserID }).ToArray();

                string allMemberString = string.Empty;
                for (int index = 0; index < allMembersId.Count(); index++)
                {
                    if (index != allMembersId.Count() - 1)
                    {
                        allMemberString += allMembersId[index] + ',';
                    }
                    else
                    {
                        allMemberString += allMembersId[index];
                    }
                }

                count = 0;
                foreach (var p in receivers)
                {
                    var user = onlineUsers[p.ReceiverID];
                    if (user != null)
                    {
                        if (p.ReceiverChatStatus != null && p.ReceiverChatStatus == true)
                        {
                            if (!msg.GroupPicURL.Equals(" ") || msg.GroupPicURL.Equals("null"))
                            {
                                GroupUpdatePicSyncMessage gpSyncPic = new GroupUpdatePicSyncMessage()
                                {
                                    ChatID = p.ReceiverChatID,
                                    GroupPicURL = msg.GroupPicURL.Equals("null") ? " " : msg.GroupPicURL,
                                    NotificationID = recvUpdatePicMessageIDs[count],
                                    NotificationMessage = MessageConverter.HtmlCustomEncode(recvUpdatePicMessages[count]),
                                    Timestamp = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff")
                                };

                                ReplyMessage(user, gpSyncPic);

                            }

                            if (!msg.Title.Equals(" "))
                            {
                                GroupUpdateTitleSyncMessage gpSyncTitle = new GroupUpdateTitleSyncMessage()
                                {
                                    ChatID = p.ReceiverChatID,
                                    Title = MessageConverter.HtmlEncode(msg.Title),
                                    NotificationID = recvUpdateTitleMessageIDs[count],
                                    NotificationMessage = MessageConverter.HtmlCustomEncode(recvUpdateTitleMessages[count]),
                                    Timestamp = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff")
                                };

                                ReplyMessage(user, gpSyncTitle);
                            }
                        }
                       

                    }
                    else
                    {
                        if (p.ReceiverChatStatus != null && p.ReceiverChatStatus == true)
                        {
                            if (!msg.GroupPicURL.Equals(" ") || msg.GroupPicURL.Equals("null"))
                            {
                                GroupUpdatePicSyncMessage gpSyncPic = new GroupUpdatePicSyncMessage()
                                {
                                    ChatID = p.ReceiverChatID,
                                    GroupPicURL = msg.GroupPicURL.Equals("null") ? " " : msg.GroupPicURL,
                                    NotificationID = recvUpdatePicMessageIDs[count],
                                    NotificationMessage = MessageConverter.HtmlCustomEncode(recvUpdatePicMessages[count]),
                                    Timestamp = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff")
                                };

                                RelayServerCommunication.SendSearchUserMessage(p.ReceiverID, gpSyncPic.ToString());

                            }

                            if (!msg.Title.Equals(" "))
                            {
                                GroupUpdateTitleSyncMessage gpSyncTitle = new GroupUpdateTitleSyncMessage()
                                {
                                    ChatID = p.ReceiverChatID,
                                    Title = MessageConverter.HtmlEncode(msg.Title),
                                    NotificationID = recvUpdateTitleMessageIDs[count],
                                    NotificationMessage = MessageConverter.HtmlCustomEncode(recvUpdateTitleMessages[count]),
                                    Timestamp = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff")
                                };

                                RelayServerCommunication.SendSearchUserMessage(p.ReceiverID, gpSyncTitle.ToString());
                            }
                        }
                        
                    }

                    count++;
                }

                // Reply back to modifier
                if (!msg.GroupPicURL.Equals(" ") || msg.GroupPicURL.Equals("null"))
                {
                    GroupUpdatePicAckMessage ack = new GroupUpdatePicAckMessage
                    {
                        ChatID = currentChat.ChatID,
                        GroupPicURL = msg.GroupPicURL.Equals("null") ? " " : msg.GroupPicURL,
                        NotificationMessage = senderUpdatePicMessage,
                        Timestamp = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff")
                    };
                    ReplyMessage(currentUser, ack);
                }

                if (!msg.Title.Equals(" "))
                {
                    GroupUpdateTitleAckMessage ack = new GroupUpdateTitleAckMessage()
                    {
                        ChatID = currentChat.ChatID,
                        Title = MessageConverter.HtmlEncode(msg.Title),
                        NotificationMessage = senderUpdateTitleMessage,
                        Timestamp = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff")
                    };
                    ReplyMessage(currentUser, ack);
                }
            }
            catch (Exception ex)
            {
                Logger.Info(ex.StackTrace.ToString());
            }
        }
    }
}
