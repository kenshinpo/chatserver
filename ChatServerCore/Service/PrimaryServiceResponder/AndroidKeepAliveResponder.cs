﻿using System.Linq;
using ChatServerCore.Message;

namespace ChatServerCore.Service.PrimaryServiceResponder
{
    public class AndroidKeepAliveResponder : MessageResponder
    {
        public AndroidKeepAliveResponder()
            : base()
        {
            MessageOfInterested = MessageFormat.KEEPALIVE.First();
        }

        public override void Respond(ChatSessionCollection onlineUsers, ChatSession currentUser, string message)
        {
            //Logger.Info("Keeping Android Alive");
        }
    }
}
