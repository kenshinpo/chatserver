﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ChatServerCore.Service
{
    /// <summary>
    /// Encapsulates a collection of ChatSession object. 
    /// The purpose of this class is to reduce the change to other code if we changed the implementation of ChatSession storage.
    /// </summary>
    public class ChatSessionCollection
    {
        private SortedDictionary<string, ChatSession> sessions;

        public ChatSessionCollection()
        {
            sessions = new SortedDictionary<string, ChatSession>();
        }

        public ChatSession this[string chatUserID]
        {
            get
            {
                if (Contains(chatUserID)) return sessions[chatUserID];
                return null;
            }

            set
            {
                sessions[chatUserID] = value;
            }
        }

        public void Add(string chatUserID, ChatSession transport)
        {
            sessions.Add(chatUserID, transport);
        }

        public void Remove(string chatUserID)
        {
            sessions.Remove(chatUserID);
        }

        public bool Contains(string chatUserID)
        {
            return sessions.ContainsKey(chatUserID);
        }

        public int Count()
        {
            return sessions.Count;
        }

        public void ForEach(Action<ChatSession> action)
        {
            var keys = sessions.Keys.ToArray();
            foreach (var s in keys) action(sessions[s]);
        }
    }
}
