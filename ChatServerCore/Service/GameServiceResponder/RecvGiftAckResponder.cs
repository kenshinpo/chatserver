﻿using ChatServerCore.Data;
using ChatServerCore.Message;
using System;
using System.Linq;
using ChatServerCore.Utilities;
using ChatServerCore.Data.Entity;

namespace ChatServerCore.Service.GameServiceResponder
{
    public class RecvGiftAckResponder : MessageResponder
    {
        public RecvGiftAckResponder()
            : base()
        {
            MessageOfInterested = MessageFormat.RECVGIFTACK.First();
        }

        public override void Respond(ChatSessionCollection onlineUsers, ChatSession currentUser, string message)
        {
            try
            {
                RecvGiftAckMessage msg = RealizeMessage<RecvGiftAckMessage>(message);
                Conversation conv = null;
                Chat chat = null;

                var conversations = new Repositories<ConversationRepository>();
                var chats = new Repositories<ChatRepository>();
                var users = new Repositories<UserRepository>();

                var recvConvRepo = conversations[currentUser.ChatUserID];
                conv = recvConvRepo.FindById(msg.MsgID).First();

                chat = chats[currentUser.ChatUserID].GetById(msg.ChatID);

                recvConvRepo.UpdateStatusByGameMsgId(16, null, null, null, msg.MsgID);
                users[currentUser.ChatUserID].DecrementBadge();

                conversations[conv.SenderID].UpdateStatusByGameMsgId(18, 14, 19, conv.SenderConversationID, null);

                var user = onlineUsers[conv.SenderID];
                SentGiftMessage reply = new SentGiftMessage() { ChatID = conv.SenderChatID, MsgID = conv.SenderConversationID };
                if (user != null)
                {
                    ReplyMessage(user, reply);
                }
                else
                {
                    RelayServerCommunication.SendSearchUserMessage(conv.SenderID, reply.ToString());
                }
            }
            catch (Exception ex)
            {
                Logger.Info(ex.StackTrace.ToString());
            }
            
        }
    }
}
