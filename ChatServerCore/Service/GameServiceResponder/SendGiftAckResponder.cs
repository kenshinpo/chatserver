﻿using System;
using System.Linq;
using ChatServerCore.Data;
using ChatServerCore.Message;
using ChatServerCore.Utilities;

namespace ChatServerCore.Service.GameServiceResponder
{
    public class SendGiftAckResponder : MessageResponder
    {
        public SendGiftAckResponder()
            : base()
        {
            MessageOfInterested = MessageFormat.SENDGIFTACK.First();
        }

        public override void Respond(ChatSessionCollection onlineUsers, ChatSession currentUser, string message)
        {
            try
            {
                SendGiftAckMessage msg = RealizeMessage<SendGiftAckMessage>(message);

                Repositories<ConversationRepository> conversations = new Repositories<ConversationRepository>();

                conversations[currentUser.ChatUserID].UpdateStatusByMsgId(msg.MsgID, 13);
            }
            catch (Exception ex)
            {
                Logger.Info(ex.StackTrace.ToString());
            }

        }
    }
}
