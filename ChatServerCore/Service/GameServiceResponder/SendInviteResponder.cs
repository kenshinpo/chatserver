﻿using System;
using System.Collections;
using System.Linq;
using ChatServerCore.Data;
using ChatServerCore.Data.Database;
using ChatServerCore.Data.Entity;
using ChatServerCore.Message;
using ChatServerCore.Utilities;

namespace ChatServerCore.Service.GameServiceResponder
{
    public class SendInviteResponder : MessageResponder
    {
        public SendInviteResponder()
            : base()
        {
            MessageOfInterested = MessageFormat.SENDINVITE_GAMESERVER.First();
        }

        public override void Respond(ChatSessionCollection onlineUsers, ChatSession currentUser, string message)
        {
            try
            {
                string senderChatID;
                string receiverChatID;
                ArrayList toSenderMessages = new ArrayList();
                string messageToBeSent = string.Empty;

                //<@SENDINVITATION>T1306132011729E<@SENDERCHERRYID>Temp125<@RECVCHERRYIDS>http://202.14.202.247/UploadFiles/Images/Games/8.png<@GAMEICONURL>8<@GAMEID><@EOF> was received from User Web Service.

                // currentUser in this case will be referring to the game server
                SendInviteGameServerMessage msg = RealizeMessage<SendInviteGameServerMessage>(message);

                var primaryOnlineUsers = MessageServiceFactory.Instances["Primary Service"].OnlineUsers;

                // Convert cherryID to chatUserID
                string[] receiverCherryIDChunks = msg.ReceiverCherryIDs.Split(',');
                ArrayList cherryIDs = new ArrayList();
                cherryIDs.Add(msg.SenderCherryID);

                foreach (string id in receiverCherryIDChunks)
                {
                    cherryIDs.Add(id);
                }

                ArrayList chatUserIDs = ChatAccountManager.GetChatUserIDFromCherryID(cherryIDs);
                string senderChatUserID = (string)chatUserIDs[0];

                chatUserIDs.RemoveAt(0);

                Repositories<ParticipantRepository> participants = new Repositories<ParticipantRepository>();
                Repositories<ChatRepository> chats = new Repositories<ChatRepository>();
                Repositories<ConversationRepository> conversations = new Repositories<ConversationRepository>();
                Repositories<UserRepository> users = new Repositories<UserRepository>();

                for (int index = 0; index < chatUserIDs.Count; index++)
                {
                    string receiverChatUserID = (string)chatUserIDs[index];

                    // See if senderChatID is present
                    var existing = participants[senderChatUserID].FindByReceiverId(receiverChatUserID, false);

                    if (existing.Count() != 0)
                    {
                        senderChatID = existing.First().SenderChatID;
                        receiverChatID = existing.First().ReceiverChatID;
                    }
                    else
                    {
                        // Create chat
                        senderChatID = chats[senderChatUserID].Create(false, null, null, null);
                        receiverChatID = chats[receiverChatUserID].Create(false, null, null, null);

                        participants[senderChatUserID].Add(senderChatID, receiverChatID, receiverChatUserID, false);
                        participants[receiverChatUserID].Add(receiverChatID, senderChatID, senderChatUserID, false);
                    }

                    var userRepo = users[senderChatUserID];
                    int operatingType = userRepo.GetOperatingType();

                    // Create sender conversation
                    string senderMsgID;
                    var senderChat = chats[senderChatUserID].GetById(senderChatID);

                    if (operatingType == 1)
                    {
                        messageToBeSent = msg.SenderMessage + "{}" + msg.GameIconURL + "{}" + msg.GameID + "{}" + msg.GameName + "{}" + msg.IOSURLScheme + "{}" + msg.IOSDownload;
                    }
                    else
                    {
                        messageToBeSent = msg.SenderMessage + "{}" + msg.GameIconURL + "{}" + msg.GameID + "{}" + msg.GameName + "{}" + msg.AndroidPackageName + "{}" + msg.AndroidClassName + "{}" + msg.AndroidDownload;
                    }

                    // Status: "INVITE_SNDPEND"
                    senderMsgID = conversations[senderChatUserID].CreateSendGameConv(senderChatUserID, senderChatID, null, receiverChatUserID, receiverChatID, "INVITE_SND", messageToBeSent, 5);
                    Conversation senderMessage = conversations[senderChatUserID].FindById(senderMsgID).First();
                    SendInviteMessage toSenderMessage = new SendInviteMessage
                    {
                        ChatID = senderChatID,
                        MsgID = senderMsgID,
                        RecvChatUserID = receiverChatUserID,
                        Message = messageToBeSent,
                        Type = 5,
                        Timestamp = senderMessage.CreatedOn.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss.fff")
                    };

                    toSenderMessages.Add(toSenderMessage);

                    // Create receiver conversation
                    // Status: "INVITE_MSGPEND"
                    userRepo = users[receiverChatUserID];
                    operatingType = userRepo.GetOperatingType();

                    if (operatingType == 1)
                    {
                        messageToBeSent = msg.ReceiverMessage + "{}" + msg.GameIconURL + "{}" + msg.GameID + "{}" + msg.GameName + "{}" + msg.IOSURLScheme + "{}" + msg.IOSDownload;
                    }
                    else
                    {
                        messageToBeSent = msg.ReceiverMessage + "{}" + msg.GameIconURL + "{}" + msg.GameID + "{}" + msg.GameName + "{}" + msg.AndroidPackageName + "{}" + msg.AndroidClassName + "{}" + msg.AndroidDownload;
                    }

                    string receiverMsgID = conversations[receiverChatUserID].CreateRecvGameConv(senderChatUserID, senderChatID, senderMsgID, receiverChatID, receiverChatUserID, "INVITE_MSGPEND", messageToBeSent, 5);
                    users[receiverChatUserID].IncrementBadge();

                    if (primaryOnlineUsers[receiverChatUserID] == null)
                    {
                        string deviceToken = userRepo.GetDeviceToken();
                        int badge = userRepo.GetBadge();
                        string chatID = receiverChatID;

                        string notification = String.Format("sent you an invitation!");
                        PushNotification(deviceToken, String.Format("{0} {1}", ChatAccountManager.GetUserName(currentUser.ChatUserID, receiverChatUserID), notification), chatID, badge, operatingType);

                    }
                    else
                    {
                        Conversation recvMessage = conversations[receiverChatUserID].FindById(receiverMsgID).First();
                        RecvInviteMessage recv = new RecvInviteMessage()
                        {
                            ChatID = receiverChatID,
                            MsgID = receiverMsgID,
                            SenderChatUserID = senderChatUserID,
                            Message = messageToBeSent,
                            Type = 5,
                            Timestamp = recvMessage.CreatedOn.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss.fff")
                        };
                        ReplyMessage(primaryOnlineUsers[receiverChatUserID], recv);
                    }
                }

                // Send to sender if he/she is online
                if (primaryOnlineUsers[senderChatUserID] != null)
                {
                    foreach (SendInviteMessage sendInvite in toSenderMessages)
                    {
                        ReplyMessage(primaryOnlineUsers[senderChatUserID], sendInvite);
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.Info(ex.StackTrace.ToString());
            }
            
        }
    }
}
