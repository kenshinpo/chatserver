﻿using System;
using System.Collections;
using System.Linq;
using ChatServerCore.Data;
using ChatServerCore.Data.Database;
using ChatServerCore.Data.Entity;
using ChatServerCore.Message;
using ChatServerCore.Utilities;

namespace ChatServerCore.Service.GameServiceResponder
{
    public class SendGiftResponder : MessageResponder
    {
        public SendGiftResponder()
            : base()
        {
            MessageOfInterested = MessageFormat.SENDGIFT_GAMESERVER.First();
        }

        public override void Respond(ChatSessionCollection onlineUsers, ChatSession currentUser, string message)
        {
            try
            {
                string senderChatID;
                string receiverChatID;
                ArrayList toSenderMessages = new ArrayList();
                string messageToBeSent = string.Empty;

                // currentUser in this case will be referring to the game server
                SendGiftGameServerMessage msg = RealizeMessage<SendGiftGameServerMessage>(message);

                var primaryOnlineUsers = MessageServiceFactory.Instances["Primary Service"].OnlineUsers;

                // Convert cherryID to chatUserID
                string[] receiverCherryIDChunks = msg.ReceiverCherryIDs.Split(',');
                ArrayList cherryIDs = new ArrayList();
                cherryIDs.Add(msg.SenderCherryID);

                foreach (string id in receiverCherryIDChunks)
                {
                    cherryIDs.Add(id);
                }

                ArrayList chatUserIDs = ChatAccountManager.GetChatUserIDFromCherryID(cherryIDs);
                string senderChatUserID = (string)chatUserIDs[0];

                chatUserIDs.RemoveAt(0);

                Repositories<UserRepository> users = new Repositories<UserRepository>();
                Repositories<ParticipantRepository> participants = new Repositories<ParticipantRepository>();
                Repositories<ChatRepository> chats = new Repositories<ChatRepository>();
                Repositories<ConversationRepository> conversations = new Repositories<ConversationRepository>();


                for (int index = 0; index < chatUserIDs.Count; index++)
                {
                    string receiverChatUserID = (string)chatUserIDs[index];

                    // See if senderChatID is present
                    var existing = participants[senderChatUserID].FindByReceiverId(receiverChatUserID, false);

                    if (existing.Count() != 0)
                    {
                        senderChatID = existing.First().SenderChatID;
                        receiverChatID = existing.First().ReceiverChatID;
                    }
                    else
                    {
                        // Create chat
                        senderChatID = chats[senderChatUserID].Create(false, null, null, null);
                        receiverChatID = chats[receiverChatUserID].Create(false, null, null, null);

                        participants[senderChatUserID].Add(senderChatID, receiverChatID, receiverChatUserID, false);
                        participants[receiverChatUserID].Add(receiverChatID, senderChatID, senderChatUserID, false);
                    }

                    var userRepo = users[senderChatUserID];
                    int operatingType = userRepo.GetOperatingType();

                    // Create sender conversation
                    string senderMsgID;
                    var senderChat = chats[senderChatUserID].GetById(senderChatID);

                    if (operatingType == 1)
                    {
                        messageToBeSent = msg.SenderMessage + "{}" + msg.GameIconURL + "{}" + msg.GameID + "{}" + msg.GameName + "{}" + msg.IOSURLScheme + "{}" + msg.IOSDownload;
                    }
                    else
                    {
                        messageToBeSent = msg.SenderMessage + "{}" + msg.GameIconURL + "{}" + msg.GameID + "{}" + msg.GameName + "{}" + msg.AndroidPackageName + "{}" + msg.AndroidClassName + "{}" + msg.AndroidDownload;
                    }

                    // Status: "GIFT_SNDPEND"
                    senderMsgID = conversations[senderChatUserID].CreateSendGameConv(senderChatUserID, senderChatID, null, receiverChatUserID, receiverChatID, "GIFT_SND", messageToBeSent, 4);
                    Conversation senderMessage = conversations[senderChatUserID].FindById(senderMsgID).First();
                    SendGiftMessage toSenderMessage = new SendGiftMessage
                    {
                        ChatID = senderChatID,
                        MsgID = senderMsgID,
                        RecvChatUserID = receiverChatUserID,
                        Message = messageToBeSent,
                        Type = 5,
                        Timestamp = senderMessage.CreatedOn.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss.fff")
                    };

                    toSenderMessages.Add(toSenderMessage);

                    // Status: "GIFT_MSGPEND"
                    userRepo = users[receiverChatUserID];
                    operatingType = userRepo.GetOperatingType();

                    if (operatingType == 1)
                    {
                        messageToBeSent = msg.ReceiverMessage + "{}" + msg.GameIconURL + "{}" + msg.GameID + "{}" + msg.GameName + "{}" + msg.IOSURLScheme + "{}" + msg.IOSDownload;
                    }
                    else
                    {
                        messageToBeSent = msg.ReceiverMessage + "{}" + msg.GameIconURL + "{}" + msg.GameID + "{}" + msg.GameName + "{}" + msg.AndroidPackageName + "{}" + msg.AndroidClassName + "{}" + msg.AndroidDownload;
                    }

                    string receiverMsgID = conversations[receiverChatUserID].CreateRecvGameConv(senderChatUserID, senderChatID, senderMsgID, receiverChatID, receiverChatUserID, "GIFT_MSGPEND", messageToBeSent, 4);
                    users[receiverChatUserID].IncrementBadge();

                    if (primaryOnlineUsers[receiverChatUserID] == null)
                    {
                        string deviceToken = userRepo.GetDeviceToken();
                        int badge = userRepo.GetBadge();
                        string chatID = receiverChatID;

                        string notification = String.Format("sent you a gift!");
                        PushNotification(deviceToken, String.Format("{0} {1}", ChatAccountManager.GetUserName(currentUser.ChatUserID, receiverChatUserID), notification), chatID, badge, operatingType);

                    }
                    else
                    {
                        Conversation recvMessage = conversations[receiverChatUserID].FindById(receiverMsgID).First();
                        RecvGiftMessage recv = new RecvGiftMessage()
                        {
                            ChatID = receiverChatID,
                            MsgID = receiverMsgID,
                            SenderChatUserID = senderChatUserID,
                            Message = messageToBeSent,
                            Type = 4,
                            Timestamp = recvMessage.CreatedOn.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss.fff")
                        };
                        ReplyMessage(primaryOnlineUsers[receiverChatUserID], recv);
                    }
                }

                // Send to sender if he/she is online
                if (primaryOnlineUsers[senderChatUserID] != null)
                {
                    foreach (SendGiftMessage sendGame in toSenderMessages)
                    {
                        ReplyMessage(primaryOnlineUsers[senderChatUserID], sendGame);
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.Info(ex.StackTrace.ToString());
            }
           
        }
    }
}
