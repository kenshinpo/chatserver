﻿using System;
using System.Linq;
using ChatServerCore.Data;
using ChatServerCore.Message;
using ChatServerCore.Utilities;

namespace ChatServerCore.Service.GameServiceResponder
{
    public class SentGiftAckResponder : MessageResponder
    {
        public SentGiftAckResponder()
            : base()
        {
            MessageOfInterested = MessageFormat.SENTGIFTACK.First();
        }

        public override void Respond(ChatSessionCollection onlineUsers, ChatSession currentUser, string message)
        {
            try
            {
                SentGiftAckMessage msg = RealizeMessage<SentGiftAckMessage>(message);

                Repositories<ConversationRepository> conversations = new Repositories<ConversationRepository>();

                conversations[currentUser.ChatUserID].UpdateStatusByMsgId(msg.MsgID, 15);
            }
            catch (Exception ex)
            {
                Logger.Info(ex.StackTrace.ToString());
            }
        }
    }
}
