﻿using System.Linq;
using ChatServerCore.Data;
using ChatServerCore.Message;

namespace ChatServerCore.Service.GameServiceResponder
{
    public class SentInviteAckResponder : MessageResponder
    {
        public SentInviteAckResponder()
            : base()
        {
            MessageOfInterested = MessageFormat.SENTINVITEACK.First();
        }

        public override void Respond(ChatSessionCollection onlineUsers, ChatSession currentUser, string message)
        {
            SentInviteAckMessage msg = RealizeMessage<SentInviteAckMessage>(message);

            Repositories<ConversationRepository> conversations = new Repositories<ConversationRepository>();

            conversations[currentUser.ChatUserID].UpdateStatusByMsgId(msg.MsgID, 8);
        }
    }
}
