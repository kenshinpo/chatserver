﻿using System;
using System.Linq;
using ChatServerCore.Data;
using ChatServerCore.Message;
using ChatServerCore.Utilities;

namespace ChatServerCore.Service.GameServiceResponder
{
    public class SendInviteAckResponder : MessageResponder
    {
        public SendInviteAckResponder()
            : base()
        {
            MessageOfInterested = MessageFormat.SENDINVITEACK.First();
        }

        public override void Respond(ChatSessionCollection onlineUsers, ChatSession currentUser, string message)
        {
            try
            {
                SendInviteAckMessage msg = RealizeMessage<SendInviteAckMessage>(message);

                Repositories<ConversationRepository> conversations = new Repositories<ConversationRepository>();

                conversations[currentUser.ChatUserID].UpdateStatusByMsgId(msg.MsgID, 6);
            }
            catch (Exception ex)
            {
                Logger.Info(ex.StackTrace.ToString());
            }

        }
    }
}
