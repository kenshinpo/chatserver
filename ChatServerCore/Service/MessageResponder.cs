﻿using System;
using System.Text;
using ChatServerCore.Connectivity.PushNotification;
using ChatServerCore.Message;
using ChatServerCore.Utilities;

namespace ChatServerCore.Service
{
    /// <summary>
    /// Reponse type to particular user request.
    /// </summary>
    public abstract class MessageResponder
    {
        /// <summary>
        /// The header of message that this object is interested.
        /// </summary>
        public string MessageOfInterested { get; protected set; }

        /// <summary>
        /// Handle and respond to user request.
        /// </summary>
        /// <param name="onlineUsers">Collection of online user on this service.</param>
        /// <param name="currentUser">The sender of request.</param>
        /// <param name="message">Current request.</param>
        /// <returns>Indicate the server should continue reading.</returns>
        public abstract void Respond(ChatSessionCollection onlineUsers, ChatSession currentUser, string message);

        protected T RealizeMessage<T>(string textMsg) where T : MessageType<T>, new()
        {
            T msg = new T();
            msg.Realize(textMsg);
            return msg;
        }

        protected void ReplyMessage<T>(ChatSession user, MessageType<T> msg) where T : MessageType<T>, new()
        {
            try
            {
                var str = msg.ToString();
                user.Transport.Write(Encoding.UTF8.GetBytes(msg.ToString()));
            }
            catch (Exception ex)
            {
                Logger.Info(String.Format("User {0} session occurs error: {1} \nCall Stack:\n{2}", user.ChatUserID, ex.Message, ex.StackTrace));
                user.Transport.Close();
            }
        }

        protected void ReplyMessage(ChatSession user, string msg)
        {
            try
            {
                var str = msg.ToString();
                user.Transport.Write(Encoding.UTF8.GetBytes(msg.ToString()));
            }
            catch (Exception ex)
            {
                Logger.Info(String.Format("User {0} session occurs error: {1} \nCall Stack:\n{2}", user.ChatUserID, ex.Message, ex.StackTrace));
                user.Transport.Close();
            }
        }

        protected void PushNotification(string deviceToken, string message, string chatId, int badge, int operatingType)
        {
            if (message.Length > 100) message = message.Substring(0, 97) + "...";

            APNSManager.getInstance().SendNotification(deviceToken, message, chatId, badge, operatingType);
        }
    }
}
