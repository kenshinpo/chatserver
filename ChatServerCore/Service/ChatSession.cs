﻿using ChatServerCore.Connectivity;

namespace ChatServerCore.Service
{

    /// <summary>
    /// Bundle that contains of user state.
    /// </summary>
    public class ChatSession
    {
        /// <summary>
        /// Identity of the user.
        /// </summary>
        public string ChatUserID = string.Empty;

        /// <summary>
        /// The connection object which can be use to communicate with the user.
        /// </summary>
        public ITransport Transport = null;

        /// <summary>
        /// Indicate if server should automatically receive next message from this user.
        /// </summary>
        public bool ReceiveNextMessage = true;
    }
}
