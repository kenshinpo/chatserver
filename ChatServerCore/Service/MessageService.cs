﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChatServerCore.Connectivity;
using ChatServerCore.Utilities;

namespace ChatServerCore.Service
{
    public class MessageService : IProtocol
    {
        private static char[] tagDelimiter = new char[] { '>' };

        protected ChatSessionCollection onlineUsers;
        protected ChatSession currentUser;
        protected StringBuilder stringBuffer;
        protected Dictionary<string, MessageResponder> responders;

        public MessageService(ChatSessionCollection users,  Dictionary<string, MessageResponder> responders)
        {
            this.onlineUsers = users;
            this.stringBuffer = new StringBuilder();
            this.responders = responders;
            this.currentUser = new ChatSession();
        }
        
        public void ConnectionMade(ITransport transport)
        {
            currentUser.Transport = transport;
            transport.Read();
        }

        public void ConnectionLost(ITransport transport)
        {
            Logger.Info(String.Format("User {0} has lost its connection.", currentUser.ChatUserID == string.Empty ? "Web Service" : currentUser.ChatUserID));
            RelayServerCommunication.SendClientDisconnectMessage(currentUser.ChatUserID);
            onlineUsers.Remove(currentUser.ChatUserID);
        }

        public void ConnectionTimeOut(ITransport transport)
        {
            Logger.Info(String.Format("User {0} connection timeout.", currentUser.ChatUserID == string.Empty ? "Web Service" : currentUser.ChatUserID));
            currentUser.Transport.Close();
           
        }

        public void DataSent(byte[] data, int sentByteCount)
        {
            string sentStr = Encoding.UTF8.GetString(data);
            if (!sentStr.StartsWith("<@OK>"))
            {
                Logger.Info(String.Format("{0} was sent to User {1}.", sentStr, currentUser.ChatUserID));
            }
        }

        public void DataReceived(byte[] data, int receivedByteCount)
        {
            stringBuffer.Append(Encoding.UTF8.GetString(data, 0, receivedByteCount));
            IEnumerable<string> msgs = ExtractMessages(stringBuffer.ToString());

            try
            {
                foreach (var msg in msgs)
                {
                    if (!msg.StartsWith("<@P>"))
                    {
                        //Logger.Info(String.Format("{0} was received from User {1}.", msg, currentUser.ChatUserID == string.Empty ? "Web Service" : currentUser.ChatUserID));
                    }
                    int delimiterIndex = msg.IndexOf('>');
                    string tag = msg.Substring(0, delimiterIndex + 1);

                    if (responders.ContainsKey(tag)) responders[tag].Respond(onlineUsers, currentUser, msg);
                    else Logger.Info(String.Format("Receive unknown message: \"{0}\" from User {1}.", msg, currentUser.ChatUserID));
                }
                if (currentUser.ReceiveNextMessage) currentUser.Transport.Read();
            }
            catch (NullReferenceException e)
            {
                Logger.Info(String.Format("Null reference exception occurs on User {0} session, ignored. Error: {1}, {2}", currentUser.ChatUserID, e.Message, e.StackTrace));
            }
            catch (Exception ex)
            {
                Logger.Info(String.Format("Error occurs on User {0} session: {1} \nCall stack:\n {2}", currentUser.ChatUserID, ex.Message, ex.StackTrace));
                onlineUsers.Remove(currentUser.ChatUserID);
                currentUser.Transport.Close();
            }
        }

        private IEnumerable<string> ExtractMessages(string received)
        {
            List<string> msgList = new List<string>();
            int index = -1;

            while ((index = received.IndexOf("<@EOF>")) != -1)
            {
                stringBuffer.Remove(0, index + 6);
                msgList.Add(received.Substring(0, index + 6));
                received = received.Substring(index + 6);
            }
            return msgList;
        }

        private IEnumerable<string> TokenizeMessage(string msg)
        {
            List<string> strList = new List<string>(5);
            string head = string.Empty;
            string tail = msg;
            char[] seperator1 = { '>' };
            string[] seperator2 = { "<@" };

            while (tail != string.Empty)
            {
                string[] splited = tail.Split(seperator1, 2);
                string value = splited[0].Split(seperator2, 2, StringSplitOptions.None).First();
                if (value != "") strList.Add(value);
                tail = splited[1];
            }
            return strList;
        }
    }
}
