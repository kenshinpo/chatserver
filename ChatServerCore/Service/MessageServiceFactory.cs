﻿using System;
using System.Collections.Generic;
using ChatServerCore.Connectivity;
using ChatServerCore.Connectivity.PushNotification;

namespace ChatServerCore.Service
{
    public class MessageServiceFactory : IProtocolFactory
    {
        public static Dictionary<string, MessageServiceFactory> Instances =
            new Dictionary<string, MessageServiceFactory>();

        private Dictionary<string, MessageResponder> responders;

        public string Name { get; private set; }

        public ChatSessionCollection OnlineUsers { get; private set; }

        public MessageServiceFactory(string name)
        {
            Name = name;
            responders = new Dictionary<string, MessageResponder>();
        }

        public void RegisterResponder(MessageResponder responder)
        {
            responders.Add(responder.MessageOfInterested, responder);
        }

        public void DropResponder(MessageResponder responder)
        {
            responders.Remove(responder.MessageOfInterested);
        }

        public void Start()
        {
            APNSManager.startManager(APNSManager.OPERATION_MODE.OPS_PRO_CC);
            OnlineUsers = new ChatSessionCollection();
            
            if (Instances.ContainsKey(Name))
            {
                throw new InvalidOperationException("An instance with same name is already exists");
            }
            Instances[Name] = this;
            
            //Logger.Info(String.Format("{0} started.", Name)); 
        }

        public IProtocol CreateService()
        {
            return new MessageService(OnlineUsers, responders);
        }

        public void Stop()
        {
            OnlineUsers.ForEach(new Action<ChatSession>((cs) => cs.Transport.Close()));
            Instances.Remove(Name);
            //Logger.Info(String.Format("{0} stopped.", Name)); 
        }

        public string GetName()
        {
            return Name;
        }

        public ChatSessionCollection GetOnlineChatUsers()
        {
            return OnlineUsers;
        }
    }
}
