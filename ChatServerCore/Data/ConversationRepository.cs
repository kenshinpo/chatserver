﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading;
using ChatServerCore.Data.Database;
using ChatServerCore.Data.Entity;
using ChatServerCore.Utilities;

namespace ChatServerCore.Data
{
    /// <summary>
    /// Interface type to data source. Provide variety of query to data source.
    /// </summary>
    class ConversationRepository : DataRepository
    {
        private DBDetails _dbDetails;

        private DBDetails DBDetails
        {
            get
            {
                if (_dbDetails == null) _dbDetails = TableNameResolver.GetDBDetails(chatUserID);
                return _dbDetails;
            }
        }

        /// <summary>
        /// Insert a conversation record for sender of message.
        /// </summary>
        /// <param name="senderChatId">The sender ID.</param>
        /// <param name="senderMsgId">The sender msgID.</param>
        /// <param name="receiverId">The receiver ID.</param>
        /// <param name="receiverChatId">The receiver chat ID.</param>
        /// <param name="status">The QOS status of this record.</param>
        /// <param name="message">The message.</param>
        /// <param name="type">Indicate type of message.</param>
        /// <returns>The number of row affected in data source after executing this query.</returns>
        public string CreateSendConv(string senderChatID, string senderConversationID, string receiverChatUserID, string receiverChatID, string status, string message, int type)
        {
            string senderMessageID = string.Empty;
            try
            {
                senderMessageID = ChatManager.PushConversation(chatUserID, senderChatID, senderConversationID, receiverChatUserID, receiverChatID, status, message, type, DBDetails);
            }
            catch (SqlException ex)
            {
                Logger.Info(ex.Message.ToString() + " " + ex.StackTrace.ToString());
                Thread.Sleep(1);
                senderMessageID = ChatManager.PushConversation(chatUserID, senderChatID, senderConversationID, receiverChatUserID, receiverChatID, status, message, type, DBDetails);
            }

            return senderMessageID;
        }

        public string CreateSendGameConv(string senderChatUserID, string senderChatID, string senderConversationID, string receiverChatUserID, string receiverChatID, string status, string message, int type)
        {
            string senderMessageID = string.Empty;
            try
            {
                senderMessageID = ChatManager.PushConversation(senderChatUserID, senderChatID, senderConversationID, receiverChatUserID, receiverChatID, status, message, type, TableNameResolver.GetDBDetails(senderChatUserID));
            }
            catch (SqlException ex)
            {
                Logger.Info(ex.Message.ToString() + " " + ex.StackTrace.ToString());
                Thread.Sleep(1);
                senderMessageID = ChatManager.PushConversation(senderChatUserID, senderChatID, senderConversationID, receiverChatUserID, receiverChatID, status, message, type, TableNameResolver.GetDBDetails(senderChatUserID));
            }
            return senderMessageID;
        }

        /// <summary>
        /// Insert a conversation record for receiver of message.
        /// </summary>
        /// <param name="senderChatId">The sender ID.</param>
        /// <param name="senderMsgId">The sender msgID.</param>
        /// <param name="receiverId">The receiver ID.</param>
        /// <param name="receiverChatId">The receiver chat ID.</param>
        /// <param name="status">The QOS status of this record.</param>
        /// <param name="message">The message.</param>
        /// <param name="type">Indicate type of message.</param>
        /// <returns>The number of row affected in data source after executing this query.</returns>
        public string CreateRecvConv(string senderChatUserID, string senderChatID, string senderConversationID, string receiverChatID, string status, string message, int type)
        {
            string recvMessageID = string.Empty;
            try
            {
                //Logger.Info("Receiver: " + DBDetails.ChatTableName);
                recvMessageID = ChatManager.PushConversation(senderChatUserID, senderChatID, senderConversationID, chatUserID, receiverChatID, status, message, type, DBDetails);
            }
            catch (SqlException ex)
            {
                Logger.Info(ex.Message.ToString() + " " + ex.StackTrace.ToString());
                Thread.Sleep(1);
                recvMessageID = ChatManager.PushConversation(senderChatUserID, senderChatID, senderConversationID, chatUserID, receiverChatID, status, message, type, DBDetails);
            }
            return recvMessageID;
        }

        public string CreateRecvGameConv(string senderChatUserID, string senderChatID, string senderConversationID, string receiverChatID, string receiverChatUserID, string status, string message, int type)
        {
            string recvMessageID = string.Empty;
            try
            {
                recvMessageID = ChatManager.PushConversation(senderChatUserID, senderChatID, senderConversationID, receiverChatUserID, receiverChatID, status, message, type, TableNameResolver.GetDBDetails(receiverChatUserID));
            }
            catch (SqlException ex)
            {
                Logger.Info(ex.Message.ToString() + " " + ex.StackTrace.ToString());
                Thread.Sleep(1);
                recvMessageID = ChatManager.PushConversation(senderChatUserID, senderChatID, senderConversationID, receiverChatUserID, receiverChatID, status, message, type, TableNameResolver.GetDBDetails(receiverChatUserID));
            }
            return recvMessageID;
        }

        /// <summary>
        /// Update a conversation QOS status.
        /// </summary>
        /// <param name="msgId">The ID of conversation.</param>
        /// <param name="status">The QOS status</param>
        public void UpdateStatusByMsgId(string conversationID, int status)
        {
            ChatManager.RenewConversation(status, null, conversationID, DBDetails);
        }

        //RenewGameConversation(string status, string sendStatus, string sentNotReceivedStatus, string senderChatID, string receiverChatID, string tableName)
        public void UpdateStatusByGameMsgId(int? status, int? sendStatus, int? sentNotReceivedStatus, string senderChatID, string receiverChatID)
        {
            ChatManager.RenewGameConversation(status, sendStatus, sentNotReceivedStatus, senderChatID, receiverChatID, DBDetails);
        }

        public void UpdateStatusBySenderChatId(string senderChatID, int status)
        {
            ChatManager.RenewConversation(status, senderChatID, null, DBDetails);
        }

        public IEnumerable<Conversation> FindById(string conversationID)
        {
            return Conversation.Map(ChatManager.PullConversationByID(conversationID, DBDetails));
        }

        public IEnumerable<Conversation> FindByAllNewConversationByChatID(string chatID, string lastSyncDate)
        {
            return Conversation.Map(ChatManager.PullAllNewConversationByChatID(chatID, chatUserID, lastSyncDate, DBDetails));
        }

        public IEnumerable<LastConversation> FindByLastPendingRecv(string lastSyncDate)
        {
            return LastConversation.Map(ChatManager.PullLastConversationByPendingRecv(chatUserID, lastSyncDate, DBDetails));
        }

    }
}
