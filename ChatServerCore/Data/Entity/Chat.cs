﻿using System;
using System.Collections.Generic;
using System.Data;

namespace ChatServerCore.Data.Entity
{
    /// <summary>
    /// Provides strong typed representation for data source record.
    /// </summary>
    public class Chat
    {
        public string ChatID { get; set; }
        public string CreatorID { get; set; }
        public string Title { get; set; }
        public bool IsGroupChat { get; set; }
        public string GroupPicURL { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime LastModified { get; set; }
        public bool Status { get; set; }
        public string AdminID { get; set; }

        /// <summary>
        /// Map database records into list of objects.
        /// </summary>
        /// <param name="reader">Reader object that is bound to database.</param>
        /// <returns>Collection of Chat objects.</returns>
        internal static IEnumerable<Chat> Map(DataView dView)
        {
            List<Chat> chatList = new List<Chat>();

            for (int i = 0; i < dView.Count; i++)
            {
                Chat chat = new Chat();
                chat.ChatID = dView[i]["id"].ToString();
                chat.CreatorID = dView[i]["creatorId"].ToString();
                chat.Title = dView[i]["title"] == DBNull.Value ? string.Empty : dView[i]["title"].ToString();
                chat.IsGroupChat = (bool)dView[i]["isGroup"];
                chat.GroupPicURL = dView[i]["groupPicURL"] == DBNull.Value || dView[i]["groupPicURL"] .ToString().Length <= 1? string.Empty : dView[i]["groupPicURL"].ToString();
                chat.AdminID = dView[i]["admin"] == DBNull.Value ? string.Empty : dView[i]["admin"].ToString();
                chat.CreatedOn = DateTime.Parse(dView[i]["createdDate"].ToString());
                chat.LastModified = DateTime.Parse(dView[i]["lastModifiedDate"].ToString());
                chat.Status = (bool)dView[i]["status"];
                chatList.Add(chat);
            }

            return chatList;
        }
    }
}
