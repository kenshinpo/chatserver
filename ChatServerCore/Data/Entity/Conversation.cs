﻿using System;
using System.Collections.Generic;
using System.Data;

namespace ChatServerCore.Data.Entity
{
    /// <summary>
    /// Provides strong typed representation for data source record.
    /// </summary>
    public class Conversation
    {
        public string ConversationID { get; set; }
        public string SenderID { get; set; }
        public string SenderChatID { get; set; }
        public string SenderConversationID { get; set; }
        public string ReceiverID { get; set; }
        public string ReceiverChatID { get; set; }
        public string Status { get; set; }
        public string Message { get; set; }
        public int Type { get; set; }
        public DateTime CreatedOn { get; set; }
        public int StatusCode { get; set; }
        

        /// <summary>
        /// Map database records into list of objects.
        /// </summary>
        /// <param name="reader">Reader object that is bound to database.</param>
        /// <returns>Collection of Conversation objects.</returns>
        internal static IEnumerable<Conversation> Map(DataView dView)
        {
            List<Conversation> convList = new List<Conversation>();
            for (int i = 0; i < dView.Count; i++)
            {
                Conversation conv = new Conversation()
                {
                    ConversationID = dView[i]["id"].ToString(),
                    SenderID = dView[i]["senderChatUserId"].ToString(),
                    SenderChatID = dView[i]["senderChatId"].ToString(),
                    SenderConversationID = dView[i]["senderConversationId"].ToString(),
                    ReceiverID = dView[i]["receiverChatUserID"].ToString(),
                    ReceiverChatID = dView[i]["receiverChatID"].ToString(),
                    Status = (dView[i]["status"].ToString()).Trim(),
                    Message = dView[i]["message"].ToString(),
                    Type = Convert.ToInt16(dView[i]["type"]),
                    CreatedOn = (DateTime)dView[i]["createdDate"],
                    StatusCode = Convert.ToInt16(dView[i]["statusTemp"])
                };
                convList.Add(conv);
            }
          
            return convList;
        }
    }
}
