﻿using System;
using System.Collections.Generic;
using System.Data;

namespace ChatServerCore.Data.Entity
{
    public class Participant
    {
        /// <summary>
        /// Provides strong typed representation for data source record.
        /// </summary>
        public string SenderChatID { get; set; }
        public string SenderID { get; set; }
        public string ReceiverChatID { get; set; }
        public string ReceiverID { get; set; }
        public bool IsGroup { get; set; }
        public Nullable<bool> ReceiverChatStatus { get; set; }
        public DateTime CreatedOn { get; set; }


        /// <summary>
        /// Map database records into list of objects.
        /// </summary>
        /// <param name="reader">Reader object that is bound to database.</param>
        /// <returns>Collection of Participant objects.</returns>
        internal static IEnumerable<Participant> Map(DataView dView)
        {
            List<Participant> partList = new List<Participant>();

            for (int i = 0; i < dView.Count; i++)
            {
                Participant part = new Participant()
                {
                    SenderChatID = dView[i]["senderChatId"].ToString(),
                    SenderID = dView[i]["senderChatUserId"].ToString(),
                    ReceiverChatID = dView[i]["receiverChatId"].ToString(),
                    ReceiverID = dView[i]["receiverChatUserId"].ToString(),
                    IsGroup = (bool)dView[i]["isGroup"],
                    ReceiverChatStatus = !dView.ToTable().Columns.Contains("receiverChatStatus") ? null : dView[i]["receiverChatStatus"] as Nullable<bool>,
                    CreatedOn = (DateTime)dView[i]["createdDate"],
                };
                partList.Add(part);
            }
            return partList;
        }
    }
}
