﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace ChatServerCore.Data.Entity
{
    /// <summary>
    /// Provides strong typed representation for data source record.
    /// </summary>
    public class User
    {
        public string ID { get; set; }
        public string DeviceToken { get; set; }
        public long Badge { get; set; }

        /// <summary>
        /// Map database records into list of objects.
        /// </summary>
        /// <param name="reader">Reader object that is bound to database.</param>
        /// <returns>Collection of Participant objects.</returns>
        internal static IEnumerable<User> Map(SqlDataReader reader)
        {
            List<User> userList = new List<User>();
            try
            {
                while (reader.Read())
                {
                    User user = new User()
                    {
                        ID = reader[0].ToString(),
                        DeviceToken = reader[1].ToString(),
                        Badge = Int64.Parse(reader[2].ToString())
                    };
                    userList.Add(user);
                }
                return userList;
            }
            finally
            {
                reader.Close();
            }
        }
    }
}
