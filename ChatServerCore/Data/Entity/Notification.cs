﻿using System.Collections.Generic;

namespace ChatServerCore.Data.Entity
{
    /// <summary>
    /// Provides strong typed representation for data source record.
    /// </summary>
    public class Notification
    {
        public List<string> NotificationID { get; set; }
        public List<string> NotificationMessage { get; set; }
    }
}
