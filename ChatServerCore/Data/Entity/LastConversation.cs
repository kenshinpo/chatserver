﻿using System;
using System.Collections.Generic;
using System.Data;

namespace ChatServerCore.Data.Entity
{
    /// <summary>
    /// Provides strong typed representation for data source record.
    /// </summary>
    public class LastConversation
    {
        public string ConversationID { get; set; }
        public string SenderID { get; set; }
        public string SenderChatID { get; set; }
        public string SenderConversationID { get; set; }
        public string ReceiverID { get; set; }
        public string ReceiverChatID { get; set; }
        public string Status { get; set; }
        public string Message { get; set; }
        public int Type { get; set; }
        public int UnreadCount { get; set; }
        public DateTime CreatedOn { get; set; }
        

        /// <summary>
        /// Map database records into list of objects.
        /// </summary>
        /// <param name="reader">Reader object that is bound to database.</param>
        /// <returns>Collection of Conversation objects.</returns>
        internal static IEnumerable<LastConversation> Map(DataView dView)
        {
            List<LastConversation> convList = new List<LastConversation>();
            //Logger.Info("Last Conversations Count: " + dView.Count);

            for (int i = 0; i < dView.Count; i++)
            {
                LastConversation conv = new LastConversation()
                {
                    ConversationID = dView[i]["id"].ToString(),
                    SenderID = dView[i]["senderChatUserId"].ToString(),
                    SenderChatID = dView[i]["senderChatId"].ToString(),
                    SenderConversationID = dView[i]["senderConversationId"].ToString(),
                    ReceiverID = dView[i]["receiverChatUserId"].ToString(),
                    ReceiverChatID = dView[i]["receiverChatId"].ToString(),
                    Status = (dView[i]["status"].ToString()).Trim(),
                    Message = dView[i]["message"].ToString(),
                    Type = Convert.ToInt16(dView[i]["type"]),
                    UnreadCount = Convert.ToInt16(dView[i]["count"]),
                    CreatedOn = (DateTime)dView[i]["createdDate"]
                };

                //Logger.Info(conv.ConversationID + " is the last conversation for ChatID " + conv.ReceiverChatID);

                convList.Add(conv);
            }
          
            return convList;
        }
    }
}
