﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using ChatServerCore.Data.Database;
using ChatServerCore.Data.Entity;
using ChatServerCore.Utilities;

namespace ChatServerCore.Data
{
    class ParticipantRepository : DataRepository
    {
        private DBDetails _dbDetails;

        private DBDetails DBDetails
        {
            get
            {
                if (_dbDetails == null) _dbDetails = TableNameResolver.GetDBDetails(chatUserID);
                return _dbDetails;
            }
        }

        public void Add(string senderChatID, string receiverChatID, string receiverChatUserID, bool isGroup)
        {
            try
            {
                ChatManager.PushParticipant(chatUserID, senderChatID, receiverChatUserID, receiverChatID, isGroup, DBDetails);
            }
            catch (SqlException ex)
            {
                Logger.Info(ex.Message.ToString() + " " + ex.StackTrace.ToString());
                Thread.Sleep(1);
                ChatManager.PushParticipant(chatUserID, senderChatID, receiverChatUserID, receiverChatID, isGroup, DBDetails);
            }
            
        }

        public void Remove(string senderChatID, string receiverChatUserID, string receiverChatID)
        {
            ChatManager.DeleteParticipant(senderChatID, receiverChatUserID, receiverChatID, DBDetails);

            Cache.Invalidate(String.Format("{0}-fbscid{1}", chatUserID, senderChatID));
            Cache.Invalidate(String.Format("{0}-fbscidg{1},{2}", chatUserID, senderChatID, true));
            Cache.Invalidate(String.Format("{0}-fbr{1},{2}", chatUserID, receiverChatUserID, receiverChatID));
            Cache.Invalidate(String.Format("{0}-fbrg{1},{2},{3}", chatUserID, receiverChatUserID, receiverChatID, true));
            Cache.Invalidate(String.Format("{0}-fbridg{1},{2}", chatUserID, receiverChatUserID, true));
        }

        /*
        public IEnumerable<Participant> FindBySenderChatId(string senderChatID)
        {
            string cacheKey = String.Format("{0}-fbscid{1}", chatUserID, senderChatID);
            IEnumerable<Participant> cachedItem = Cache.Get<IEnumerable<Participant>>(cacheKey);

            if (cachedItem != null)
            {
                return cachedItem;
            }
            else
            {
                cachedItem = Participant.Map(ChatManager.PullParticipantBySender(senderChatID, null, DBDetails));
                if (cachedItem.Count() != 0) Cache.Set(cacheKey, cachedItem);
                return cachedItem;
            }
        }*/

        public IEnumerable<Participant> FindBySenderChatId(string senderChatID, bool isGroup)
        {
            IEnumerable<Participant> cachedItem;

            if (!isGroup)
            {
                string cacheKey = String.Format("{0}-fbscidg{1},{2}", chatUserID, senderChatID, isGroup);
                cachedItem = Cache.Get<IEnumerable<Participant>>(cacheKey);
                if (cachedItem != null)
                {
                    return cachedItem;
                }
                else
                {
                    cachedItem = Participant.Map(ChatManager.PullParticipantBySender(senderChatID, isGroup, DBDetails));
                    return cachedItem;
                }
            }
            
            return Participant.Map(ChatManager.PullParticipantBySender(senderChatID, isGroup, DBDetails));
        }

        public IEnumerable<Participant> FindByReceiver(string receiverChatUserID, string receiverChatID)
        {
            string cacheKey = String.Format("{0}-fbr{1},{2}", DBDetails.ParticipantTableName, receiverChatUserID, receiverChatID);
            IEnumerable<Participant> cachedItem = Cache.Get<IEnumerable<Participant>>(cacheKey);

            if (cachedItem != null)
            {
                return cachedItem;
            }
            else
            {
                cachedItem = Participant.Map(ChatManager.PullParticipantByRecv(receiverChatID, receiverChatUserID, null, DBDetails));
                if (cachedItem.Count() != 0) Cache.Set(cacheKey, cachedItem);
                return cachedItem;
            }
        }

        public IEnumerable<Participant> FindByReceiver(string receiverChatUserID, string receiverChatID, bool isGroup)
        {
            string cacheKey = String.Format("{0}-fbrg{1},{2},{3}", DBDetails.ParticipantTableName, receiverChatUserID, receiverChatID, isGroup);
            IEnumerable<Participant> cachedItem = Cache.Get<IEnumerable<Participant>>(cacheKey);

            if (cachedItem != null)
            {
                return cachedItem;
            }
            else
            {
                cachedItem = Participant.Map(ChatManager.PullParticipantByRecv(receiverChatID, receiverChatUserID, isGroup, DBDetails));
                if (cachedItem.Count() != 0) Cache.Set(cacheKey, cachedItem);
                return cachedItem;
            }
        }

        public IEnumerable<Participant> FindByReceiverId(string receiverChatUserID, bool isGroup)
        {
            string cacheKey = String.Format("{0}-fbridg{1},{2},{3}", DBDetails.ParticipantTableName, chatUserID, receiverChatUserID, isGroup);
            IEnumerable<Participant> cachedItem = Cache.Get<IEnumerable<Participant>>(cacheKey);

            if (cachedItem != null)
            {
                return cachedItem;
            }
            else
            {
                cachedItem = Participant.Map(ChatManager.PullParticipantByRecvAndSender(chatUserID, receiverChatUserID, isGroup, DBDetails));
                if (cachedItem.Count() != 0) Cache.Set(cacheKey, cachedItem);
                return cachedItem;
            }
        }
    }
}
