﻿
namespace ChatServerCore.Data
{
    class Repositories<Repository> where Repository : DataRepository, new()
    {
        public Repositories()
        {
        }

        public Repository this[string chatUserID]
        {
            get
            {
                return new Repository() { chatUserID = chatUserID };
            }
        }
    }
}
