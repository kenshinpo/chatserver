﻿using System;
using ChatServerCore.Utilities;

namespace CherryCredits.Library
{
    public class Maths
    {
        /// <summary>
        /// Decimal convert to 62h
        /// </summary>
        /// <param name="value">decimal value</param>
        /// <param name="length">output length</param>
        /// <returns>62h value</returns>
        public static string DecimalToH62(long value, int length)
        {
            char[] charSet = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();

            string result = string.Empty;
            try
            {
                if (value < 62)
                {
                    result = charSet[value].ToString().PadLeft(length, '0');
                }
                else
                {
                    long tempValue = value;
                    while (tempValue > 0)
                    {
                        long val = tempValue % 62;
                        result = charSet[val] + result;
                        tempValue = tempValue / 62;
                    }
                    result = result.PadLeft(length, '0');
                }
            }
            catch (Exception ex)
            {
                result = string.Empty;
                Logger.Info(ex.Message + "\n" + ex.StackTrace.ToString());
                //UB.LogFile.Append(logPath, ex.Message);
            }
            return result;
        }

        /// <summary>
        /// 62h convert to Decimal
        /// </summary>
        /// <param name="value">62h value</param>
        /// <returns>decimal value</returns>
        public static long H62ToDecimal(string value)
        {
            long result = 0; ;
            try
            {
                string strSet = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

                for (int i = 0; i < value.Length; i++)
                {
                    int intIndexOf = strSet.IndexOf(value.Substring(i, 1));
                    Int64 intBon = 0;
                    intBon = (value.Length - 1 - i);
                    intBon = (Int64)System.Math.Pow(strSet.Length, intBon);
                    result += intIndexOf * intBon;
                }
            }
            catch (Exception ex)
            {
                result = 0;
                Logger.Info(ex.Message + "\n" + ex.StackTrace.ToString());
                //UB.LogFile.Append(logPath, ex.Message);
            }
            return result;
        }
    }
}