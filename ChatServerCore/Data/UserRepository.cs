﻿using System.Collections.Generic;
using ChatServerCore.Data.Database;

namespace ChatServerCore.Data
{
    class UserRepository : DataRepository
    {
        public int GetOperatingType()
        {
            return ChatAccountManager.GetOperatingType(chatUserID);
        }

        public string GetDeviceToken()
        {
            return ChatAccountManager.GetDeviceToken(chatUserID);
        }

        public Dictionary<string, int> InitAccount(string cID, string deviceToken)
        {
            return ChatAccountManager.InitAccount(chatUserID, cID, deviceToken);
        }

        public int GetBadge()
        {
            return ChatManager.GetUserBadge(chatUserID, TableNameResolver.GetDBDetails(chatUserID));
        }

        public void IncrementBadge()
        {
            ChatManager.RenewUserBadge(chatUserID, 2, TableNameResolver.GetDBDetails(chatUserID));
        }

        public void DecrementBadge()
        {
            ChatManager.RenewUserBadge(chatUserID, 1, TableNameResolver.GetDBDetails(chatUserID));
        }

        public void ClearBadge()
        {
            ChatManager.RenewUserBadge(chatUserID, 0, TableNameResolver.GetDBDetails(chatUserID));
        }
    }
}
