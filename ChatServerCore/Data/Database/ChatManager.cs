﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChatServerCore.Utilities;
using CherryCredits.Library;

namespace ChatServerCore.Data.Database
{
    static class ChatManager
    {
        private static readonly string ID_TYPE_CRID = "R";
        private static readonly string ID_TYPE_CMID = "M";
        private static readonly string ID_TYPE_CPID = "P";

        #region Badge
        public static int GetUserBadge(string chatUserID, DBDetails DBDetails)
        {
            int badge = 0;
            using (SqlConnection conn = new SqlConnection(DBDetails.DatabaseConString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("cmaPullUserBadge", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@chatUserID", SqlDbType.VarChar, 15).Value = chatUserID;
                SqlParameter retval = cmd.Parameters.Add("@badge", SqlDbType.BigInt);
                retval.Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                badge = Convert.ToInt32(cmd.Parameters["@badge"].Value == DBNull.Value ? 0 : cmd.Parameters["@badge"].Value);
            }

            return badge;
        }

        public static void RenewUserBadge(string chatUserID, int incrementBasis, DBDetails DBDetails)
        {
            using (SqlConnection conn = new SqlConnection(DBDetails.DatabaseConString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("cmaRenewBadge", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@chatUserID", SqlDbType.VarChar, 15).Value = chatUserID;
                cmd.Parameters.Add("@incrementBasis", SqlDbType.TinyInt).Value = incrementBasis;
                cmd.ExecuteNonQuery();
            }
        }
        #endregion

        #region Chat
        public static string PushChat(string creatorID, string title, bool isGroupChat, string groupPicURL, string admin, DBDetails DBDetails)
        {
            string newChatID = GetId(ID_TYPE_CRID, "01");

            using (SqlConnection conn = new SqlConnection(DBDetails.DatabaseConString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("cmaPushChat", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@chatID", SqlDbType.VarChar, 15).Value = newChatID;
                cmd.Parameters.Add("@creatorID", SqlDbType.VarChar, 15).Value = creatorID;

                if (title != null)
                {
                    cmd.Parameters.Add("@title", SqlDbType.NVarChar, 20).Value = title;
                }

                cmd.Parameters.Add("@isGroup", SqlDbType.Bit).Value = Convert.ToInt32(isGroupChat);

                if (groupPicURL != null)
                {
                    cmd.Parameters.Add("@groupPicURL", SqlDbType.VarChar, int.MaxValue).Value = groupPicURL;
                }

                if (admin != null)
                {
                    cmd.Parameters.Add("@admin", SqlDbType.VarChar, 15).Value = admin;
                }

                cmd.Parameters.Add("@tableName", SqlDbType.VarChar, int.MaxValue).Value = DBDetails.ChatTableName;
                cmd.ExecuteNonQuery();
            }

            return newChatID;
        }

        public static void RenewChat(string chatID, string title, string groupPicUrl, string newAdminID, bool? status, DBDetails DBDetails)
        {
            using (SqlConnection conn = new SqlConnection(DBDetails.DatabaseConString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("cmaRenewChat", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@chatID", SqlDbType.VarChar, 15).Value = chatID;
                if (title != null)
                {
                    cmd.Parameters.Add("@title", SqlDbType.NVarChar, 20).Value = title;
                }

                if (groupPicUrl != null)
                {
                    cmd.Parameters.Add("@groupPicUrl", SqlDbType.VarChar, int.MaxValue).Value = groupPicUrl;
                }

                if (newAdminID != null)
                {
                    cmd.Parameters.Add("@newAdminID", SqlDbType.VarChar, 15).Value = newAdminID;
                }

                if (status != null)
                {
                    cmd.Parameters.Add("@status", SqlDbType.Bit).Value = status;
                }

                cmd.Parameters.Add("@tableName", SqlDbType.VarChar, int.MaxValue).Value = DBDetails.ChatTableName;
                cmd.ExecuteNonQuery();
            }
        }

        public static DataView PullChatByID(string chatID, DBDetails DBDetails)
        {
            DataSet ds = new DataSet();
            using (SqlConnection conn = new SqlConnection(DBDetails.DatabaseConString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("cmaPullChat", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@chatID", SqlDbType.VarChar, 15).Value = chatID;
                cmd.Parameters.Add("@tableName", SqlDbType.VarChar, int.MaxValue).Value = DBDetails.ChatTableName;
                SqlDataAdapter dAdapter = new SqlDataAdapter(cmd);
                dAdapter.Fill(ds);
            }
            return ds.Tables[0].DefaultView;
        }

        public static DataView PullChatByID(string chatID, bool isGroup, DBDetails DBDetails)
        {
            DataSet ds = new DataSet();
            using (SqlConnection conn = new SqlConnection(DBDetails.DatabaseConString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("cmaPullChat", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@chatID", SqlDbType.VarChar, 15).Value = chatID;
                cmd.Parameters.Add("@isGroup", SqlDbType.Bit).Value = isGroup;
                cmd.Parameters.Add("@tableName", SqlDbType.VarChar, int.MaxValue).Value = DBDetails.ChatTableName;
                SqlDataAdapter dAdapter = new SqlDataAdapter(cmd);
                dAdapter.Fill(ds);
            }
            return ds.Tables[0].DefaultView;
        }


        public static DataView PullChatByGroupAndDate(string creatorID, string dateTime, DBDetails DBDetails)
        {
            DataSet ds = new DataSet();
            using (SqlConnection conn = new SqlConnection(DBDetails.DatabaseConString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("cmaPullChat", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@creatorID", SqlDbType.VarChar, 15).Value = creatorID;
                cmd.Parameters.Add("@dateTime", SqlDbType.VarChar, 23).Value = dateTime;
                cmd.Parameters.Add("@tableName", SqlDbType.VarChar, int.MaxValue).Value = DBDetails.ChatTableName;
                SqlDataAdapter dAdapter = new SqlDataAdapter(cmd);
                dAdapter.Fill(ds);
            }
            return ds.Tables[0].DefaultView;
        }

        public static DataView PullChatByGroupWithChatUserID(string creatorID, bool isGroup, DBDetails DBDetails)
        {
            DataSet ds = new DataSet();
            using (SqlConnection conn = new SqlConnection(DBDetails.DatabaseConString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("cmaPullChat", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@creatorID", SqlDbType.VarChar, 15).Value = creatorID;
                cmd.Parameters.Add("@isGroup", SqlDbType.Bit).Value = isGroup;
                cmd.Parameters.Add("@tableName", SqlDbType.VarChar, int.MaxValue).Value = DBDetails.ChatTableName;
                SqlDataAdapter dAdapter = new SqlDataAdapter(cmd);
                dAdapter.Fill(ds);
            }
            return ds.Tables[0].DefaultView;
        }

        public static void DeleteChat(string chatID, DBDetails DBDetails)
        {
            using (SqlConnection conn = new SqlConnection(DBDetails.DatabaseConString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("cmaDeleteChat", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@chatID", SqlDbType.VarChar, 15).Value = chatID;
                cmd.Parameters.Add("@tableName", SqlDbType.VarChar, int.MaxValue).Value = DBDetails.ChatTableName;
                cmd.ExecuteNonQuery();
            }
        }
        #endregion

        #region Conversation
        public static string PushConversation(string senderChatUserID, string senderChatID, string senderConversationID, string receiverChatUserID, string receiverChatID, string status, string message, int type, DBDetails DBDetails)
        {
            string newConversationID = GetId(ID_TYPE_CMID, "01");
            int statusCode = 0;

            using (SqlConnection conn = new SqlConnection(DBDetails.DatabaseConString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("cmaPushConversation", conn);

                switch (status)
                {
                    case "BLOCKED":
                        statusCode = 0;
                        break;
                    case "SNDPEND":
                        statusCode = 1;
                        break;
                    case "SENTPEND":
                        statusCode = 2;
                        break;
                    case "SENT":
                        statusCode = 3;
                        break;
                    case "MSGPEND":
                        statusCode = 4;
                        break;
                    case "RECEIVED":
                        statusCode = 5;
                        break;
                    case "INVITE_SNDPEND":
                        statusCode = 6;
                        break;
                    case "INVITE_SND":
                        statusCode = 7;
                        break;
                    case "INVITE_SENT":
                        statusCode = 8;
                        break;
                    case "INVITE_RECEIVED":
                        statusCode = 9;
                        break;
                    case "INVITE_MSGPEND":
                        statusCode = 10;
                        break;
                    case "INVITE_SNTPEND":
                        statusCode = 11;
                        break;
                    case "INVITE_!SNTPEND":
                        statusCode = 12;
                        break;
                    case "GIFT_SNDPEND":
                        statusCode = 13;
                        break;
                    case "GIFT_SND":
                        statusCode = 14;
                        break;
                    case "GIFT_SENT":
                        statusCode = 15;
                        break;
                    case "GIFT_RECEIVED":
                        statusCode = 16;
                        break;
                    case "GIFT_MSGPEND":
                        statusCode = 17;
                        break;
                    case "GIFT_SNTPEND":
                        statusCode = 18;
                        break;
                    case "GIFT_!SNTPEND":
                        statusCode = 19;
                        break;
                    case "GROUPMSG":
                        statusCode = 20;
                        break;
                    case "LEFTGRP":
                        statusCode = 21;
                        break;
                    case "REMOVEGRP":
                        statusCode = 22;
                        break;
                }

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@conversationID", SqlDbType.VarChar, 15).Value = newConversationID;
                cmd.Parameters.Add("@senderChatUserID", SqlDbType.VarChar, 15).Value = senderChatUserID;
                cmd.Parameters.Add("@senderChatID", SqlDbType.VarChar, 15).Value = senderChatID;

                if (senderConversationID != null)
                {
                    cmd.Parameters.Add("@senderConversationID", SqlDbType.VarChar, 15).Value = senderConversationID;
                }

                if (receiverChatUserID != null)
                {
                    cmd.Parameters.Add("@receiverChatUserID", SqlDbType.VarChar, 15).Value = receiverChatUserID;
                }


                if (receiverChatID != null)
                {
                    cmd.Parameters.Add("@receiverChatID", SqlDbType.VarChar, 15).Value = receiverChatID;
                }

                cmd.Parameters.Add("@status", SqlDbType.VarChar, 15).Value = status;
                cmd.Parameters.Add("@statusCode", SqlDbType.TinyInt).Value = statusCode;
                cmd.Parameters.Add("@message", SqlDbType.NVarChar, int.MaxValue).Value = message;
                cmd.Parameters.Add("@type", SqlDbType.TinyInt).Value = type;
                cmd.Parameters.Add("@tableName", SqlDbType.VarChar, int.MaxValue).Value = DBDetails.ConversationTableName;
                cmd.ExecuteNonQuery();
            }

            return newConversationID;
        }

        public static void RenewConversation(int status, string senderChatID, string conversationID, DBDetails DBDetails)
        {
            using (SqlConnection conn = new SqlConnection(DBDetails.DatabaseConString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("cmaRenewConversation", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@status", SqlDbType.TinyInt).Value = status;

                if (senderChatID != null)
                {
                    cmd.Parameters.Add("@senderChatID", SqlDbType.VarChar, 15).Value = senderChatID;
                }
                else
                {
                    cmd.Parameters.Add("@senderChatID", SqlDbType.VarChar, 15).Value = DBNull.Value;
                }

                if (conversationID != null)
                {
                    cmd.Parameters.Add("@conversationID", SqlDbType.VarChar, 15).Value = conversationID;
                }
                else
                {
                    cmd.Parameters.Add("@conversationID", SqlDbType.VarChar, 15).Value = DBNull.Value;
                }

                cmd.Parameters.Add("@tableName", SqlDbType.VarChar, int.MaxValue).Value = DBDetails.ConversationTableName;
                cmd.ExecuteNonQuery();
            }
        }

        public static void RenewGameConversation(int? status, int? sendStatus, int? sentNotReceivedStatus, string senderChatID, string receiverChatID, DBDetails DBDetails)
        {
            using (SqlConnection conn = new SqlConnection(DBDetails.DatabaseConString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("cmaRenewGameConversation", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@status", SqlDbType.TinyInt).Value = status;

                if (senderChatID != null)
                {
                    cmd.Parameters.Add("@senderChatID", SqlDbType.VarChar, 15).Value = senderChatID;
                    cmd.Parameters.Add("@sentNotReceivedStatus", SqlDbType.TinyInt).Value = sentNotReceivedStatus;
                    cmd.Parameters.Add("@sendStatus", SqlDbType.TinyInt).Value = sendStatus;
                }
                else
                {
                    cmd.Parameters.Add("@senderChatID", SqlDbType.VarChar, 15).Value = DBNull.Value;
                    cmd.Parameters.Add("@sentNotReceivedStatus", SqlDbType.TinyInt).Value = DBNull.Value;
                    cmd.Parameters.Add("@sendStatus", SqlDbType.TinyInt).Value = DBNull.Value;
                }

                if (receiverChatID != null)
                {
                    cmd.Parameters.Add("@receiverChatID", SqlDbType.VarChar, 15).Value = receiverChatID;
                }
                else
                {
                    cmd.Parameters.Add("@receiverChatID", SqlDbType.VarChar, 15).Value = DBNull.Value;
                }

                cmd.Parameters.Add("@tableName", SqlDbType.VarChar, int.MaxValue).Value = DBDetails.ConversationTableName;
                cmd.ExecuteNonQuery();
            }
        }

        public static DataView PullConversationByID(string conversationID, DBDetails DBDetails)
        {
            DataSet ds = new DataSet();
            using (SqlConnection conn = new SqlConnection(DBDetails.DatabaseConString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("cmaPullConversation", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@conversationID", SqlDbType.VarChar, 15).Value = conversationID;
                cmd.Parameters.Add("@tableName", SqlDbType.VarChar, int.MaxValue).Value = DBDetails.ConversationTableName;
                SqlDataAdapter dAdapter = new SqlDataAdapter(cmd);
                dAdapter.Fill(ds);
            }
            return ds.Tables[0].DefaultView;
        }

        public static DataView PullAllNewConversationByChatID(string chatID, string chatUserID, string lastSyncDate, DBDetails DBDetails)
        {
            DataSet ds = new DataSet();

            using (SqlConnection conn = new SqlConnection(DBDetails.DatabaseConString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("cmaPullConversation", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@chatUserID", SqlDbType.VarChar, 15).Value = chatUserID;
                cmd.Parameters.Add("@chatID", SqlDbType.VarChar, 15).Value = chatID;
                if (lastSyncDate.Length > 1)
                {
                    cmd.Parameters.Add("@dateTime", SqlDbType.VarChar, 23).Value = lastSyncDate;
                }
                else
                {
                    cmd.Parameters.Add("@dateTime", SqlDbType.VarChar, 23).Value = DBNull.Value;
                }

                cmd.Parameters.Add("@tableName", SqlDbType.VarChar, int.MaxValue).Value = DBDetails.ConversationTableName;
                SqlDataAdapter dAdapter = new SqlDataAdapter(cmd);
                dAdapter.Fill(ds);
            }

            //Logger.Info("New conversations returned from DB: " + ds.Tables[0].DefaultView.Count);
            return ds.Tables[0].DefaultView;
        }

        public static DataView PullLastConversationByPendingRecv(string receiverChatUserID, string lastSyncDate, DBDetails DBDetails)
        {
            DataSet ds = new DataSet();
            using (SqlConnection conn = new SqlConnection(DBDetails.DatabaseConString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("cmaPullLastConversation", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@chatUserID", SqlDbType.VarChar, 15).Value = receiverChatUserID;
                cmd.Parameters.Add("@dateTime", SqlDbType.VarChar, 23).Value = lastSyncDate;
                cmd.Parameters.Add("@tableName", SqlDbType.VarChar, int.MaxValue).Value = DBDetails.ConversationTableName;
                SqlDataAdapter dAdapter = new SqlDataAdapter(cmd);
                dAdapter.Fill(ds);
            }

            //Logger.Info("Last conversations returned from DB: " + ds.Tables[0].DefaultView.Count);
            return ds.Tables[0].DefaultView;
        }

        #endregion

        #region Participant
        public static void PushParticipant(string senderChatUserID, string senderChatID, string receiverChatUserID, string receiverChatID, bool isGroup, DBDetails DBDetails)
        {
            string newParticipantID = GetId(ID_TYPE_CPID, "01");

            using (SqlConnection conn = new SqlConnection(DBDetails.DatabaseConString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("cmaPushParticipant", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@participantID", SqlDbType.VarChar, 15).Value = newParticipantID;
                cmd.Parameters.Add("@senderChatID", SqlDbType.VarChar, 15).Value = senderChatID;
                cmd.Parameters.Add("@senderChatUserID", SqlDbType.VarChar, 15).Value = senderChatUserID;
                cmd.Parameters.Add("@receiverChatID", SqlDbType.VarChar, 15).Value = receiverChatID;
                cmd.Parameters.Add("@receiverChatUserID", SqlDbType.VarChar, 15).Value = receiverChatUserID;
                cmd.Parameters.Add("@isGroup", SqlDbType.Bit).Value = isGroup;
                cmd.Parameters.Add("@tableName", SqlDbType.VarChar, int.MaxValue).Value = DBDetails.ParticipantTableName;
                cmd.ExecuteNonQuery();
            }
        }

        public static void DeleteParticipant(string senderChatID, string receiverChatUserID, string receiverChatID, DBDetails DBDetails)
        {
            using (SqlConnection conn = new SqlConnection(DBDetails.DatabaseConString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("cmaRemoveParticipant", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@senderChatID", SqlDbType.VarChar, 15).Value = senderChatID;
  
                cmd.Parameters.Add("@receiverChatID", SqlDbType.VarChar, 15).Value = receiverChatID;
                cmd.Parameters.Add("@receiverChatUserID", SqlDbType.VarChar, 15).Value = receiverChatUserID;
                cmd.Parameters.Add("@tableName", SqlDbType.VarChar, int.MaxValue).Value = DBDetails.ParticipantTableName;
                cmd.ExecuteNonQuery();
            }
        }

        public static DataView PullParticipantBySender(string senderChatID, bool? isGroup, DBDetails DBDetails)
        {
            DataSet ds = new DataSet();
            // Get participant chatIDs
            using (SqlConnection conn = new SqlConnection(DBDetails.DatabaseConString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("cmaPullParticipant", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@senderChatID", SqlDbType.VarChar, 15).Value = senderChatID;

                if (isGroup != null)
                {
                    cmd.Parameters.Add("@isGroup", SqlDbType.Bit).Value = isGroup;
                }

                cmd.Parameters.Add("@tableName", SqlDbType.VarChar, int.MaxValue).Value = DBDetails.ParticipantTableName;
                SqlDataAdapter dAdapter = new SqlDataAdapter(cmd);
                dAdapter.Fill(ds);
            }

            DataView participantTable = ds.Tables[0].DefaultView;

            List<bool> chatStatus = new List<bool>();
            // Using the participant chatIDs, get the corresponding chat status
            for (int i = 0; i < participantTable.Count; i++)
            {
                DataView chatTable = PullChatByID(participantTable[i]["receiverChatId"].ToString(), TableNameResolver.GetDBDetails(participantTable[i]["receiverChatUserId"].ToString()));
                if (chatTable.Count == 0)
                {
                    Logger.Info("ChatID does not exist: " + participantTable[i]["receiverChatId"].ToString() + " (" + TableNameResolver.GetDBDetails(participantTable[i]["receiverChatUserId"].ToString()).ChatTableName + ")");
                    chatStatus.Add(false);
                }
                else
                {
                    chatStatus.Add((bool)chatTable[0]["status"]);
                }

            }
            
            // Create corresponding table
            DataTable table = new DataTable();
            table.Columns.Add("senderChatId", typeof(string));
            table.Columns.Add("senderChatUserId", typeof(string));
            table.Columns.Add("receiverChatId", typeof(string));
            table.Columns.Add("receiverChatUserId", typeof(string));
            table.Columns.Add("isGroup", typeof(bool));
            table.Columns.Add("receiverChatStatus", typeof(bool));
            table.Columns.Add("createdDate", typeof(DateTime));

            for (int i = 0; i < participantTable.Count; i++)
            {
                table.Rows.Add(participantTable[i]["senderChatId"].ToString(),
                                participantTable[i]["senderChatUserId"].ToString(),
                                participantTable[i]["receiverChatId"].ToString(),
                                participantTable[i]["receiverChatUserId"].ToString(),
                                (bool)participantTable[i]["isGroup"],
                                (bool)chatStatus[i],
                                participantTable[i]["createdDate"].ToString());
            }

            return table.AsDataView();
        }

        public static DataView PullParticipantByRecv(string receiverChatID, string receiverChatUserID, bool? isGroup, DBDetails DBDetails)
        {
            DataSet ds = new DataSet();
            using (SqlConnection conn = new SqlConnection(DBDetails.DatabaseConString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("cmaPullParticipant", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@receiverChatUserID", SqlDbType.VarChar, 15).Value = receiverChatUserID;
                cmd.Parameters.Add("@receiverChatID", SqlDbType.VarChar, 15).Value = receiverChatID;

                if (isGroup != null)
                {
                    cmd.Parameters.Add("@isGroup", SqlDbType.Bit).Value = isGroup;
                }

                cmd.Parameters.Add("@tableName", SqlDbType.VarChar, int.MaxValue).Value = DBDetails.ParticipantTableName;
                SqlDataAdapter dAdapter = new SqlDataAdapter(cmd);
                dAdapter.Fill(ds);
            }
            return ds.Tables[0].DefaultView;
        }

        public static DataView PullParticipantByRecvAndSender(string senderChatUserID, string receiverChatUserID, bool? isGroup, DBDetails DBDetails)
        {
            DataSet ds = new DataSet();
            using (SqlConnection conn = new SqlConnection(DBDetails.DatabaseConString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("cmaPullParticipant", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@receiverChatUserID", SqlDbType.VarChar, 15).Value = receiverChatUserID;
                cmd.Parameters.Add("@senderChatUserID", SqlDbType.VarChar, 15).Value = senderChatUserID;

                if (isGroup != null)
                {
                    cmd.Parameters.Add("@isGroup", SqlDbType.Bit).Value = isGroup;
                }

                cmd.Parameters.Add("@tableName", SqlDbType.VarChar, int.MaxValue).Value = DBDetails.ParticipantTableName;
                SqlDataAdapter dAdapter = new SqlDataAdapter(cmd);
                dAdapter.Fill(ds);
            }
            return ds.Tables[0].DefaultView;
        }

        #endregion

        private static string GetId(string id_type, string databaseId)
        {
            string id = string.Empty;
            try
            {
                id = id_type + databaseId + Maths.DecimalToH62(RandomNumber(0, 3843), 2) +
                    Maths.DecimalToH62(DateTime.Now.Year, 3) + Maths.DecimalToH62(DateTime.Now.Month, 1) + Maths.DecimalToH62(DateTime.Now.Day, 1) + Maths.DecimalToH62(DateTime.Now.Hour, 1) + Maths.DecimalToH62(DateTime.Now.Minute, 1) + Maths.DecimalToH62(DateTime.Now.Second, 1) + Maths.DecimalToH62(DateTime.Now.Millisecond, 2);
            }
            catch (Exception ex)
            {
                id = string.Empty;
                Logger.Info(ex.Message + "\n" + ex.StackTrace.ToString());
            }
            return id;
        }

        private static readonly Random random = new Random();
        private static readonly object syncLock = new object();
        public static int RandomNumber(int min, int max)
        {
            lock (syncLock)
            { // synchronize
                return random.Next(min, max);
            }
        }
    }
}
