﻿using ChatServerCore.Utilities;
using System;

namespace ChatServerCore.Data.Database
{
    /// <summary>
    /// Static helper class that resolve the table name for given user ID.
    /// </summary>
    static class TableNameResolver
    {
        public static DBDetails GetDBDetails(string chatUserID)
        {
            string cacheKey = String.Format("gctn{0}", chatUserID);
            DBDetails cachedItem = Cache.Get<DBDetails>(cacheKey);

            if (cachedItem != null)
            {
                return cachedItem;
            }
            else
            {
                cachedItem = ChatAccountManager.GetUserRegion(chatUserID);
                Cache.Set(cacheKey, cachedItem);
                return cachedItem;
            }
        }
    }
}
