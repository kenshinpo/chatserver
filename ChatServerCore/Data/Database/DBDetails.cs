﻿
namespace ChatServerCore.Data.Database
{
    class DBDetails
    {
        public string ChatTableName {get; set;}
        public string ConversationTableName { get; set; }
        public string ParticipantTableName { get; set; }
        public string DatabaseConString { get; set; }
    }
}
