﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Collections;

namespace ChatServerCore.Data.Database
{
    enum UserRegion
    {
        eastern_africa,
        middle_africa,
        northern_africa,
        southern_africa,
        western_africa,
        caribbean,
        central_america,
        south_america,
        northern_america,
        central_asia,
        eastern_asia,
        southern_asia,
        south_eastern_asia,
        western_asia,
        eastern_europe,
        northern_europe,
        southern_europe,
        western_europe,
        australia_and_newzealand,
        melanesia,
        micronesia,
        polynesia,
        unknown,
        antarctic,
        oceania_unknown
    }

    static class ChatAccountManager
    {
        private static string ConnStr = ConfigurationManager.ConnectionStrings["cma"].ToString();

        public static DBDetails GetUserRegion(string chatUserID)
        {
            int row = 0;
            int db = 3;
            using (SqlConnection conn = new SqlConnection(ConnStr))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("cmaPullUserRegion", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@chatUserID", SqlDbType.VarChar, 15).Value = chatUserID;
                SqlParameter retval = cmd.Parameters.Add("@regionID", SqlDbType.SmallInt);
                retval.Direction = ParameterDirection.Output;
                retval = cmd.Parameters.Add("@db", SqlDbType.TinyInt);
                retval.Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                row = Convert.ToInt16(cmd.Parameters["@regionID"].Value);
                db = Convert.ToInt16(cmd.Parameters["@db"].Value);
            }

            string tableName = Enum.GetName(typeof(UserRegion), row - 1);
            //Logger.Info("Tablename: " + tableName);

            DBDetails dbDetails = new DBDetails();
            dbDetails.DatabaseConString = ConfigurationManager.ConnectionStrings[String.Format("chatdb{0}", db)].ToString();
            dbDetails.ChatTableName = String.Format("tbl_cm_{0}_chat", tableName);
            dbDetails.ConversationTableName = String.Format("tbl_cm_{0}_conversation", tableName);
            dbDetails.ParticipantTableName = String.Format("tbl_cm_{0}_participant", tableName);

            return dbDetails;
        }

        public static string GetUserName(string senderChatUserID, string receiverChatUserID)
        {
            string assignedName = string.Empty;
            using (SqlConnection conn = new SqlConnection(ConnStr))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("cmaPullAssignedName", conn);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@receiverChatUserID", SqlDbType.VarChar, 15).Value = receiverChatUserID;
                cmd.Parameters.Add("@senderChatUserID", SqlDbType.VarChar, 15).Value = senderChatUserID;
                SqlParameter retval = cmd.Parameters.Add("@assignedName", SqlDbType.NVarChar, 30);
                retval.Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                assignedName = cmd.Parameters["@assignedName"].Value.ToString();
            }

            return (string.IsNullOrEmpty(assignedName) ? "A friend": assignedName);
        }

        public static string[] GetUserProfileName(string[] chatUsersID)
        {
            string[] profileNames = new string[chatUsersID.Length];

            using (SqlConnection conn = new SqlConnection(ConnStr))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("cmaPullProfileName", conn);

                cmd.CommandType = CommandType.StoredProcedure; 
                cmd.Parameters.Add("@chatUserID", SqlDbType.VarChar, 15);
                SqlParameter retval = cmd.Parameters.Add("@profileName", SqlDbType.NVarChar, 20);
                retval.Direction = ParameterDirection.Output;

                for (int index = 0; index < chatUsersID.Length; index++)
                {
                    cmd.Parameters["@chatUserID"].Value = chatUsersID[index];
                    cmd.ExecuteNonQuery();
                    profileNames[index] = cmd.Parameters["@profileName"].Value.ToString();
                }

            }

            return profileNames;
        }

        public static Dictionary<string, bool> IsUserBlocked(string senderChatUserID, string receiverChatUserID)
        {
            int isBlocked = 0;
            int isNotificationAllow = 0;
            using (SqlConnection conn = new SqlConnection(ConnStr))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("cmaPullRelationship", conn);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@ownerChatUserID", SqlDbType.VarChar, 15).Value = senderChatUserID;
                cmd.Parameters.Add("@friendChatUserID", SqlDbType.VarChar, 15).Value = receiverChatUserID;
                SqlParameter retval = cmd.Parameters.Add("@isBlocked", SqlDbType.Bit);
                retval.Direction = ParameterDirection.Output;
                retval = cmd.Parameters.Add("@isNotificationAllow", SqlDbType.Bit);
                retval.Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                isBlocked = Convert.ToInt16(cmd.Parameters["@isBlocked"].Value);
                isNotificationAllow = Convert.ToInt16(cmd.Parameters["@isNotificationAllow"].Value);
            }
            
            Dictionary<string, bool> dict = new Dictionary<string,bool>();
            dict.Add("isBlocked", Convert.ToBoolean(isBlocked));
            dict.Add("isNotificationAllow", Convert.ToBoolean(isNotificationAllow));

            return dict;
        }

        public static int GetOperatingType(string chatUserID)
        {
            int operatingType = 0;
            using (SqlConnection conn = new SqlConnection(ConnStr))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("cmaPullOperatingType", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@chatUserID", SqlDbType.VarChar, 15).Value = chatUserID;
                SqlParameter retval = cmd.Parameters.Add("@operatingType", SqlDbType.TinyInt);
                retval.Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                operatingType = Convert.ToInt16(cmd.Parameters["@operatingType"].Value);
            }

            return operatingType;
            
        }

        public static string GetDeviceToken(string chatUserID)
        {
            string deviceToken = string.Empty;
            using (SqlConnection conn = new SqlConnection(ConnStr))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("cmaPullDeviceToken", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@chatUserID", SqlDbType.VarChar, 15).Value = chatUserID;
                SqlParameter retval = cmd.Parameters.Add("@deviceToken", SqlDbType.VarChar, 512);
                retval.Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                deviceToken = cmd.Parameters["@deviceToken"].Value.ToString();
            }

            return deviceToken;
        }


        public static Dictionary<string, int> InitAccount(string chatUserID, string cID, string currentDeviceToken)
        {
            Dictionary<string, int> dictReturned = new Dictionary<string, int>();
            int isValid = 0;
            int isCherryAccountLogin = 0;
            using (SqlConnection conn = new SqlConnection(ConnStr))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("cmaInitAccount", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@chatUserID", SqlDbType.VarChar, 15).Value = chatUserID;

                if (string.IsNullOrEmpty(cID))
                {
                    cmd.Parameters.Add("@cID", SqlDbType.VarChar, 15).Value = DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@cID", SqlDbType.VarChar, 15).Value = cID;
                }

                cmd.Parameters.Add("@currentDeviceToken", SqlDbType.VarChar, 512).Value = currentDeviceToken;
                SqlParameter retval = cmd.Parameters.Add("@isValid", SqlDbType.Bit);
                retval.Direction = ParameterDirection.Output;
                retval = cmd.Parameters.Add("@isCherryAccountLogin", SqlDbType.Bit);
                retval.Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                isValid = Convert.ToInt16(cmd.Parameters["@isValid"].Value != DBNull.Value ? cmd.Parameters["@isValid"].Value : 0);
                isCherryAccountLogin = Convert.ToInt16(cmd.Parameters["@isCherryAccountLogin"].Value != DBNull.Value ? cmd.Parameters["@isCherryAccountLogin"].Value : 0);

                dictReturned.Add("isValid", isValid);
                dictReturned.Add("isCherryAccountLogin", isCherryAccountLogin);
            }

            return dictReturned;
        }

        public static ArrayList GetFriendsForUser(string chatUserID)
        {
            ArrayList friendList = new ArrayList();
            DataSet ds = new DataSet();
            using (SqlConnection conn = new SqlConnection(ConnStr))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("cmaPullFriendIDForUser", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@ownerChatUserID", SqlDbType.VarChar, 15).Value = chatUserID;
                SqlDataAdapter dAdapter = new SqlDataAdapter(cmd);
                dAdapter.Fill(ds);
            }

            DataView dView = ds.Tables[0].DefaultView;

            for (int i = 0; i < dView.Count; i++)
            {
                friendList.Add(dView[i]["friendChatUserID"].ToString());
            }

            return friendList;
        }

        public static ArrayList GetChatUserIDFromCherryID(ArrayList cherryIDs)
        {
            ArrayList chatUserIDs = new ArrayList();

            using (SqlConnection conn = new SqlConnection(ConnStr))
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand("cmaPullChatUserInfoWithCherryID", conn);
                cmd.Parameters.Add("@cID", SqlDbType.VarChar, 15);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter retval = cmd.Parameters.Add("@chatUserID", SqlDbType.VarChar, 15);
                retval.Direction = ParameterDirection.Output;

                for (int index = 0; index < cherryIDs.Count; index++)
                {
                    cmd.Parameters["@cID"].Value = cherryIDs[index];
                    cmd.ExecuteNonQuery();
                    chatUserIDs.Add(cmd.Parameters["@chatUserID"].Value.ToString());
                }

            }
          

            return chatUserIDs;
        }

        public static string GetDefaultMessageFromEvent(string adminChatUserID)
        {
            string defaultMessage = string.Empty;

            using (SqlConnection conn = new SqlConnection(ConnStr))
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand("cmaPullGameDefaultMessage", conn);
                cmd.Parameters.Add("@adminChatUserID", SqlDbType.VarChar, 15).Value = adminChatUserID;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter retval = cmd.Parameters.Add("@defaultText", SqlDbType.NVarChar, 100);
                retval.Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                defaultMessage = cmd.Parameters["@defaultText"].Value != DBNull.Value ? cmd.Parameters["@defaultText"].Value.ToString() : string.Empty;
            }

            return defaultMessage;
        }
    }
}
