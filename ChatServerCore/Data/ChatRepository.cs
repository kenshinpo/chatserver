﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using ChatServerCore.Data.Database;
using ChatServerCore.Data.Entity;
using ChatServerCore.Utilities;

namespace ChatServerCore.Data
{
    /// <summary>
    /// Interface type to data source. Provide variety of query to data source.
    /// </summary>
    class ChatRepository : DataRepository
    {
        private DBDetails _dbDetails;

        private DBDetails DBDetails
        {
            get
            {
                if (_dbDetails == null) _dbDetails = TableNameResolver.GetDBDetails(chatUserID);
                return _dbDetails;
            }
        }

        /// <summary>
        /// Create a new chat record in data source.
        /// </summary>
        /// <param name="title">The chat title.</param>
        /// <param name="isGroupChat">Indicate if the new chat is group chat.</param>
        /// <param name="adminId">ID of group chat administrator.</param>
        /// <returns>The ID of new created chat.</returns>
        public string Create(bool isGroupChat, string title, string groupPicURL, string admin)
        {
            string chatID = string.Empty; 
            try
            {
                chatID = ChatManager.PushChat(chatUserID, title, isGroupChat, groupPicURL, admin, DBDetails);
            }
            catch (SqlException ex)
            {
                Logger.Info(ex.Message.ToString() + " " + ex.StackTrace.ToString());
                Thread.Sleep(1);
                chatID = ChatManager.PushChat(chatUserID, title, isGroupChat, groupPicURL, admin, DBDetails);
            }

            return chatID;
        }

        /// <summary>
        /// Update the chat title of a chat.
        /// </summary>
        /// <param name="chatId">ID of the chat to be updated.</param>
        /// <param name="title">The new title.</param>
        public void UpdateTitle(string chatID, string title)
        {
            ChatManager.RenewChat(chatID, title, null, null, null, DBDetails);
        }

        /// <summary>
        /// Update the group chat profile picture.
        /// </summary>
        /// <param name="chatId">ID of the chat to be updated.</param>
        /// <param name="picPath">The path of the new picture.</param>
        public void UpdatePicture(string chatID, string groupPicURL)
        {
            ChatManager.RenewChat(chatID, null, groupPicURL, null, null, DBDetails);
        }

        /// <summary>
        /// Update admin of group.
        /// </summary>
        /// <param name="chatId">ID of the chat to be updated.</param>
        /// <param name="newAdminID">The new admin ID.</param>
        public void UpdateAdmin(string chatID, string newAdminID)
        {
            ChatManager.RenewChat(chatID, null, null, newAdminID, null, DBDetails);
        }

        /// <summary>
        /// Update status of group.
        /// </summary>
        /// <param name="chatId">status of the chat to be updated.</param>
        /// <param name="status">The new status.</param>
        public void UpdateStatus(string chatID, bool status)
        {
            ChatManager.RenewChat(chatID, null, null, null, status, DBDetails);
        }

        /// <summary>
        /// Mark the chat as updated so the server will synchronize the info of this chat to user.
        /// </summary>
        /// <param name="chatId">The ID to be updated.</param>
        /// <returns>The number of row affected in data source after executing this query.</returns>
        public void MarkAsUpdated(string chatID)
        {
            ChatManager.RenewChat(chatID, null, null, null, null, DBDetails);
        }

        /// <summary>
        /// Find a chat by its ID.
        /// </summary>
        /// <param name="chatId">The ID of desired chat.</param>
        /// <returns>Object representation of chat record.</returns>
        public Chat GetById(string chatID)
        {
            return Chat.Map(ChatManager.PullChatByID(chatID, DBDetails)).FirstOrDefault();
        }

        /// <summary>
        /// Find a chat by its ID and specify if it is a group chat.
        /// </summary>
        /// <param name="chatId">The ID of desired chat.</param>
        /// <param name="isGroup">Indicate if the chat is a group chat.</param>
        /// <returns>Object representation of chat record.</returns>
        public Chat GetById(string chatID, bool isGroup)
        {
            return Chat.Map(ChatManager.PullChatByID(chatID, isGroup, DBDetails)).FirstOrDefault();
        }

        /// <summary>
        /// Find any chat that was updated since certain time.
        /// </summary>
        /// <param name="datetime">The reference time.</param>
        /// <returns>A collection of chat objects.</returns>
        public IEnumerable<Chat> FindGroupSince(string dateTime)
        {
            return Chat.Map(ChatManager.PullChatByGroupAndDate(chatUserID, dateTime, DBDetails));
        }

        public IEnumerable<Chat> FindGroupWithChatUserID(bool isGroup)
        {
            return Chat.Map(ChatManager.PullChatByGroupWithChatUserID(chatUserID, isGroup, DBDetails));
        }

        /// <summary>
        /// Delete a chat.
        /// </summary>
        /// <param name="chatId">The ID of chat to be deleted.</param>
        /// <returns>Object representation of chat record.</returns>
        public void Delete(string chatID)
        {
            ChatManager.DeleteChat(chatID, DBDetails);
        }
    }
}
