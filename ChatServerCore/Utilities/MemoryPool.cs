﻿using System.Collections.Generic;

namespace ChatServerCore.Utilities
{
    /// <summary>
    /// A pool of cached and reusable objects that help to optimize memory usage.
    /// </summary>
    /// <typeparam name="Type">The type of cached array of pool.</typeparam>
    class MemoryPool<Type>
    {
        private object syncRoot = new object();
        private int chrunkSize;
        private int maxPoolSize;
        private Stack<Type[]> pool;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="chrunkSize">The length of array spawn from this pool.</param>
        /// <param name="maxPoolSize">The maximum size of this pool in term of array count.</param>
        public MemoryPool(int chrunkSize, int maxPoolSize = 100)
        {
            pool = new Stack<Type[]>();
            this.chrunkSize = chrunkSize;
            this.maxPoolSize = maxPoolSize;
        }

        /// <summary>
        /// Allocate a new array, it might be poped from cache or newly created.
        /// </summary>
        /// <returns>The allocated array.</returns>
        public Type[] Allocate()
        {
            lock(syncRoot)
            {
                if (pool.Count == 0) return new Type[chrunkSize];
                return pool.Pop();
            }
        }

        /// <summary>
        /// Recycle array so it could be reused later.
        /// </summary>
        /// <param name="data">The array to be recycled.</param>
        /// <param name="recycleHint">The length of dirty data that need to be erased.</param>
        public void Recycle(Type[] data, int recycleHint)
        {
            lock (syncRoot)
            {
                if (pool.Count < maxPoolSize)
                {
                    //Array.Clear(data, 0, recycleHint);
                    pool.Push(data);
                }
            }
        }

        /// <summary>
        /// Recycle array so it could be reused later. This overload erase the whole array.
        /// </summary>
        /// <param name="data">The array to be recycled.</param>
        public void Recycle(Type[] data)
        {
            lock (syncRoot)
            {
                if (pool.Count < maxPoolSize)
                {
                    //Array.Clear(data, 0, data.Length);
                    pool.Push(data);
                }
            }
        }
    }
}
