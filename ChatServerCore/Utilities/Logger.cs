﻿using System;
using System.Configuration;

namespace ChatServerCore.Utilities
{
    public static class Logger
    {
        public static string logPath = ConfigurationManager.AppSettings["logPath"].ToString();

        static Logger()
        {
            System.Diagnostics.Debug.AutoFlush = true;
        }

        /// <summary>
        /// Convinient log method with date and time prefix.
        /// </summary>
        /// <param name="str">The string to be logged.</param>
        public static void Info(string str)
        {
            System.Diagnostics.Trace.WriteLine("[@" + DateTime.Now + "]: " + str);
            System.Diagnostics.Debug.WriteLine("[@" + DateTime.Now + "]: " + str);

            UB.LogFile.Append(logPath, str);
        }

        /// <summary>
        /// Convinient log method with date and time prefix. Can be turned on by #define DEBUG.
        /// </summary>
        /// <param name="str">The string to be logged.</param>
        //[Conditional("DEBUG")]
        public static void Debug(string str)
        {
            System.Diagnostics.Trace.WriteLine("[@" + DateTime.Now + "]: " + str);
            System.Diagnostics.Debug.WriteLine("[@" + DateTime.Now + "]: " + str);
        }
    }
}
