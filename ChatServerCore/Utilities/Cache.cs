﻿using System;
using System.Runtime.Caching;

namespace ChatServerCore.Utilities
{
    static class Cache
    {
        private static MemoryCache cache = MemoryCache.Default;

        public static object Get(string key)
        {
            if (cache.Contains(key)) return cache[key];
            else return null;
        }

        public static T Get<T>(string key) where T : class
        {
            if (cache.Contains(key)) return (T)cache[key];
            else return null;
        }

        public static void Set(string key, object value)
        {
            cache.Add(key, value, DateTimeOffset.Now + TimeSpan.FromMinutes(30.0d));
        }

        public static void Invalidate(string key)
        {
            if(cache.Contains(key)) cache.Remove(key);
        }
    }
}
