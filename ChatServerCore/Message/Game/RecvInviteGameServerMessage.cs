﻿
namespace ChatServerCore.Message
{
    [MessageType("<@RECVINVITE_GAMESERVER>", "<@EOF>")]
    class RecvInviteGameServerMessage : MessageType<RecvInviteGameServerMessage>
    {
        [MessageTag("<@CHATID>")]
        public string ChatID;

        [MessageTag("<@MSGID>")]
        public string MsgID;

        [MessageTag("<@SENDERCHATUSERID>")]
        public string SenderChatUserID;

        [MessageTag("<@RECVCHATUSERID>")]
        public string RecvChatUserID;

        [MessageTag("<@MESSAGE>")]
        public string Message;
    }
}
