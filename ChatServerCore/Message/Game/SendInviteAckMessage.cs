﻿
namespace ChatServerCore.Message
{
    [MessageType("<@SENDINVITEACK>", "<@EOF>")]
    class SendInviteAckMessage : MessageType<SendInviteAckMessage>
    {
        [MessageTag("<@CHATID>")]
        public string ChatID;

        [MessageTag("<@MSGID>")]
        public string MsgID;
    }
}
