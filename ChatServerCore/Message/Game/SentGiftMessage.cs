﻿
namespace ChatServerCore.Message
{
    [MessageType("<@SENTGIFT>", "<@EOF>")]
    class SentGiftMessage : MessageType<SentGiftMessage>
    {
        [MessageTag("<@CHATID>")]
        public string ChatID;

        [MessageTag("<@MSGID>")]
        public string MsgID;
    }
}
