﻿
namespace ChatServerCore.Message
{
    [MessageType("<@SENDGIFTACK>", "<@EOF>")]
    class SendGiftAckMessage : MessageType<SendGiftAckMessage>
    {
        [MessageTag("<@CHATID>")]
        public string ChatID;

        [MessageTag("<@MSGID>")]
        public string MsgID;
    }
}
