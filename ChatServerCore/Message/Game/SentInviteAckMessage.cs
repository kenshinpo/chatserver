﻿
namespace ChatServerCore.Message
{
    [MessageType("<@SENTINVITEACK>", "<@EOF>")]
    class SentInviteAckMessage : MessageType<SentInviteAckMessage>
    {
        [MessageTag("<@CHATID>")]
        public string ChatID;

        [MessageTag("<@MSGID>")]
        public string MsgID;
    }
}
