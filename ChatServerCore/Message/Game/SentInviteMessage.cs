﻿
namespace ChatServerCore.Message
{
    [MessageType("<@SENTINVITE>", "<@EOF>")]
    class SentInviteMessage : MessageType<SentInviteMessage>
    {
        [MessageTag("<@CHATID>")]
        public string ChatID;

        [MessageTag("<@MSGID>")]
        public string MsgID;
    }
}
