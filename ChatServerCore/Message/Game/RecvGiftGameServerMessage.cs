﻿
namespace ChatServerCore.Message
{
    [MessageType("<@RECVGIFT_GAMESERVER>", "<@EOF>")]
    class RecvGiftGameServerMessage : MessageType<RecvGiftGameServerMessage>
    {
        [MessageTag("<@CHATID>")]
        public string ChatID;

        [MessageTag("<@MSGID>")]
        public string MsgID;

        [MessageTag("<@SENDERCHATUSERID>")]
        public string SenderChatUserID;

        [MessageTag("<@RECVCHATUSERID>")]
        public string RecvChatUserID;

        [MessageTag("<@MESSAGE>")]
        public string Message;

    }
}
