﻿
namespace ChatServerCore.Message
{
    [MessageType("<@SENTGIFTACK>", "<@EOF>")]
    class SentGiftAckMessage : MessageType<SentGiftAckMessage>
    {
        [MessageTag("<@CHATID>")]
        public string ChatID;

        [MessageTag("<@MSGID>")]
        public string MsgID;
    }
}
