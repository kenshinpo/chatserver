﻿
namespace ChatServerCore.Message
{
    [MessageType("<@RECVGIFTACK>", "<@EOF>")]
    class RecvGiftAckMessage : MessageType<RecvGiftAckMessage>
    {
        [MessageTag("<@CHATID>")]
        public string ChatID;

        [MessageTag("<@MSGID>")]
        public string MsgID;
    }
}
