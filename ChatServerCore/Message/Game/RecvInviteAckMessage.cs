﻿
namespace ChatServerCore.Message
{
    [MessageType("<@RECVINVITEACK>", "<@EOF>")]
    class RecvInviteAckMessage : MessageType<RecvInviteAckMessage>
    {
        [MessageTag("<@CHATID>")]
        public string ChatID;

        [MessageTag("<@MSGID>")]
        public string MsgID;

    }
}
