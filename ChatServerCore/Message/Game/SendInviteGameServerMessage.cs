﻿
namespace ChatServerCore.Message
{
    [MessageType("<@SENDINVITATION>", "<@EOF>")]
    class SendInviteGameServerMessage : MessageType<SendInviteGameServerMessage>
    {
        //<@SENDINVITATION>{0}<@SENDERCHERRYID>{1}<@RECVCHERRYIDS>{2}<@GAMEID>{3}<@GAMENAME>{4}<@GAMEICONURL>{5}<@IOSURLSCHEME>{6}<@ISODOWNLOAD>{7}<@ANDPACKAGE>{8}<@ANDCLASS>{9}<@ANDDOWNLOAD>{10}<@SENDERMSG>{11}<@RECVMSG>

        [MessageTag("<@SENDERCHERRYID>")]
        public string SenderCherryID;

        [MessageTag("<@RECVCHERRYIDS>")]
        public string ReceiverCherryIDs;

        [MessageTag("<@GAMEID>")]
        public int GameID;

        [MessageTag("<@GAMENAME>")]
        public string GameName;

        [MessageTag("<@GAMEICONURL>")]
        public string GameIconURL;

        [MessageTag("<@IOSURLSCHEME>")]
        public string IOSURLScheme;

        [MessageTag("<@ISODOWNLOAD>")]
        public string IOSDownload;

        [MessageTag("<@ANDPACKAGE>")]
        public string AndroidPackageName;

        [MessageTag("<@ANDCLASS>")]
        public string AndroidClassName;

        [MessageTag("<@ANDDOWNLOAD>")]
        public string AndroidDownload;

        [MessageTag("<@SENDERMSG>")]
        public string SenderMessage;

        [MessageTag("<@RECVMSG>")]
        public string ReceiverMessage;
    }
}
