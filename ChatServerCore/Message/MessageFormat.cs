﻿
namespace ChatServerCore.Message
{
    /**
     *  The schema for all kind of messages.
     */
    static class MessageFormat
    {
        #region Init
        // Client sends this message to server when it is logging in.
        public static string[] INIT = { "<@INIT>", "<@CHATUSERID>", "<@CID>", "<@OPERATINGTYPE>", "<@DEVTOKEN>", "<@EOF>" };

        // Server reply this message to client upon success log in.
        public static string[] INITACK = { "<@INITACK>", "<@CHATUSERID>", "<@EOF>" };
        #endregion 

        #region Send (Sender)
        // Client sends this message to server for sending media to other user.
        public static string[] SENDMEDIA = { "<@SENDMEDIA>", "<@CHATID>", "<@TEMP>", "<@MESSAGE>", "<@TYPE>", "<@EOF>" };

        // Client sends this message to server for sending a text message to other user.
        public static string[] SEND = { "<@SEND>", "<@CHATID>", "<@TEMP>", "<@MESSAGE>", "<@TYPE>", "<@EOF>" };

        // Server acknowledge client that the messsage is received.
        public static string[] SENDACK = { "<@SENDACK>", "<@CHATID>", "<@MSGID>", "<@TEMP>", "<@EOF>" };

        // Server reply the sender that the other user received the text message it sent previously.
        public static string[] SENT = { "<@SENT>", "<@CHATID>", "<@MSGID>", "<@EOF>" };

        // Client acknowledge server that it has received the notification.
        public static string[] SENTACK = { "<@SENTACK>", "<@CHATID>", "<@MSGID>", "<@EOF>" }; 
        #endregion

        #region Receive (Receiver)
        // Server sends this message to client whenever somebody sent a media to it.
        public static string[] RECVMEDIA = { "<@RECVMEDIA>", "<@CHATID>", "<@MSGID>", "<@SENDERCHATUSERID>", "<@TIMESTAMP>", "<@MESSAGE>", "<@TYPE>", "<@EOF>" };
        
        // Server sends this message to client whenever somebody sent a text message to it.
        public static string[] RECV = { "<@RECV>", "<@CHATID>", "<@MSGID>", "<@SENDERCHATUSERID>", "<@TIMESTAMP>", "<@MESSAGE>", "<@TYPE>", "<@EOF>" };

        // Client acknowledge server once that he/she has received the text message.
        public static string[] RECVACK = { "<@RECVACK>", "<@CHATID>", "<@MSGID>", "<@EOF>" }; 
        #endregion

        #region Group
        // Client sends this message to server to create a new chat group.
        public static string[] GROUPCREATE = { "<@GROUPCREATE>", "<@TITLE>", "<@TEMP>", "<@MEMBERS>", "<@EOF>" };
        
        // Server acknowledge creator that the chat group has been created.
        public static string[] GROUPCREATEACK = { "<@GROUPCREATEACK>", "<@CHATID>", "<@TEMP>", "<@EOF>" };
        
        // Client sends this message to server to add new participant into existing chat group.
        public static string[] GROUPADD = { "<@GROUPADD>", "<@MEMBERS>", "<@CHATID>", "<@EOF>" };

        // Server acknowledge adder that member(s) already added.
        public static string[] GROUPADDACK = { "<@GROUPADDACK>", "<@CHATID>", "<@EOF>" };

        // Client sends this message to server to leave a chat group.
        public static string[] GROUPLEAVE = { "<@GROUPLEAVE>", "<@CHATID>", "<@EOF>" };

        // Client sends this message to server to remove a member from a chat group.
        public static string[] GROUPREMOVE = { "<@GROUPREMOVE>", "<@REMOVEDMEMBERID>", "<@CHATID>", "<@EOF>" };

        // Server sends this message to client who leave.
        public static string[] GROUPLEAVEACK = { "<@GROUPLEAVEACK>", "<@CHATID>", "<@EOF>" };

        // Client send update message to server for title/group pic update.
        public static string[] GROUPUPDATE = { "<@GROUPUPDATE>", "<@CHATID>", "<@TITLE>", "<@GROUPPICURL>", "<@EOF>" };

        // Server acknowledge to client who made the changes.
        public static string[] GROUPUPDATEACK = { "<@GROUPUPDATEACK>", "<@CHATID>", "<@TITLE>", "<@GROUPPICURL>", "<@EOF>" };

        // Server sends this message to all member within the chat group to notify everyone about an recent update of the chat group.
        public static string[] GROUPSYNC = { "<@GROUPSYNC>", "<@CHATID>", "<@TITLE>", "<@GROUPPICURL>", "<@ADMIN>", "<@MODIFIEDBY>", "<@MEMBERS>", "<@TIMESTAMP>", "<@EOF>" }; 
        #endregion

        #region Sync
        // Client sends this message to server to request synchronization.
        public static string[] SYNC = { "<@SYNC>", "<@TIMESTAMP>", "<@EOF>" };

        //<@FETCHMESSAGES><@CHATID><@TIMESTAMP> 
        public static string[] FETCHCONVERSATION = { "<@FETCHMESSAGES>", "<@CHATID>", "<@TIMESTAMP>", "<@EOF>" };

        #endregion

        #region Gift/Invite
        // Gift responder
        //<@SENDGIFT>{0}<@SENDERCHERRYID>{1}<@RECVCHERRYIDS>{2}<@GAMEID>{3}<@GAMEICONURL>{4}<@IOSURLSCHEME>{5}<@ANDPACKAGE>{6}<@ANDCLASS>{7}<@SENDERMSG>{8}<@RECVMSG><@EOF>
        public static string[] SENDGIFT_GAMESERVER = { "<@SENDGIFT>", "<@SENDERCHERRYID>", "<@RECVCHERRYIDS>", "<@GAMEID>", "<@GAMENAME>","<@GAMEICONURL>", "<@IOSURLSCHEME>", "<@ANDPACKAGE>", "<@ANDCLASS>", "<@SENDERMSG>", "<@RECVMSG>", "<@EOF>" };
        public static string[] SENDGIFTACK = { "<@SENDGIFTACK>", "<@CHATID>", "<@MSGID>", "<@EOF>" };
        public static string[] SENTGIFTACK = { "<@SENTGIFTACK>", "<@CHATID>", "<@MSGID>", "<@EOF>" };
        public static string[] RECVGIFTACK = { "<@RECVGIFTACK>", "<@CHATID>", "<@MSGID>", "<@EOF>" };

        // Invite responder
        //<@SENDINVITATION>{0}<@SENDERCHERRYID>{1}<@RECVCHERRYIDS>{2}<@GAMEID>{3}<@GAMEICONURL>{4}<@IOSURLSCHEME>{5}<@ANDPACKAGE>{6}<@ANDCLASS>{7}<@SENDERMSG>{8}<@RECVMSG><@EOF>
        public static string[] SENDINVITE_GAMESERVER = { "<@SENDINVITATION>", "<@SENDERCHERRYID>", "<@RECVCHERRYIDS>", "<@GAMEID>", "<@GAMENAME>", "<@GAMEICONURL>", "<@IOSURLSCHEME>", "<@ANDPACKAGE>", "<@ANDCLASS>", "<@SENDERMSG>", "<@RECVMSG>", "<@EOF>" };
        public static string[] SENDINVITEACK = { "<@SENDINVITEACK>", "<@CHATID>", "<@MSGID>", "<@EOF>" };
        public static string[] SENTINVITEACK = { "<@SENTINVITEACK>", "<@CHATID>", "<@MSGID>", "<@EOF>" };
        public static string[] RECVINVITEACK = { "<@RECVINVITEACK>", "<@CHATID>", "<@MSGID>", "<@EOF>" };
        #endregion

        #region Create
        // Client sends this message to server to request a chat ID to talk to other user.
        public static string[] CREATE = { "<@CREATE>", "<@RECVCHATUSERID>", "<@TEMP>", "<@EOF>" };

        // Server reply for the CREATE request.
        public static string[] CREATEACK = { "<@CREATEACK>", "<@CHATID>", "<@RECVCHATUSERID>", "<@TEMP>", "<@EOF>" }; 
        #endregion

        #region WebService
        // Update profile command from Web Service
        public static string[] UPDATEPROFILE = { "<@UPDATEPROFILE>", "<@CHATUSERID>", "<@PROFILENAME>", "<@PROFILEIMAGEURL>", "<@STATUS>", "<@LASTUPDATEDPROFILEDATE>", "<@EOF>" };
        public static string[] ADDFRIEND = { "<@ADDFRIEND>", "<@FRIENDCHATUSERID>", "<@CHATUSERID>", "<@PROFILENAME>", "<@PROFILEIMAGEURL>", "<@STATUS>", "<@LASTUPDATEDPROFILEDATE>", "<@RELATIONSHIPTYPE>", "<@EOF>" };
        public static string[] LOGOUTCHERRYACCOUNT = { "<@LOGOUTCHERRYACCOUNT>", "<@EXISTINGCHATUSERID>", "<@EOF>" };
        public static string[] LOGOUTCHATACCOUNT = { "<@LOGOUTCHATACCOUNT>", "<@EXISTINGCHATUSERID>", "<@EOF>" };
        #endregion  

        #region Others
        public static string[] QUERY = { "<@QUERY>", "<@CHATID>", "<@EOF>" };
        public static string[] QUERYACK = { "<@QUERYACK>", "<@CHATID>", "<@ISGROUP>", "<@TITLE>", "<@MEMBERS>", "<@TIMESTAMP>", "<@EOF>" };
        public static string[] KEEPALIVE = { "<@KEEPALIVE>", "<@EOF>" };

        public static string[] PING = { "<@P>", "<@EOF>" };
        public static string[] OK = { "<@OK>", "<@EOF>" };
        #endregion
    }
}
