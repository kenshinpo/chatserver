﻿
namespace ChatServerCore.Message
{
    [MessageType("<@RECVACK>", "<@EOF>")]
    class RecvAckMessage : MessageType<RecvAckMessage>
    {
        [MessageTag("<@CHATID>")]
        public string ChatID;

        [MessageTag("<@MSGID>")]
        public string MsgID;
    }
}
