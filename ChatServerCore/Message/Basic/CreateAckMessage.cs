﻿
namespace ChatServerCore.Message
{
    [MessageType("<@CREATEACK>", "<@EOF>")]
    class CreateAckMessage : MessageType<CreateAckMessage>
    {
        [MessageTag("<@CHATID>")]
        public string ChatId;

        [MessageTag("<@RECVCHATUSERID>")]
        public string RecvId;

        [MessageTag("<@TEMP>")]
        public string Temp;
    }
}
