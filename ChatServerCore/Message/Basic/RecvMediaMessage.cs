﻿
namespace ChatServerCore.Message
{
    [MessageType("<@RECVMEDIA>", "<@EOF>")]
    class RecvMediaMessage : MessageType<RecvMediaMessage>
    {
        [MessageTag("<@CHATID>")]
        public string ChatID;

        [MessageTag("<@MSGID>")]
        public string MsgID;

        [MessageTag("<@SENDERCHATUSERID>")]
        public string SenderChatUserID;

        [MessageTag("<@MESSAGE>")]
        public string Message;

        [MessageTag("<@TYPE>")]
        public int Type;

        [MessageTag("<@TIMESTAMP>")]
        public string Timestamp;
    }
}
