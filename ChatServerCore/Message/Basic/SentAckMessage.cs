﻿
namespace ChatServerCore.Message
{
    [MessageType("<@SENTACK>", "<@EOF>")]
    class SentAckMessage : MessageType<SentAckMessage>
    {
        [MessageTag("<@CHATID>")]
        public string ChatID;

        [MessageTag("<@MSGID>")]
        public string MsgID;
    }
}
