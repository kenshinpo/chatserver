﻿
namespace ChatServerCore.Message
{
    [MessageType("<@SEND>", "<@EOF>")]
    class SendMessage : MessageType<SendMessage>
    {
        [MessageTag("<@CHATID>")]
        public string ChatID;

        [MessageTag("<@TEMP>")]
        public string Temp;

        [MessageTag("<@MESSAGE>")]
        public string Message;

        [MessageTag("<@TYPE>")]
        public int Type;
    }
}
