﻿
namespace ChatServerCore.Message
{
    [MessageType("<@SENDMEDIA>", "<@EOF>")]
    class SendMediaMessage : MessageType<SendMediaMessage>
    {
        [MessageTag("<@CHATID>")]
        public string ChatID;

        [MessageTag("<@TEMP>")]
        public string Temp;

        [MessageTag("<@MESSAGE>")]
        public string Message;

        [MessageTag("<@TYPE>")]
        public int Type;
    }
}
