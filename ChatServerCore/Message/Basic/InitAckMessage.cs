﻿
namespace ChatServerCore.Message
{
    [MessageType("<@INITACK>", "<@EOF>")]
    class InitAckMessage : MessageType<InitAckMessage>
    {
        [MessageTag("<@CHATUSERID>")]
        public string ChatUserID;

        [MessageTag("<@ISVALID>")]
        public int IsValid;

        [MessageTag("<@ISCHERRYACCOUNTLOGIN>")]
        public int IsCherryAccountLogin;
    }
}
