﻿
namespace ChatServerCore.Message
{
    [MessageType("<@LOGOUTCHATACCOUNT>", "<@EOF>")]
    class LogoutChatAccountMessage : MessageType<LogoutChatAccountMessage>
    {
        [MessageTag("<@EXISTINGCHATUSERID>")]
        public string ExistingChatUserID;
    }
}
