﻿
namespace ChatServerCore.Message
{
    [MessageType("<@LOGOUTCHERRYACCOUNT>", "<@EOF>")]
    class LogoutCherryAccountMessage : MessageType<LogoutCherryAccountMessage>
    {
        [MessageTag("<@EXISTINGCHATUSERID>")]
        public string ExistingChatUserID;
    }
}
