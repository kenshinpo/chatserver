﻿
namespace ChatServerCore.Message
{
    [MessageType("<@UPDATEPROFILE>", "<@EOF>")]
    class UpdateProfileMessage : MessageType<UpdateProfileMessage>
    {
        [MessageTag("<@CHATUSERID>")]
        public string ChatUserID;

        [MessageTag("<@PROFILENAME>")]
        public string ProfileName;

        [MessageTag("<@PROFILEIMAGEURL>")]
        public string ProfileImageURL;

        [MessageTag("<@STATUS>")]
        public string Status;

        [MessageTag("<@LASTUPDATEDPROFILEDATE>")]
        public string LastUpdatedProfileDate;
    }
}
