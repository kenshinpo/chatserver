﻿
namespace ChatServerCore.Message
{
    [MessageType("<@SENDACK>", "<@EOF>")]
    class SendAckMessage : MessageType<SendAckMessage>
    {
        [MessageTag("<@CHATID>")]
        public string ChatID;

        [MessageTag("<@MSGID>")]
        public string MsgID;

        [MessageTag("<@TEMP>")]
        public string Temp;

        [MessageTag("<@TIMESTAMP>")]
        public string Timestamp;
    }
}
