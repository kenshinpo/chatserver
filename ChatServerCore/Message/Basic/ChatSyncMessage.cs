﻿
namespace ChatServerCore.Message
{
    [MessageType("<@CHATSYNC>", "<@ENDCHATSYNC>")]
    class ChatSyncMessage : MessageType<ChatSyncMessage>
    {
        [MessageTag("<@CHATID>")]
        public string ChatID;

        [MessageTag("<@SENDERCHATUSERID>")]
        public string SenderChatUserID;

        [MessageTag("<@MESSAGE>")]
        public string Message;

        [MessageTag("<@TYPE>")]
        public int Type;

        [MessageTag("<@UNREADCOUNT>")]
        public int UnreadCount;

        [MessageTag("<@TIMESTAMP>")]
        public string Timestamp;

    }
}
