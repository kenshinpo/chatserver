﻿
namespace ChatServerCore.Message
{
    [MessageType("<@INIT>", "<@EOF>")]

    //[@5/16/2014 12:37:28 PM]: <@INIT>CC0100wu5fh6XcL<@CHATUSERID>T1306111712756B<@CID>1<@OPERATINGTYPE>7bdce98ae3e669dda04391af083d11f4b256daf487b908cb3a829e693abc82a6<@DEVTOKEN><@EOF>

    class InitMessage : MessageType<InitMessage>
    {
        [MessageTag("<@CHATUSERID>")]
        public string ChatUserID;

        [MessageTag("<@CID>")]
        public string CID;

        [MessageTag("<@OPERATINGTYPE>")]
        public string OperatingType;

        [MessageTag("<@DEVTOKEN>")]
        public string DeviceToken;
    }
}
