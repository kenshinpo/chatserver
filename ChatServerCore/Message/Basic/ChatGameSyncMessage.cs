﻿
namespace ChatServerCore.Message
{
    [MessageType("<@CHATGAMESYNC>", "<@ENDCHATGAMESYNC>")]
    class ChatGameSyncMessage : MessageType<ChatGameSyncMessage>
    {
        [MessageTag("<@CHATID>")]
        public string ChatID;

        [MessageTag("<@RECVCHATUSERID>")]
        public string ReceiverChatUserID;

        [MessageTag("<@SENDERCHATUSERID>")]
        public string SenderChatUserID;

        [MessageTag("<@MESSAGE>")]
        public string Message;

        [MessageTag("<@TYPE>")]
        public int Type;

        [MessageTag("<@UNREADCOUNT>")]
        public int UnreadCount;

        [MessageTag("<@TIMESTAMP>")]
        public string Timestamp;

    }
}
