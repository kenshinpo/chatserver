﻿
namespace ChatServerCore.Message
{
    [MessageType("<@SENT>", "<@EOF>")]
    class SentMessage : MessageType<SentMessage>
    {
        [MessageTag("<@CHATID>")]
        public string ChatID;

        [MessageTag("<@MSGID>")]
        public string MsgID;
    }
}
