﻿
namespace ChatServerCore.Message
{
    [MessageType("<@FETCHMESSAGES>", "<@EOF>")]
    class FetchConversationMessage : MessageType<FetchConversationMessage>
    {
        [MessageTag("<@CHATID>")]
        public string ChatID;

        [MessageTag("<@TIMESTAMP>")]
        public string Timestamp;
    }
}
