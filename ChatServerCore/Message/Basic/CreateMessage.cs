﻿
namespace ChatServerCore.Message
{
    [MessageType("<@CREATE>", "<@EOF>")]
    class CreateMessage : MessageType<CreateMessage>
    {
        [MessageTag("<@RECVCHATUSERID>")]
        public string RecvID;

        [MessageTag("<@TEMP>")]
        public string Temp;
    }
}
