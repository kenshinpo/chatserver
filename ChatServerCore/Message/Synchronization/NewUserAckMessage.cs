﻿
namespace ChatServerCore.Message
{
    [MessageType("<@NEWUSERACK>", "<@EOF>")]
    class NewUserAckMessage : MessageType<NewUserAckMessage>
    {
        [MessageTag("<@CHATUSERID>")]
        public string ChatUserID;

        [MessageTag("<@SUCCESS>")]
        public int Succcess;
    }
}
