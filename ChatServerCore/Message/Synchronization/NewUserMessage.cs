﻿
namespace ChatServerCore.Message
{
    [MessageType("<@NEWUSER>", "<@EOF>")]
    class NewUserMessage : MessageType<NewUserMessage>
    {
        [MessageTag("<@ChatUserID>")]
        public string ChatUserID;
    }
}
