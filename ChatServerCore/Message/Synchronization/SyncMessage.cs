﻿
namespace ChatServerCore.Message
{
    [MessageType("<@SYNC>", "<@EOF>")]
    class SyncMessage : MessageType<SyncMessage>
    {
        [MessageTag("<@TIMESTAMP>")]
        public string Timestamp;
    }
}
