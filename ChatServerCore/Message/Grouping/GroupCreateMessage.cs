﻿
namespace ChatServerCore.Message
{
    [MessageType("<@GROUPCREATE>", "<@EOF>")]

    class GroupCreateMessage : MessageType<GroupCreateMessage>
    {
        [MessageTag("<@TITLE>")]
        public string Title;

        [MessageTag("<@TEMP>")]
        public string Temp;

        [MessageTag("<@MEMBERS>")]
        public string Members;
    }
}
