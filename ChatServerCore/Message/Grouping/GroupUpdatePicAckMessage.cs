﻿
namespace ChatServerCore.Message
{
    [MessageType("<@GROUPUPDATEPICACK>", "<@EOF>")]
    class GroupUpdatePicAckMessage : MessageType<GroupUpdatePicAckMessage>
    {
        [MessageTag("<@CHATID>")]
        public string ChatID;

        [MessageTag("<@GROUPPICURL>")]
        public string GroupPicURL;

        [MessageTag("<@NOTIFICATIONMESSAGE>")]
        public string NotificationMessage;

        [MessageTag("<@TIMESTAMP>")]
        public string Timestamp;
    }
}
