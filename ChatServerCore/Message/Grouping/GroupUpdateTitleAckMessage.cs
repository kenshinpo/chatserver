﻿
namespace ChatServerCore.Message
{
    [MessageType("<@GROUPUPDATETITLEACK>", "<@EOF>")]
    class GroupUpdateTitleAckMessage : MessageType<GroupUpdateTitleAckMessage>
    {
        [MessageTag("<@CHATID>")]
        public string ChatID;

        [MessageTag("<@TITLE>")]
        public string Title;

        [MessageTag("<@NOTIFICATIONMESSAGE>")]
        public string NotificationMessage;

        [MessageTag("<@TIMESTAMP>")]
        public string Timestamp;

    }
}
