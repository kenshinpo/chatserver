﻿
namespace ChatServerCore.Message
{
    [MessageType("<@GROUPADD>", "<@EOF>")]
    class GroupAddMessage : MessageType<GroupAddMessage>
    {
        [MessageTag("<@MEMBERS>")]
        public string Members;

        [MessageTag("<@CHATID>")]
        public string ChatID;
    }
}
