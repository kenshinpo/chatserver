﻿
namespace ChatServerCore.Message
{
    [MessageType("<@GROUPLEAVE>", "<@EOF>")]
    class GroupLeaveMessage : MessageType<GroupLeaveMessage>
    {
        [MessageTag("<@CHATID>")]
        public string ChatID;
    }
}
