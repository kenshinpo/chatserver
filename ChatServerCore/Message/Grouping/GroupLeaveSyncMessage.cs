﻿
namespace ChatServerCore.Message
{
    [MessageType("<@GROUPLEAVESYNC>", "<@EOF>")]
    class GroupLeaveSyncMessage : MessageType<GroupLeaveSyncMessage>
    {
        [MessageTag("<@CHATID>")]
        public string ChatID;

        [MessageTag("<@ADMINID>")]
        public string AdminID;

        [MessageTag("<@MEMBERSID>")]
        public string MembersID;

        [MessageTag("<@MEMBERSPROFILENAME>")]
        public string MembersProfileName;

        [MessageTag("<@NOTIFICATIONID>")]
        public string NotificationID;

        [MessageTag("<@NOTIFICATIONMESSAGE>")]
        public string NotificationMessage;

        [MessageTag("<@TIMESTAMP>")]
        public string Timestamp;
    }
}
