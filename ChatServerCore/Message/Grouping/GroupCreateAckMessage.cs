﻿
namespace ChatServerCore.Message
{
    [MessageType("<@GROUPCREATEACK>", "<@EOF>")]
    class GroupCreateAckMessage : MessageType<GroupCreateAckMessage>
    {
        [MessageTag("<@CHATID>")]
        public string ChatID;

        [MessageTag("<@TEMP>")]
        public string Temp;

        [MessageTag("<@MEMBERSID>")]
        public string MembersID;

        [MessageTag("<@MEMBERSPROFILENAME>")]
        public string MembersProfileName;

        [MessageTag("<@TIMESTAMP>")]
        public string Timestamp;
    }
}
