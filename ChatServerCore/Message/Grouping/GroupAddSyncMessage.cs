﻿
namespace ChatServerCore.Message
{
    [MessageType("<@GROUPADDSYNC>", "<@EOF>")]
    class GroupAddSyncMessage : MessageType<GroupAddSyncMessage>
    {
        [MessageTag("<@CHATID>")]
        public string ChatID;

        [MessageTag("<@MEMBERSID>")]
        public string MembersID;

        [MessageTag("<@MEMBERSPROFILENAME>")]
        public string MembersProfileName;

        [MessageTag("<@NOTIFICATIONID>")]
        public string NotificationID;

        [MessageTag("<@NOTIFICATIONMESSAGE>")]
        public string NotificationMessage;

        [MessageTag("<@TIMESTAMP>")]
        public string Timestamp;
    }
}
