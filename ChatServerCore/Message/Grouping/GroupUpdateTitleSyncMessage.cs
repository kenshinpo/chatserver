﻿
namespace ChatServerCore.Message
{
    [MessageType("<@GROUPUPDATETITLESYNC>", "<@EOF>")]
    class GroupUpdateTitleSyncMessage : MessageType<GroupUpdateTitleSyncMessage>
    {
        [MessageTag("<@CHATID>")]
        public string ChatID;

        [MessageTag("<@TITLE>")]
        public string Title;

        [MessageTag("<@NOTIFICATIONID>")]
        public string NotificationID;

        [MessageTag("<@NOTIFICATIONMESSAGE>")]
        public string NotificationMessage;

        [MessageTag("<@TIMESTAMP>")]
        public string Timestamp;
    }
}
