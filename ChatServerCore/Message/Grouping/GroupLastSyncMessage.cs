﻿
namespace ChatServerCore.Message
{
    [MessageType("<@GROUPLASTSYNC>", "<@ENDGROUPLASTSYNC>")]
    class GroupLastSyncMessage : MessageType<GroupLastSyncMessage>
    {
        [MessageTag("<@CHATID>")]
        public string ChatID;

        [MessageTag("<@TITLE>")]
        public string Title;

        [MessageTag("<@GROUPPICURL>")]
        public string GroupPicURL;

        [MessageTag("<@ADMINID>")]
        public string AdminID;

        [MessageTag("<@MEMBERSID>")]
        public string MembersID;

        [MessageTag("<@MEMBERSPROFILENAME>")]
        public string MembersProfileName;

        [MessageTag("<@TIMESTAMP>")]
        public string Timestamp;
    }
}
