﻿
namespace ChatServerCore.Message
{
    [MessageType("<@GROUPUPDATEPICSYNC>", "<@EOF>")]
    class GroupUpdatePicSyncMessage : MessageType<GroupUpdatePicSyncMessage>
    {
        [MessageTag("<@CHATID>")]
        public string ChatID;

        [MessageTag("<@GROUPPICURL>")]
        public string GroupPicURL;

        [MessageTag("<@NOTIFICATIONID>")]
        public string NotificationID;

        [MessageTag("<@NOTIFICATIONMESSAGE>")]
        public string NotificationMessage;

        [MessageTag("<@TIMESTAMP>")]
        public string Timestamp;
    }
}
