﻿
namespace ChatServerCore.Message
{
    [MessageType("<@GROUPUPDATEACK>", "<@EOF>")]
    class GroupUpdateAckMessage : MessageType<GroupUpdateAckMessage>
    {
        [MessageTag("<@CHATID>")]
        public string ChatID;

        [MessageTag("<@TITLE>")]
        public string Title;

        [MessageTag("<@GROUPPICURL>")]
        public string GroupPicURL;
    }
}
