﻿
namespace ChatServerCore.Message
{
    [MessageType("<@GROUPADDACK>", "<@EOF>")]
    class GroupAddAckMessage : MessageType<GroupAddAckMessage>
    {
        [MessageTag("<@CHATID>")]
        public string ChatID;

        [MessageTag("<@MEMBERSID>")]
        public string MembersID;

        [MessageTag("<@MEMBERSPROFILENAME>")]
        public string MembersProfileName;

        [MessageTag("<@TIMESTAMP>")]
        public string Timestamp;
    }
}
