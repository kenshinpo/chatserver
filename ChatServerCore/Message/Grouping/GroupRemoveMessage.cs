﻿
namespace ChatServerCore.Message
{
    [MessageType("<@GROUPREMOVE>", "<@EOF>")]
    class GroupRemoveMessage : MessageType<GroupRemoveMessage>
    {
        [MessageTag("<@REMOVEDMEMBERID>")]
        public string RemovedMemberID;

        [MessageTag("<@CHATID>")]
        public string ChatID;
    }
}
