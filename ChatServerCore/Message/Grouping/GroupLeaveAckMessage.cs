﻿
namespace ChatServerCore.Message
{
    [MessageType("<@GROUPLEAVEACK>", "<@EOF>")]
    class GroupLeaveAckMessage : MessageType<GroupLeaveAckMessage>
    {
        [MessageTag("<@CHATID>")]
        public string ChatID;

        [MessageTag("<@TIMESTAMP>")]
        public string Timestamp;
    }
}
