﻿
namespace ChatServerCore.Message
{
    [MessageType("<@GROUPUPDATE>", "<@EOF>")]
    class GroupUpdateMessage : MessageType<GroupUpdateMessage>
    {
        [MessageTag("<@CHATID>")]
        public string ChatID;

        [MessageTag("<@TITLE>")]
        public string Title;

        [MessageTag("<@GROUPPICURL>")]
        public string GroupPicURL;
    }
}
