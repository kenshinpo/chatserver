﻿
namespace ChatServerCore.Message
{
    [MessageType("<@GROUPREMOVEACK>", "<@EOF>")]
    class GroupRemoveAckMessage : MessageType<GroupRemoveAckMessage>
    {
        [MessageTag("<@CHATID>")]
        public string ChatID;

        [MessageTag("<@ADMINID>")]
        public string AdminID;

        [MessageTag("<@MEMBERSID>")]
        public string MembersID;

        //[MessageTag("<@MEMBERSPROFILENAME>")]
        //public string MembersProfileName;

        [MessageTag("<@TIMESTAMP>")]
        public string Timestamp;
    }
}
