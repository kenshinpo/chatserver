﻿
namespace ChatServerCore.Message
{
    [MessageType("<@RECVMEDIA>", "<@EOF>")]
    class RSRecvMediaMessage : MessageType<RSRecvMediaMessage>
    {
        [MessageTag("<@CHATID>")]
        public string ChatID;

        [MessageTag("<@MSGID>")]
        public string MsgID;

        [MessageTag("<@SENDERCHATUSERID>")]
        public string SenderChatUserID;

        [MessageTag("<@MESSAGE>")]
        public string Message;

        [MessageTag("<@TYPE>")]
        public int Type;

        [MessageTag("<@TIMESTAMP>")]
        public string Timestamp;

        [MessageTag("<@NOTIFICATIONMESSAGE>")]
        public string NotificationMessage;

        [MessageTag("<@BADGE>")]
        public int Badge;

        [MessageTag("<@OPERATINGTYPE>")]
        public int OperatingType;

        [MessageTag("<@DEVICETOKEN>")]
        public string DeviceToken;
    }
}
