﻿
namespace ChatServerCore.Message
{
    [MessageType("<@CHATRECVGIFT>", "<@ENDCHATRECVGIFT>")]
    class ChatRecvGiftMessage : MessageType<ChatRecvGiftMessage>
    {
        [MessageTag("<@CHATID>")]
        public string ChatID;

        [MessageTag("<@MSGID>")]
        public string MsgID;

        [MessageTag("<@SENDERCHATUSERID>")]
        public string SenderChatUserID;

        [MessageTag("<@MESSAGE>")]
        public string Message;

        [MessageTag("<@TYPE>")]
        public int Type;

        [MessageTag("<@TIMESTAMP>")]
        public string Timestamp;
    }
}
