﻿
namespace ChatServerCore.Message
{
    [MessageType("<@CHATSENTINVITE>", "<@ENDCHATSENTINVITE>")]
    class ChatSentInviteMessage : MessageType<ChatSentInviteMessage>
    {
        [MessageTag("<@CHATID>")]
        public string ChatID;

        [MessageTag("<@MSGID>")]
        public string MsgID;

        [MessageTag("<@RECVCHATUSERID>")]
        public string RecvChatUserID;

        [MessageTag("<@MESSAGE>")]
        public string Message;

        [MessageTag("<@TYPE>")]
        public int Type;

        [MessageTag("<@TIMESTAMP>")]
        public string Timestamp;
    }
}
