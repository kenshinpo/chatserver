﻿
namespace ChatServerCore.Message
{
    [MessageType("<@CHATSENDINVITE>", "<@ENDCHATSENDINVITE>")]
    class ChatSendInviteMessage : MessageType<ChatSendInviteMessage>
    {
        [MessageTag("<@CHATID>")]
        public string ChatID;

        [MessageTag("<@MSGID>")]
        public string MsgID;

        [MessageTag("<@RECVCHATUSERID>")]
        public string RecvChatUserID;

        [MessageTag("<@MESSAGE>")]
        public string Message;

        [MessageTag("<@TYPE>")]
        public int Type;

        [MessageTag("<@TIMESTAMP>")]
        public string Timestamp;
    }
}
