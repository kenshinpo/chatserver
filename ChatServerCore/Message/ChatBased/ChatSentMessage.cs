﻿
namespace ChatServerCore.Message
{
    [MessageType("<@CHATSENT>", "<@ENDCHATSENT>")]
    class ChatSentMessage : MessageType<ChatSentMessage>
    {
        [MessageTag("<@CHATID>")]
        public string ChatID;

        [MessageTag("<@MSGID>")]
        public string MsgID;
    }
}
