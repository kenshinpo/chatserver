﻿
namespace ChatServerCore.Message
{
    [MessageType("<@CHATRECVINVITE>", "<@ENDCHATRECVINVITE>")]
    class ChatRecvInviteMessage : MessageType<ChatRecvInviteMessage>
    {
        [MessageTag("<@CHATID>")]
        public string ChatID;

        [MessageTag("<@MSGID>")]
        public string MsgID;

        [MessageTag("<@SENDERCHATUSERID>")]
        public string SenderChatUserID;

        [MessageTag("<@MESSAGE>")]
        public string Message;

        [MessageTag("<@TYPE>")]
        public int Type;

        [MessageTag("<@TIMESTAMP>")]
        public string Timestamp;

    }
}
