﻿
namespace ChatServerCore.Message
{
    [MessageType("<@CHATSENDGIFT>", "<@ENDCHATSENDGIFT>")]
    class ChatSendGiftMessage : MessageType<ChatSendGiftMessage>
    {
        [MessageTag("<@CHATID>")]
        public string ChatID;

        [MessageTag("<@MSGID>")]
        public string MsgID;

        [MessageTag("<@RECVCHATUSERID>")]
        public string RecvChatUserID;

        [MessageTag("<@MESSAGE>")]
        public string Message;

        [MessageTag("<@TYPE>")]
        public int Type;

        [MessageTag("<@TIMESTAMP>")]
        public string Timestamp;

    }
}
