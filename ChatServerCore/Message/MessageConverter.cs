﻿using System.Web;

namespace ChatServerCore.Message
{
    class MessageConverter
    {

        public static string HtmlEncode(string plainText)
        {
            //string HTMLEncodedText = HttpUtility.HtmlEncode(plainText);
            return HtmlCustomEncode(plainText);
        }

        public static string HtmlCustomEncode(string plainText)
        {
            plainText = plainText.Replace("\"", "&#34;");
            plainText = plainText.Replace("'", "&#39");
            plainText = plainText.Replace(",", "&#44;");
            plainText = plainText.Replace("<", "&#60;");
            plainText = plainText.Replace(">", "&#62;");
            plainText = plainText.Replace("@", "&#64;");
            plainText = plainText.Replace("\\", "&#92;");
            
            //Logger.Info(plainText);

            return plainText;
        }

        public static string HtmlDecode(string plainText)
        {
            string HTMLEncodedText = HttpUtility.HtmlDecode(plainText);
            return HtmlCustomDecode(HTMLEncodedText);
        }

        public static string HtmlCustomDecode(string plainText)
        {
            plainText = plainText.Replace("&#44;", ",");
            plainText = plainText.Replace(@"'", @"''");
            return plainText;
        }
    }
}
