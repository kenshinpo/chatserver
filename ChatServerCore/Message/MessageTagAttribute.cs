﻿using System;

namespace ChatServerCore.Message
{
    /// <summary>
    /// Metadata type that provide information to serialize a field into string.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field)]
    class MessageTagAttribute : Attribute
    {
        /// <summary>
        /// The tag that append behind the field value.
        /// </summary>
        public string Tag { get; set; }

        public MessageTagAttribute(string tag)
            : base()
        {
            Tag = tag;
        }
    }
}
