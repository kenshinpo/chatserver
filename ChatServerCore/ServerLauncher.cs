﻿using System.Net;
using ChatServerCore.Connectivity;
using ChatServerCore.Service;
using ChatServerCore.Service.GameServiceResponder;
using ChatServerCore.Service.PrimaryServiceResponder;

namespace ChatServerCore
{
    public class ServerLauncher
    {
        private ServiceHost primaryHost;

        public IPEndPoint PrimaryEndpoint { get { return primaryHost.ListeningEndPoint; } }

        /// <summary>
        /// Start the server.
        /// </summary>
        public void Start()
        {
            MessageServiceFactory primaryServiceFactory = new MessageServiceFactory("Primary Service");
            primaryServiceFactory.RegisterResponder(new InitResponder());
            primaryServiceFactory.RegisterResponder(new SendResponder());
            primaryServiceFactory.RegisterResponder(new RecvAckResponder());
            primaryServiceFactory.RegisterResponder(new SentAckResponder());
            primaryServiceFactory.RegisterResponder(new CreateResponder());
            primaryServiceFactory.RegisterResponder(new GroupCreateResponder());
            primaryServiceFactory.RegisterResponder(new GroupUpdateResponder());
            primaryServiceFactory.RegisterResponder(new GroupAddResponder());
            primaryServiceFactory.RegisterResponder(new GroupLeaveResponder());
            primaryServiceFactory.RegisterResponder(new GroupRemoveResponder());
            primaryServiceFactory.RegisterResponder(new SyncResponder());
            primaryServiceFactory.RegisterResponder(new PingResponder());
            primaryServiceFactory.RegisterResponder(new SendMediaResponder());
            primaryServiceFactory.RegisterResponder(new RecvGiftAckResponder());
            primaryServiceFactory.RegisterResponder(new SentGiftAckResponder());
            primaryServiceFactory.RegisterResponder(new RecvInviteAckResponder());
            primaryServiceFactory.RegisterResponder(new SentInviteAckResponder());
            primaryServiceFactory.RegisterResponder(new SendGiftAckResponder());
            primaryServiceFactory.RegisterResponder(new SendInviteAckResponder());
            primaryServiceFactory.RegisterResponder(new UpdateProfileResponder());
            primaryServiceFactory.RegisterResponder(new AddFriendResponder());
            primaryServiceFactory.RegisterResponder(new FetchConversationPerChatResponder());
            primaryServiceFactory.RegisterResponder(new LogoutCherryAccountResponder());
            primaryServiceFactory.RegisterResponder(new LogoutChatAccountResponder());
            primaryServiceFactory.RegisterResponder(new AndroidKeepAliveResponder());

            primaryHost = new ServiceHost(8000, primaryServiceFactory);

            primaryHost.Run();

            RelayServerCommunication.StartConnectionToRS();

        }

        /// <summary>
        /// Close the server.
        /// </summary>
        public void Close()
        {
            RelayServerCommunication.CloseConnectionToRS();
            primaryHost.Stop();
        }
    }
}
