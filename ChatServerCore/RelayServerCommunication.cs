﻿using System;
using System.Configuration;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using ChatServerCore.Service;
using ChatServerCore.Utilities;

namespace ChatServerCore
{
    // State object for receiving data from remote device.
    public class RSStateObject
    {
        // Client socket.
        public Socket workSocket = null;
        // Size of receive buffer.
        public const int BufferSize = 1024;
        // Receive buffer.
        public byte[] buffer = new byte[BufferSize];
        // Received data string.
        public StringBuilder sb = new StringBuilder();
    }

    public sealed class RelayServerCommunication
    {
        private static readonly RelayServerCommunication instance = new RelayServerCommunication();

        // Log Path.
        private static string logPath = ConfigurationManager.AppSettings["logPath"].ToString();

        // The port number for the RS
        private const int RSPort = 7030;
        // Convert IP to long (in reverse order)
        private const long RSIPAddress = 4194412736;
        public static bool isRSConnected = false;

        // Listening thread
        private static Thread listeningForRSThread;

        // Socket connection from ChatServer to IPTracker
        private static Socket clientSocketToRS;

        // ManualResetEvent instances signal completion.
        private static ManualResetEvent connectDone = new ManualResetEvent(false);
        private static ManualResetEvent sendDone = new ManualResetEvent(false);
        private static ManualResetEvent receiveDone = new ManualResetEvent(false);

        // The response from the remote device.
        private static string response = string.Empty;

        private RelayServerCommunication()
        {

        }

        public static RelayServerCommunication Instance
        {
            get
            {
                return instance;
            }
        }

        public static void StartConnectionToRS()
        {
            //Logger.Info("Server starting connection with RS");

            if (clientSocketToRS == null)
            {
                // Reverse long for ip address
                IPEndPoint remoteEP = new IPEndPoint(RSIPAddress, RSPort);
                clientSocketToRS = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                // Connect to the remote endpoint.
                clientSocketToRS.BeginConnect(remoteEP, new AsyncCallback(ConnectCallback), clientSocketToRS);
                connectDone.WaitOne();

                // Sending Init to IPTracker once successfully connected
                SendInitRSMessage();

                listeningForRSThread = new Thread(ConstantReceivingModeFromRS);
                listeningForRSThread.Start();

                //Logger.Info(String.Format("Response for connecting to RS: {0}", response));
            }
        }

        public static void CloseConnectionToRS()
        {
            if (isRSConnected)
            {
                isRSConnected = false;
                listeningForRSThread.Abort();
                clientSocketToRS.Shutdown(SocketShutdown.Both);
                clientSocketToRS.Close();
            }

        }

        private static void ConnectCallback(IAsyncResult ar)
        {
            try
            {
                //Logger.Info("Establishing connection with RS");

                // Retrieve the socket from the state object.
                Socket client = (Socket)ar.AsyncState;

                // Complete the connection.
                client.EndConnect(ar);

                isRSConnected = true;

                //Logger.Info(String.Format("Connected to RS"));

                // Signal that the connection has been made.
                connectDone.Set();
            }
            catch (Exception e)
            {
                Logger.Info("Connect call back error: " + e.ToString());
            }
        }

        private static void ConstantReceivingModeFromRS()
        {
            try
            {
                while (isRSConnected)
                {
                    Receive(clientSocketToRS);
                    receiveDone.WaitOne();

                    //Logger.Info("Response from ConstantReceivingModeFromRS: " + response.ToString());
                    //Response from ConstantReceivingModeFromRS: CC01N0wu66fn42O<@GROUPSYNC>R01Br0wu69hoB6n<@CHATID>444444<@TITLE> <@GROUPPICURL>CC01E0wu66cmHg3<@MODIFIEDBY>CC01x0wu66d7n7R,CC01N0wu66fn42O,CC01E0wu66cmHg3<@MEMBERS>2014-06-09 09:24:36.000<@TIMESTAMP><@EOF>


                    // Check response from RS
                    // Send message to receiver

                    string receiverID = string.Empty;
                    
                    if(response.Contains("<@"))
                    {
                        receiverID = Regex.Split(response, "<@")[0];
                        ChatSessionCollection onlineUsers = MessageServiceFactory.Instances["Primary Service"].OnlineUsers;

                        if (!string.IsNullOrEmpty(receiverID) && onlineUsers[receiverID] != null)
                        {
                            string messageSub = response.Substring(receiverID.Length, response.Length - receiverID.Length);
                            //Logger.Info("Response from ConstantReceivingModeFromRS: " + messageSub);
                            onlineUsers[receiverID].Transport.Write(Encoding.UTF8.GetBytes(messageSub));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Info(String.Format("RS.ConstantReceivingModeFromRS occurs error: {0}\nStack Trace:\n{1}", ex.Message, ex.StackTrace));
            }
        }

        private static void Receive(Socket client)
        {
            try
            {
                receiveDone = new ManualResetEvent(false);

                //Logger.Info("Inside Receive");

                // Create the state object.
                RSStateObject state = new RSStateObject();
                state.workSocket = client;

                // Begin receiving the data from the remote device.
                client.BeginReceive(state.buffer, 0, RSStateObject.BufferSize, 0,
                    new AsyncCallback(ReceiveCallback), state);
            }
            catch (Exception e)
            {
                Logger.Info("Receive: " + e.ToString());
            }
        }

        private static void ReceiveCallback(IAsyncResult ar)
        {
            try
            {
                if (isRSConnected)
                {
                    //Logger.Info("Inside ReceiveCallback");

                    // Retrieve the state object and the client socket 
                    // from the asynchronous state object.
                    RSStateObject state = (RSStateObject)ar.AsyncState;
                    Socket client = state.workSocket;

                    // Read data from the remote device.
                    int bytesRead = client.EndReceive(ar);

                    if (bytesRead > 0)
                    {
                        state.sb.Append(Encoding.UTF8.GetString(state.buffer, 0, bytesRead));

                        response = state.sb.ToString();
                        //Logger.Info("Response from RS in ReceiveCallback: " + response.ToString());

                        receiveDone.Set();
                    }
                }


            }
            catch (Exception e)
            {
                Logger.Info("ReceiveCallback: " + e.ToString());
            }
        }

        public static void SendInitRSMessage()
        {
            string data = String.Format("<@CHATSERVER_INIT>{0}<@IP><@EOF>", GetCurrentServerExternalAddress());
            if (isRSConnected)
            {
                Send(clientSocketToRS, data);
                //Logger.Info("Sent: " + data);
                sendDone.WaitOne();

                //Receive(clientSocketToRS);
                //receiveDone.WaitOne();

                //Logger.Info("Response for connecting to RS: " + response);
            }
            else
            {
                Logger.Info("Unable to send message to RS, Server not connected to RS");
            }
        }

        public static void SendClientConnectMessage(string chatUserID)
        {
            string data = String.Format("<@CLIENT_CHATSERVER_INIT>{0}<@CHATUSERID><@EOF>", chatUserID);
            if (isRSConnected)
            {
                Send(clientSocketToRS, data);
                //Logger.Info("Sent: " + data);
                sendDone.WaitOne();
            }
            else
            {
                Logger.Info("Unable to send message to RS, Server not connected to RS");
            }
        }

        public static void SendClientDisconnectMessage(string chatUserID)
        {
            string data = String.Format("<@CLIENT_CHATSERVER_DISCONNECT>{0}<@CHATUSERID><@EOF>", chatUserID);
            if (isRSConnected)
            {
                Send(clientSocketToRS, data);
                //Logger.Info("Sent: " + data);
                sendDone.WaitOne();
            }
            else
            {
                Logger.Info("Unable to send message to RS, Server not connected to RS");
            }
        }

        public static void SendSearchUserMessage(string receiverChatUserID, string message)
        {
            string[] messageChunks = null;
            string data = string.Empty;

            if (message.Contains("<@RECV>"))
            {
                // For receiver
                messageChunks = Regex.Split(message, "<@RECV>");
                data = String.Format("<@CHATSERVER_SEARCHUSERFORRECV>{0}<@CHATUSERID>{1}", receiverChatUserID, messageChunks[1]);
            }
            else if (message.Contains("<@RECVMEDIA>"))
            {
                // For receiver (media)
                messageChunks = Regex.Split(message, "<@RECVMEDIA>");
                data = String.Format("<@CHATSERVER_SEARCHUSERFORRECVMEDIA>{0}<@CHATUSERID>{1}", receiverChatUserID, messageChunks[1]);
            }
            else if (message.Contains("<@SENT>"))
            {
                // For sender
                messageChunks = Regex.Split(message, "<@SENT>");
                data = String.Format("<@CHATSERVER_SEARCHUSERFORSENT>{0}<@CHATUSERID>{1}", receiverChatUserID, messageChunks[1]);
            }
            else if (message.Contains("<@GROUPSYNC>"))
            {
                // Send Group SYNC
                messageChunks = Regex.Split(message, "<@GROUPSYNC>");
                data = String.Format("<@CHATSERVER_SEARCHUSERFORGROUPSYNC>{0}<@CHATUSERID>{1}", receiverChatUserID, messageChunks[1]);
            }
            else if (message.Contains("<@GROUPUPDATEPICSYNC>"))
            {
                // Send Group SYNC (pic)
                messageChunks = Regex.Split(message, "<@GROUPUPDATEPICSYNC>");
                data = String.Format("<@CHATSERVER_SEARCHUSERFORGROUPPICUPDATE>{0}<@CHATUSERID>{1}", receiverChatUserID, messageChunks[1]);
            }
            else if (message.Contains("<@GROUPUPDATETITLESYNC>"))
            {
                // Send Group SYNC (title)
                messageChunks = Regex.Split(message, "<@GROUPUPDATETITLESYNC>");
                data = String.Format("<@CHATSERVER_SEARCHUSERFORGROUPTITLEUPDATE>{0}<@CHATUSERID>{1}", receiverChatUserID, messageChunks[1]);
            }
            else if (message.Contains("<@GROUPLEAVESYNC>"))
            {
                // Send Group SYNC (leave)
                messageChunks = Regex.Split(message, "<@GROUPLEAVESYNC>");
                data = String.Format("<@CHATSERVER_SEARCHUSERFORGROUPLEAVE>{0}<@CHATUSERID>{1}", receiverChatUserID, messageChunks[1]);
            }
            else if (message.Contains("<@GROUPREMOVESYNC>"))
            {
                // Send Group SYNC (remove)
                messageChunks = Regex.Split(message, "<@GROUPREMOVESYNC>");
                data = String.Format("<@CHATSERVER_SEARCHUSERFORGROUPREMOVE>{0}<@CHATUSERID>{1}", receiverChatUserID, messageChunks[1]);
            }
            else if (message.Contains("<@GROUPADDSYNC>"))
            {
                // Send Group SYNC (add)
                messageChunks = Regex.Split(message, "<@GROUPADDSYNC>");
                data = String.Format("<@CHATSERVER_SEARCHUSERFORGROUPADD>{0}<@CHATUSERID>{1}", receiverChatUserID, messageChunks[1]);
            }
            else if (message.Contains("<@SENTGIFT>"))
            {
                // For sender (gift)
                messageChunks = Regex.Split(message, "<@SENTGIFT>");
                data = String.Format("<@CHATSERVER_SEARCHUSERFORGIFTSENT>{0}<@CHATUSERID>{1}", receiverChatUserID, messageChunks[1]);
            }
            else if (message.Contains("<@SENTINVITE>"))
            {
                // For sender (invite)
                messageChunks = Regex.Split(message, "<@SENTINVITE>");
                data = String.Format("<@CHATSERVER_SEARCHUSERFORINVITESENT>{0}<@CHATUSERID>{1}", receiverChatUserID, messageChunks[1]);
            }

            if (isRSConnected)
            {
                Send(clientSocketToRS, data);
                //Logger.Info("Sent: " + data);
                sendDone.WaitOne();
            }
            else
            {
                Logger.Info("Unable to send message to RS, Server not connected to RS");
            }
        }

        private static void Send(Socket client, String data)
        {
            // Convert the string data to byte data using UTF8.
            byte[] byteData = Encoding.UTF8.GetBytes(data);

            // Begin sending the data to the remote device.
            client.BeginSend(byteData, 0, byteData.Length, 0,
                new AsyncCallback(SendCallback), client);
        }

        private static void SendCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.
                Socket client = (Socket)ar.AsyncState;

                // Complete sending the data to the remote device.
                int bytesSent = client.EndSend(ar);

                // Signal that all bytes have been sent.
                sendDone.Set();
            }
            catch (Exception e)
            {
                Logger.Info("SendCallback for RS: " + e.ToString());
            }
        }

        private static string GetCurrentServerExternalAddress()
        {
            IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
            int ipv4Index = 0;

            foreach (IPAddress address in ipHostInfo.AddressList)
            {
                if (address.AddressFamily == AddressFamily.InterNetwork && !address.ToString().StartsWith("192"))
                {
                    break;
                }
                else ipv4Index++;
            }

            /*
            if (ipv4Index >= ipHostInfo.AddressList.Count())
            {
                ipv4Index = 0;
            }
             */

            IPAddress ipAddress = ipHostInfo.AddressList[ipv4Index];
            string domainName = ConfigurationManager.AppSettings[ipAddress.ToString()].ToString();

            return domainName;
        }
    }
}
