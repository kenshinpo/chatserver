﻿using System;
using System.ServiceProcess;
using ChatServerCore;
using ChatServerCore.Utilities;

namespace ChatServerService
{
    partial class MainService : ServiceBase
    {
        private ServerLauncher sl;

        public MainService()
        {
            InitializeComponent();
            sl = new ServerLauncher();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                sl.Start();
            }
            catch (Exception ex)
            {
                Logger.Info(ex.Message.ToString() + " " + ex.StackTrace.ToString());
            }

        }

        protected override void OnStop()
        {
            try
            {
                sl.Close();
            }
            catch (Exception ex)
            {
                Logger.Info(ex.Message.ToString() + " " + ex.StackTrace.ToString());
            }
        }
    }
}
